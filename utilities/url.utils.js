export const generatePath = (url, params = null) => {
  if (!url) return '';

  const pathsToReplace = url.match(/:([a-zA-Z0-9]+)/g);

  if (pathsToReplace) {
    pathsToReplace.forEach(path => {
      const pattern = new RegExp(`${path}\\\??`);
      const value = params ? params[path.substring(1)] : null;
      url = value !== null || value === '' ? url.replace(pattern, value.toString()) : url;
    });
  }

  return url;
};
