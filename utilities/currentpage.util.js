export const findPage = (nav, query) => {
  const flattenedNav = [...nav.primary, ...nav.secondary, ...nav.footer];

  for (let i = 0; i < flattenedNav.length; i++) {
    const page = flattenedNav[i];

    if (page.slug === query.page) {
      if (!query.subpage) return page;

      for (let j = 0; j < page.routes.length; j++) {
        const subpage = page.routes[j];

        if (subpage.slug === query.subpage) {
          if (!query.child) return subpage;

          for (let k = 0; k < subpage.routes.length; k++) {
            const child = subpage.routes[k];
            if (child.slug === query.child) return child;
          }
        }
      }
    }
  }

  return false;
};

export const findProductById = (nav, pageId) => {
  const flattenedNav = [...nav.primary, ...nav.secondary];

  for (let i = 0; i < flattenedNav.length; i++) {
    const page = flattenedNav[i];
    if (page.pageTemplate === 'overview' && page.menuId === pageId) {
      return page;
    }

    for (let j = 0; j < page.routes.length; j++) {
      const subpage = page.routes[j];

      if (subpage.pageTemplate === 'overview' && subpage.menuId === pageId) {
        return subpage;
      }

      if (subpage.routes) {
        for (let k = 0; k < subpage.routes.length; k++) {
          const child = subpage.routes[k];
          if (child.pageTemplate === 'overview' && child.menuId === pageId) return child;
        }
      }
    }
  }

  return false;
};

export const findPageByIDs = (nav, menuId, subMenuId, pageId) => {
  const flattenedNav = [...nav.primary, ...nav.secondary];

  const primary = flattenedNav.find(item => item.menuId === menuId);
  if (!primary) return null;

  const secondary = primary.routes.find(item => item.menuId === subMenuId);
  if (!secondary) return null;

  const page = secondary.routes.find(item => item.menuId === pageId);
  if (!page) return null;

  return page.route;
};

// export const findPageByIdAndName = (nav, { menuId, name }) => {
//   const flattenedNav = [...nav.primary, ...nav.secondary];


// };
