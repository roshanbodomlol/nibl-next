const isIE = () => {
  const sAgent = window.navigator.userAgent;
  const Idx = sAgent.indexOf('MSIE');

  // If IE, return version number.
  if (Idx > 0) {
    return parseInt(sAgent.substring(Idx + 5, sAgent.indexOf('.', Idx)), 10);
  }

  if (!!navigator.userAgent.match(/Trident\/7\./)) {
    return 11;
  }

  return false;
};

export {
  isIE
};
