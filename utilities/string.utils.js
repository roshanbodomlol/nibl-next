export const removeSpecialCharacters = (str) => {
  const a = str.replace(/[^\w\s]/gi, '');
  const b = a.replace(/[0-9]/gi, '');

  return b;
};

export const numeric = (str) => {
  const a = str.replace(/[^\w\s]/gi, '');
  const b = a.replace(/[\w\s]/gi, '');

  return b;
};
