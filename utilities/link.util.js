import React, { Children } from 'react';
import { withRouter } from 'next/router';
import Link from 'next/link';
import classnames from 'classnames';

const ActiveLinkDynamic = ({
  router, children, ...props
}) => {
  const child = Children.only(children);
  const { props: childProps } = child;

  const { as } = props;
  const { query } = router;

  const linkParts = as.split('/');
  const queryParts = Object.values(query);

  linkParts.splice(0, 1);

  const inPath = linkParts[0] === queryParts[0] && linkParts[1] === queryParts[1];
  const active = inPath && linkParts[2] === queryParts[2];

  const classes = () => classnames(childProps.className, {
    inPath,
    active
  });

  return <Link {...props}>{React.cloneElement(child, { ...childProps, className: classes() })}</Link>;
};

export default withRouter(ActiveLinkDynamic);
