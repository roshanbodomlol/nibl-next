import { Component } from 'react';
import { GoLocation } from 'react-icons/go';
import { MdPhone, MdAttachMoney } from 'react-icons/md';
import { AiOutlineCalculator } from 'react-icons/ai';
import { FiLink } from 'react-icons/fi';
import Link from 'next/link';
import { Tooltip } from 'antd';
import { connect } from 'react-redux';
import { take, slice } from 'lodash';
import cn from 'classnames';

import { withTranslation } from '../../locales/i18n';
import { setSplashOpen } from '../../redux/actions/common.actions';
import { get } from '../../services/api.services';
import { API } from '../../constants';
import Styles from './styles.module.scss';

/**
 * @visibleName Fixed Right Icons
 */

class FixedRightIcon extends Component {
  state = {
    concatSlideList: true,
    slides: [],
    tools: []
  }

  componentDidMount() {
    const { dispatch } = this.props;

    const oldsplashes = localStorage.getItem('NIBL_SPLASHES') || '';

    get(API.endPoints.POPUP)
      .then((response) => {
        const slides = response.pageContent.map((r) => r.bannerTitle);
        this.setState({ slides }, () => {
          const splashesString = JSON.stringify(slides.join('-'));
          localStorage.setItem('NIBL_SPLASHES', splashesString);
          if (splashesString !== oldsplashes) {
            dispatch(setSplashOpen(0));
          }
        });
      })
      .catch((e) => console.error(e));
    get(API.endPoints.TOOLBAR_LINKS)
      .then((res) => this.setState({ tools: res.pageContent }))
      .catch((e) => {
        console.log(e);
      });
  }

  openSplashModal = (slideIndex) => {
    const { dispatch } = this.props;
    dispatch(setSplashOpen(slideIndex));
  }

  render() {
    const { concatSlideList, slides, tools } = this.state;
    const { t } = this.props;

    const list = slides.map((slide, i) => (
      <li key={`slide-button-${i}`}>
        <span
          role="button"
          tabIndex="-1"
          onClick={() => { this.openSplashModal(i); }}
        >
          {slide}
        </span>
      </li>
    ));

    const firstThreeList = take(list, 3);
    const restList = slice(list, 3);

    const toolCalculator = tools.find(e => e.toolbarType === 'Calculator');
    const toolPhone = tools.find(e => e.toolbarType === 'Contact');
    const toolLocation = tools.find(e => e.toolbarType === 'Location');
    const toolQuickLink = tools.find(e => e.toolbarType === 'QuickLink');
    const toolForex = tools.find(e => e.toolbarType === 'Forex');
    const toolEbanking = tools.find(e => e.toolbarType === 'E-banking');
    const toolOnlineAccount = tools.find(e => e.toolbarType === 'OnlineAccount');

    return (
      <div className={Styles.outer}>
        <div className={Styles.wrapper}>
          {
            toolLocation && (
              <Link href="/[page]/[subpage]/[child]" as={toolLocation.toolbarUrl}>
                <Tooltip title={toolLocation.toolbarTitle} placement="left">
                  <a className={Styles.iconWrapper}>
                    <div className={Styles.icon}>
                      <GoLocation/>
                    </div>
                  </a>
                </Tooltip>
              </Link>
            )
          }
          {
            toolPhone && (
              <Link href="/[page]/[subpage]/[child]" as={toolPhone.toolbarUrl}>
                <Tooltip title={toolPhone.toolbarTitle} placement="left">
                  <a className={Styles.iconWrapper}>
                    <div className={Styles.icon}>
                      <MdPhone/>
                    </div>
                  </a>
                </Tooltip>
              </Link>
            )
          }
          {
            toolForex && (
              <Link href="/[page]/[subpage]/[child]" as={toolForex.toolbarUrl}>
                <Tooltip title={toolForex.toolbarTitle} placement="left">
                  <a className={Styles.iconWrapper}>
                    <div className={Styles.icon}>
                      <MdAttachMoney/>
                    </div>
                  </a>
                </Tooltip>
              </Link>
            )
          }
          {
            toolCalculator && (
              <Link href={toolCalculator.toolbarUrl}>
                <Tooltip title={toolCalculator.toolbarTitle} placement="left">
                  <a className={Styles.iconWrapper}>
                    <div className={Styles.icon}>
                      <AiOutlineCalculator/>
                    </div>
                  </a>
                </Tooltip>
              </Link>
            )
          }
          {
            toolQuickLink && (
              <Link href={toolQuickLink.toolbarUrl}>
                <Tooltip title={toolQuickLink.toolbarTitle} placement="left">
                  <a className={Styles.iconWrapper}>
                    <div className={Styles.icon}>
                      <FiLink/>
                    </div>
                  </a>
                </Tooltip>
              </Link>
            )
          }
          {
            toolEbanking && (
              <Tooltip title={toolEbanking.toolbarTitle} placement="left">
                <a
                  target="_blank"
                  className={Styles.iconWrapper}
                  href={toolEbanking.toolbarUrl}
                >
                  <div className={Styles.icon}>
                    <img src="/img/e-banking.svg" alt=""/>
                  </div>
                </a>
              </Tooltip>
            )
          }
          {
            toolOnlineAccount && (
              <Tooltip title={toolOnlineAccount.toolbarTitle} placement="left">
                <a
                  target="_blank"
                  className={Styles.iconWrapper}
                  href={toolOnlineAccount.toolbarUrl}
                >
                  <div className={Styles.icon}>
                    <img src="/img/account-opening.svg" alt=""/>
                  </div>
                </a>
              </Tooltip>
            )
          }
        </div>
        {
          slides.length > 0 && (
            <div className={Styles.info}>
              <div
                className={cn(Styles.button, 'animated bounce animate__infinite infinite')}
                onClick={() => { this.openSplashModal(0); }}
                role="button"
                tabIndex="-1"
              >
                <span>i</span>
              </div>
              <div className={Styles.submenu}>
                <ul>
                  {firstThreeList}
                  {
                    list.length > 3 && (
                      <>
                        {
                          concatSlideList
                            ? (
                              <li>
                                <span
                                  role="button"
                                  tabIndex="-1"
                                  onClick={() => { this.setState({ concatSlideList: false }); }}
                                >
                                  {t('global_viewMore')}
                                </span>
                              </li>
                            )
                            : restList
                        }
                      </>
                    )
                  }
                </ul>
              </div>
            </div>
          )
        }
      </div>
    );
  }
}

export default connect()(withTranslation('common')(FixedRightIcon));
