import React from 'react';
import classnames from 'classnames';
import { MdSearch as Search } from 'react-icons/md';

import Styles from './searchBar.module.scss';

const SearchBar = () => (
  <div className={classnames(Styles.search, '_search')}>
    <div className={classnames(Styles.icon, 'greyIcon')}>
      <Search style={{ fontSize: 21 }}/>
    </div>
    <input type="text" placeholder="Search for Jobs"/>
  </div>
);

export default SearchBar;
