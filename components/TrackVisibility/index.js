import 'intersection-observer';
import { useInView } from 'react-intersection-observer';

const TrackVisibility = ({
  threshold, children, onView, onEntry
}) => {
  const [ref, inView, entry] = useInView({ threshold });

  if (inView) {
    onView();
  }

  if (entry) {
    onEntry();
  }

  return (
    <div ref={ref}>
      {children}
    </div>
  );
};

TrackVisibility.defaultProps = {
  threshold: 0,
  onView() {},
  onEntry() {}
};

export default TrackVisibility;
