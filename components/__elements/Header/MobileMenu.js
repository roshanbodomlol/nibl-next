import { IoIosMenu } from 'react-icons/io';
import { FiSearch } from 'react-icons/fi';
import { useState } from 'react';
import { connect } from 'react-redux';

import DrawerMenu from './DrawerMenu';
import { showSearchBlock } from '../../../redux/actions/common.actions';
import './MobileMenu.scss';
import LogoMain from '../../LogoMain';
import LanguageSwitcher from '../../LanguageSwitcher';

const MobileMenu = ({ dispatch }) => {
  const [isDrawerOpen, setDrawerOpen] = useState(false);

  return (
    <>
      <DrawerMenu isVisible={isDrawerOpen} onClose={() => setDrawerOpen(false)}/>
      <div id="mobile-menu">
        <div
          className="clicker"
          onClick={() => setDrawerOpen(true)}
        >
          <IoIosMenu/>
        </div>
        <div className="logo">
          <LogoMain/>
        </div>
        <div className="search">
          <FiSearch onClick={() => dispatch(showSearchBlock())}/>
          <LanguageSwitcher mobile/>
        </div>
      </div>
    </>
  );
};

export default connect()(MobileMenu);
