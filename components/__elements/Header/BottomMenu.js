import { connect } from 'react-redux';
import cn from 'classnames';
import { GoSearch } from 'react-icons/go';
import { MdExpandMore } from 'react-icons/md';

import Link from '../../../utilities/link.util';
import { showSearchBlock } from '../../../redux/actions/common.actions';
import LogoMain from '../../LogoMain';
import Logos from '../../Logos';
import css from './style.module.scss';
import { withTranslation } from '../../../locales/i18n';

const mapStateToProps = ({ navigation, currentpage }) => ({ navigation, currentpage });

const BottomMenu = ({ navigation, currentpage, dispatch, t }) => {
  if (!navigation) return null;

  const menu = currentpage && currentpage.routes && currentpage.routes.map((route, i) => {
    const currentPageSlug = route.slug;
    const hasChildren = route.hasChild && route.routes && route.routes.length > 0;
    const childPages = hasChildren && route.routes.map((child) => {
      const hasSections = child.pageTemplate === 'overview' && child.productDetails && child.productDetails.length > 0;
      const lockerSections = child.pageTemplate === 'locker';
      return (
        <li key={`submenu-${child.slug}`}>
          {
            child.pageTemplate === 'pageLink'
              ? (
                <a href={child.linkUrl} target="_blank" rel="noreferrer noopener">{child.name}</a>
              )
              : (
                <Link href="/[page]/[subpage]/[child]" as={`/${currentpage.slug}/${currentPageSlug}/${child.slug}`}>
                  <a>{child.name}</a>
                </Link>
              )
          }
          {
            hasSections && (
              <div className="submenu">
                <ul>
                  <li className="menuli">
                    <Link href="/[page]/[subpage]/[child]" as={`/${currentpage.slug}/${currentPageSlug}/${child.slug}#details`}>
                      <a>{t('global_details')}</a>
                    </Link>
                  </li>
                  {
                    child.productDetails && child.productDetails[0].featureDescription && (
                      <li className="menuli">
                        <Link href="/[page]/[subpage]/[child]" as={`/${currentpage.slug}/${currentPageSlug}/${child.slug}#features`}>
                          <a>{t('global_features')}</a>
                        </Link>
                      </li>
                    )
                  }
                  {
                    child.productDetails && child.productDetails[0].benefitDescription && (
                      <li className="menuli">
                        <Link href="/[page]/[subpage]/[child]" as={`/${currentpage.slug}/${currentPageSlug}/${child.slug}#benefits`}>
                          <a>{t('template_domesticRemit_benefits')}</a>
                        </Link>
                      </li>
                    )
                  }
                  {/* <li className="menuli">
                    <Link href="/[page]/[subpage]/[child]" as={`/${currentpage.slug}/${currentPageSlug}/${child.slug}#how-to-apply`}>
                      <a>How to Apply</a>
                    </Link>
                  </li> */}
                </ul>
                <div className="submenu-extra">
                  <div className="menu-image">
                    <img src={child.productDetails && child.productDetails[0].overviewImage} alt=""/>
                    <div className="_overlay">
                      <div className="_name">{child.name}</div>
                      <div dangerouslySetInnerHTML={{ __html: child.productDetails && child.productDetails[0].overviewCaption }}/>
                      {
                        child.productDetails && child.productDetails[0].pageUploadedFile1 && (
                          <a href={child.productDetails[0].pageUploadedFile1} download>Apply Now</a>
                        )
                      }
                    </div>
                  </div>
                </div>
              </div>
            )
          }
          {
            lockerSections && (
              <div className="submenu __lockers">
                <ul>
                  <li className="menuli">
                    <Link href="/[page]/[subpage]/[child]" as={`/${currentpage.slug}/${currentPageSlug}/${child.slug}#timings`}>
                      <a>{t('template_locker_lockerTimings')}</a>
                    </Link>
                  </li>
                  <li className="menuli">
                    <Link href="/[page]/[subpage]/[child]" as={`/${currentpage.slug}/${currentPageSlug}/${child.slug}#available`}>
                      <a>{t('tempalte_locker_types')}</a>
                    </Link>
                  </li>
                  <li className="menuli">
                    <Link href="/[page]/[subpage]/[child]" as={`/${currentpage.slug}/${currentPageSlug}/${child.slug}#facility`}>
                      <a>{t('tempalte_locker_title')}</a>
                    </Link>
                  </li>
                  <li className="menuli">
                    <Link href="/[page]/[subpage]/[child]" as={`/${currentpage.slug}/${currentPageSlug}/${child.slug}#sizes`}>
                      <a>{t('tempalte_locker_sizes')}</a>
                    </Link>
                  </li>
                </ul>
              </div>
            )
          }
        </li>
      );
    });
    return (
      <li key={`submenu-${route.slug}`}>
        {
          route.pageTemplate !== 'nonclickable'
            ? (
              <>
                {
                  route.pageTemplate === 'pageLink'
                    ? (
                      <a href={route.linkUrl} target="_blank" rel="noreferrer noopener">{route.name}</a>
                    )
                    : (
                      <Link
                        href="/[page]/[subpage]"
                        as={`/${currentpage.slug}/${route.slug}`}
                      >
                        <a className={cn(css.mainMenuA, route.pageTemplate, { withChild: hasChildren })}>
                          <span className={css.menuText}>{route.name}</span>
                          {hasChildren && <MdExpandMore/>}
                        </a>
                      </Link>
                    )
                }
              </>
            )
            : (
              <>
                {
                  route.routes && route.routes.length > 0
                    ? (
                      <>
                        {
                          route.routes[0].pageTemplate === 'pageLink'
                            ? (
                              <a href={route.routes[0].linkUrl} target="_blank" rel="noreferrer noopener">{route.name}</a>
                            )
                            : (
                              <Link
                                href="/[page]/[subpage]/[child]"
                                as={`/${currentpage.slug}/${route.slug}/${route.routes[0].slug}`}
                              >
                                <a className={cn(css.mainMenuA, route.pageTemplate, { withChild: hasChildren })}>
                                  <span className={css.menuText}>{route.name}</span>
                                  {hasChildren && <MdExpandMore/>}
                                </a>
                              </Link>
                            )
                        }
                      </>
                    )
                    : (
                      <a className={css.mainMenuA}>
                        <span className={css.menuText}>{route.name}</span>
                      </a>
                    )
                }
              </>
            )
        }
        {
          hasChildren && (
            <div className="dropdown">
              <ul>
                {childPages}
              </ul>
            </div>
          )
        }
      </li>
    );
  });
  return (
    <div className={css.bottomNav}>
      <div className="big-container">
        <div className={css.btmNavFlex}>
          <div className={css.left}>
            <div className={css.logo}>
              <LogoMain/>
            </div>
            <div className={cn(css.btmMenuInner, 'main-menu')}>
              <ul>
                {menu}
              </ul>
            </div>
          </div>
          <div className={css.right}>
            <Logos/>
            <input type="text"/>
            <span
              className={css.icon}
              role="button"
              tabIndex="-1"
              onClick={() => { dispatch(showSearchBlock()); }}
            >
              <GoSearch/>
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default connect(mapStateToProps)(withTranslation('common')(BottomMenu));
