import { connect } from 'react-redux';
import Link from 'next/link';
import classnames from 'classnames';

import { setCurrentpage } from '../../../redux/actions/currentpage.actions';
import LanguageSwitcher from '../../LanguageSwitcher';
import css from './style.module.scss';

const mapStateToProps = ({ navigation, currentpage }) => ({ navigation, currentpage });

const firstChildPath = (routeObj) => {
  if (!routeObj.pageTemplate || routeObj.pageTemplate === 'nonclickable') {
    if (routeObj.routes[0].pageTemplate === 'nonclickable') {
      return ['/[page]/[subpage]/[child]', `/${routeObj.slug}/${routeObj.routes[0].slug}/${routeObj.routes[0].routes[0].slug}`];
    }

    return ['/[page]/[subpage]', `/${routeObj.slug}/${routeObj.routes[0].slug}`];
  }

  return ['/[page]', `/${routeObj.slug}`];
};

const MainMenu = ({ navigation, dispatch, currentpage }) => {
  const primaryMenu = navigation && navigation.primary && navigation.primary.map((menu, index) => {
    const classes = classnames({
      active: currentpage.slug === menu.slug
    });
    return (
      <Link
        key={`primary-menu-item-${index}`}
        href={firstChildPath(menu)[0]}
        as={firstChildPath(menu)[1]}
      >
        <a className={classes} onClick={() => dispatch(setCurrentpage(menu))}>{menu.name}</a>
      </Link>
    );
  });
  const secondaryMenu = navigation && navigation.secondary && navigation.secondary.map((menu, index) => {
    const classes = classnames({
      active: currentpage.slug === menu.slug
    });
    return (
      <Link
        key={`secondary-menu-item-${index}`}
        href={firstChildPath(menu)[0]}
        as={firstChildPath(menu)[1]}
      >
        <a className={classes} onClick={() => dispatch(setCurrentpage(menu))}>{menu.name}</a>
      </Link>
    );
  });
  return (
    <div className={css.topNav}>
      <div className="big-container">
        <div className={css.topNavFlex}>
          <div className={css.leftMenu}>
            <LanguageSwitcher/>
            <div className={css.topMenuInner}>
              {primaryMenu}
            </div>
          </div>
          <div className={css.rightMenu}>
            {/* <span className={css.secondLogo}/> */}
            <div className={css.topMenuInner}>
              {secondaryMenu}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default connect(mapStateToProps)(MainMenu);
