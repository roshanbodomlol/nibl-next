import { Drawer, Menu } from 'antd';
import { connect } from 'react-redux';
import StaticLink from 'next/link';

import Logos from '../../Logos';
import Link from '../../../utilities/link.util';
import './DrawerMenu.scss';

const { SubMenu, Item } = Menu;

const mapStateToProps = ({ navigation }) => ({ navigation });

const DrawerMenu = ({ isVisible, onClose, navigation }) => {
  const flatNavigation = [...navigation.primary, ...navigation.secondary];

  const menu = flatNavigation.map((page, i) => {
    const subPages = page.routes && page.routes.map((subpage, j) => {
      if (subpage.pageTemplate === 'nonclickable' && !subpage.hasChild && !!subpage.routes) {
        if (subpage.routes[0].pageTemplate === 'pageLink') {
          return (
            <Item>
              <a target="_blank" rel="noreferrer noopener" href={subpage.routes[0].linkUrl}>{subpage.routes[0].name}</a>
            </Item>
          );
        }
        return (
          <Item>
            <Link href="/[page]/[subpage]/[child]" as={subpage.routes[0].route}><a onClick={onClose}>{subpage.routes[0].name}</a></Link>
          </Item>
        );
      }
      const children = subpage.routes && subpage.routes.map((child, k) => {
        if (child.pageTemplate === 'pageLink') {
          return (
            <Item>
              <a target="_blank" rel="noreferrer noopener" href={subpage.routes[0].linkUrl}>{subpage.routes[0].name}</a>
            </Item>
          );
        }
        return (
          <Item
            key={`m-menu-child-${k}`}
          >
            <Link href="/[page]/[subpage]/[child]" as={child.route}><a onClick={onClose}>{child.name}</a></Link>
          </Item>
        );
      });
      return (
        <SubMenu
          key={`m-menu-subpage-${j}`}
          title={subpage.name}
        >
          {children}
        </SubMenu>
      );
    });
    return (
      <SubMenu
        key={`m-menu-page-${i}`}
        title={page.name}
      >
        {subPages}
      </SubMenu>
    );
  });

  return (
    <Drawer
      placement="left"
      visible={isVisible}
      onClose={onClose}
      closable={false}
      width={350}
    >
      <div className="drawer-logo">
        <StaticLink href="/">
          <a onClick={onClose}>
            <img src="/img/logo.png" alt=""/>
          </a>
        </StaticLink>
      </div>
      <Menu mode="inline">
        {menu}
      </Menu>
      <div className="bottom-logos">
        <Logos/>
      </div>
    </Drawer>
  );
};

export default connect(mapStateToProps)(DrawerMenu);
