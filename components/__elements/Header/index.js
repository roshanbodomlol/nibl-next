import { connect } from 'react-redux';
import { Spin } from 'antd';

import MainMenu from './MainMenu';
import BottomMenu from './BottomMenu';
import MobileMenu from './MobileMenu';
import SearchBlock from '../../SearchBlock';
import css from './style.module.scss';
import './MainMenu.scss';

const mapStateToProps = ({ common }) => ({ common });

const Header = ({ common }) => (
  <div className={css.headerWrapper}>
    {
      common.siteLoading && (
        <div className="site-loading">
          <Spin/>
        </div>
      )
    }
    <MainMenu/>
    <BottomMenu/>
    <MobileMenu/>
    <SearchBlock/>
    {/* <SearchBlockWrapper/> */}
  </div>
);

export default connect(mapStateToProps)(Header);
