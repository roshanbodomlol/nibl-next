import { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Row, Col, Tooltip } from 'antd';
import {
  FiFacebook, FiYoutube, FiLinkedin, FiTwitter, FiInstagram
} from 'react-icons/fi';

import { API } from '../../../constants';
import { get } from '../../../services/api.services';
import ScrollUpAnchor from '../../ScrollUpAnchor';
import FooterMenu from './FooterMenu';
import Styles from './styles.module.scss';

const mapStateToProps = ({ navigation }) => ({ navigation });

const Footer = () => {
  const [content, setContent] = useState([]);
  const [socialLinks, setSocialLinks] = useState([]);

  const socialMediaLinks = socialLinks.map((link) => {
    switch (link.mediaType) {
      case 1: {
        return (
          <li>
            <Tooltip title={link.mediaTitle}>
              <a href={link.mediaUrl} target="_blank"><FiFacebook/></a>
            </Tooltip>
          </li>
        );
      }

      case 2: {
        return (
          <li>
            <Tooltip title={link.mediaTitle}>
              <a href={link.mediaUrl} target="_blank"><FiInstagram/></a>
            </Tooltip>
          </li>
        );
      }

      case 3: {
        return (
          <li>
            <Tooltip title={link.mediaTitle}>
              <a href={link.mediaUrl} target="_blank"><FiLinkedin/></a>
            </Tooltip>
          </li>
        );
      }

      case 4: {
        return (
          <li>
            <Tooltip title={link.mediaTitle}>
              <a href={link.mediaUrl} target="_blank"><FiYoutube/></a>
            </Tooltip>
          </li>
        );
      }

      case 5: {
        return (
          <li>
            <Tooltip title={link.mediaTitle}>
              <a href={link.mediaUrl} target="_blank"><FiTwitter/></a>
            </Tooltip>
          </li>
        );
      }

      default:
        return null;
    }
  });

  useEffect(() => {
    get(API.endPoints.FOOTER_INFO)
      .then(res => setContent(res.pageContent))
      .catch(e => console.log(e));

    get(API.endPoints.SOCIAL_LINKS)
      .then(res => setSocialLinks(res.pageContent));
  }, []);

  return (
    <div className={Styles.footerWrapper}>
      <div className="container">
        <div className={Styles.gridBlock}>
          <Row type="flex">
            {
              content.map((item) => (
                <Col sm={8} xs={24}>
                  <div className={Styles.block}>
                    <h4>{item.footerTitle}</h4>
                    <div className={Styles.footerBlock} dangerouslySetInnerHTML={{ __html: item.footerDescription }}/>
                  </div>
                </Col>
              ))
            }

            {/* <Col sm={8} xs={24}>
              <div className={Styles.block}>
                <h4>Corporate Office</h4>
                <span>
                  Durbar Marg, P.O. Box: 3412
                  <br/>
                  Kathmandu
                  <br/>
                  Nepal
                </span>
                <span>Email: Info@nibl.com.np</span>
                <span>Fax: (977-1) 4226349, 4228927</span>
              </div>
            </Col>
            <Col sm={8} xs={24}>
              <div className={Styles.block}>
                <h4>Help Desk</h4>
                <span>Toll Free Number : 1660-01-00070</span>
                <span>Mobile Number: +977 9851147829, +977 9851145829</span>
                <span>Email: cardhelpdesk@nibl.com.np</span>
              </div>
            </Col>
            <Col sm={8} xs={24}>
              <div className={Styles.block}>
                <h4>Contact Us</h4>
                <span>Durbar Marg, Kathmandu</span>
                <span>P.O. Box: 3412</span>
                <span>Phone: +977-1-4228229</span>
                <span>Toll Free Number : 1660-01-00070</span>
                <span>Fax: +977-1-4226349, 4228927</span>
                <span>SWIFT: NIBLNPKT</span>
                <span>Email: info@nibl.com.np </span>
              </div>
            </Col>
            <Col sm={8} xs={24}>
              <div className={Styles.block}>
                <h4>Reports and Downloads</h4>
                <div className={Styles.list}>
                  <Link href="/[page]/[subpage]/[child]" as="/about-us/downloads/downloads">
                    <a>Downloads</a>
                  </Link>
                  <Link href="/[page]/[subpage]/[child]" as="/about-us/investor-relation/annual-reports">
                    <a>Annual Reports</a>
                  </Link>
                  <Link href="/[page]/[subpage]/[child]" as="/about-us/investor-relation/basel-disclosure">
                    <a>Basel Disclosure</a>
                  </Link>
                  <Link href="/[page]/[subpage]/[child]" as="/about-us/investor-relation/financial-highlights">
                    <a>Financial Highlights</a>
                  </Link>
                </div>
              </div>
            </Col>
            <Col sm={8} xs={24}>
              <div className={Styles.block}>
                <h4>Other Links</h4>
                <div className={Styles.list}>
                  <Link href="/[page]/[subpage]/[child]" as="/personal-banking/faq/faq">
                    <a>FAQ</a>
                  </Link>
                  <Link href="/[page]/[subpage]/[child]" as="/personal-banking/rates-and-fees/interest-rates">
                    <a>Interest Rates</a>
                  </Link>
                </div>
              </div>
            </Col> */}
          </Row>
        </div>
      </div>
      <div className={Styles.bottomFooter}>
        <div className="container">
          <div className={Styles.footerFlex}>
            <FooterMenu/>
            <ul className={Styles.social}>
              {socialMediaLinks}
            </ul>
          </div>
        </div>
      </div>
      <ScrollUpAnchor/>
    </div>
  );
};

export default connect(mapStateToProps)(Footer);
