import React from 'react';
import Link from 'next/link';
import { connect } from 'react-redux';

import css from './styles.module.scss';

const mapStateToProps = ({ navigation }) => ({ navigation });

const FooterMenu = ({ navigation }) => {
  const { footer: footerRoutes } = navigation;

  const footerMenu = footerRoutes.map((route, index) => {
    if (route.pageTemplate === 'pageLink') {
      return <a key={`footer-menu-${index}`} href={route.linkUrl}>{route.name}</a>;
    }

    return (
      <Link key={`footer-menu-${index}`} href="/[page]" as={`/${route.slug}`}>
        <a>{route.name}</a>
      </Link>
    );
  });

  return (
    <div className={css.footerLinks}>
      {footerMenu}
      <div className={css.copyright}>
        <span>
          Copyright ©
          {new Date().getFullYear()}
          . Nepal Investment Bank Limited | All Rights Reserved.
        </span>
      </div>
    </div>
  );
};

export default connect(mapStateToProps)(FooterMenu);
