import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { message, notification } from 'antd';
import Link from 'next/link';

import { get } from '../../services/api.services';
import { API } from '../../constants';
import { generatePath } from '../../utilities/url.utils';
import { findPageByIDs } from '../../utilities/currentpage.util';
import Loading from '../Loading';

const mapStateToProps = ({ navigation }) => ({ navigation });

const Search = ({ navigation }) => {
  const [success, setSuccess] = useState(null);
  const [loading, setLoading] = useState(true);
  const router = useRouter();

  const { token } = router.query;

  useEffect(() => {
    get(generatePath(API.endPoints.SUBSCRIPTION_VERIFY, { token }))
      .then(() => {
        setSuccess(true);
        setLoading(false);
      })
      .catch((e) => {
        console.log('error', e);
        setLoading(false);
        setSuccess(false);
        notification.open({
          description: e
        });
      });
  }, []);

  return (
    <div className="white-gradient main--content">
      <div className="container">
        {
          loading
            ? <Loading/>
            : (
              <>
                <div className="heading">
                  {
                    success
                      ? <h1>Email Verification Successsful</h1>
                      : <h1>Email Verification Failed</h1>
                  }
                </div>
                <div>
                  <Link href="/">
                    <a>Home</a>
                  </Link>
                </div>
              </>
            )
        }
      </div>
    </div>
  );
};

export default connect(mapStateToProps)(Search);
