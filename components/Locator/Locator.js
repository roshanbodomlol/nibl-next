import React, { Component } from 'react';
import { string } from 'prop-types';
import { GoogleMap, LoadScriptNext, Marker } from '@react-google-maps/api';
import { connect } from 'react-redux';
import { FaCaretRight } from 'react-icons/fa';
import classnames from 'classnames';
import { sortBy } from 'lodash';
import withSizes from 'react-sizes';
import { BsSearch } from 'react-icons/bs'

import { showGlobalSnack } from '../../redux/actions/snack.actions';
import { withTranslation } from '../../locales/i18n';
import ExpansionPanel from '../ExpansionPanel';
import './Locator.scss';

class Locator extends Component {
  state = {
    search: '',
    markers: [],
    activeMarker: null,
    filteredLocations: [],
    sidebarCollapsed: false
  };

  mapRef = React.createRef();

  bounds = null;

  libraries = ['geometry'];

  populateMarkers = () => {
    let { locations } = this.props;
    const { activeMarker } = this.state;

    this.bounds = new window.google.maps.LatLngBounds();

    locations = sortBy(locations, [(o) => o.name]);

    let filteredList;

    if (activeMarker !== null) {
      filteredList = locations.filter(location => activeMarker === location.id);
    } else {
      filteredList = locations.filter(e => e.isValid);
    }

    const markerList = filteredList.map((location) => {
      const loc = new window.google.maps.LatLng(location.lat, location.lng);
      this.bounds.extend(loc);

      return (
        <Marker
          key={`marker-${location.id}`}
          onClick={() => {}}
          position={{
            lat: location.lat,
            lng: location.lng
          }}
        />
      );
    });

    this.setState({ markers: markerList }, () => {
      this.mapRef.fitBounds(this.bounds);
    });
  }

  setLocation = (locationIndex) => {
    this.setState({ activeMarker: locationIndex }, () => {
      this.populateMarkers();
    });
  }

  setViewAll = () => {
    this.setState({ activeMarker: null }, () => {
      this.populateMarkers();
    });
  }

  filterLocations = (search) => {
    const { locations } = this.props;
    const filteredLocations = locations.filter(location => location.address.toLowerCase().includes(search.toLowerCase()));
    this.setState({ filteredLocations });
  }

  getNearestLocation = () => {
    const { locations, dispatch } = this.props;
    // const { filteredLocations } = this.state;

    // const locationsResult = filteredLocations.length > 0 ? filteredLocations : locations;
    const locationsResult = locations;

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        const pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        const locationsWithDistance = locationsResult.map((location) => {
          const from = new window.google.maps.LatLng(pos.lat, pos.lng);
          const to = new window.google.maps.LatLng(location.lat, location.lng);
          return {
            ...location,
            minDistance: window.google.maps.geometry.spherical.computeDistanceBetween(from, to)
          }
        });
        
        // floor distances
        locationsWithDistance.forEach((location) => {
          location.flooredMinDistance = Math.floor(location.minDistance);
        });

        const validLocationsWithDistances = locationsWithDistance.filter(location => !isNaN(location.flooredMinDistance));
        
        const flooredDistances = validLocationsWithDistances.map(location => location.flooredMinDistance);

        const minDistance = Math.min(...flooredDistances);

        validLocationsWithDistances.forEach((location) => {
          if (location.flooredMinDistance === minDistance) {
            this.setLocation(location.id);
          }
        });
      });
    } else {
      dispatch(showGlobalSnack('normal', 'Sorry, your browser does not support location. Please update your browser'));
    }
  }

  handleSearchSubmit = (e) => {
    const { search } = this.state;
    e.preventDefault();
    this.filterLocations(search);
  }

  componentDidMount() {
    this.filterLocations('');
  }
  
  render() {
    const { title, width, t } = this.props;
    const {
      markers, activeMarker, filteredLocations, sidebarCollapsed,
      search
    } = this.state;

    const locationsResult = sortBy(filteredLocations, [o => o.name]);

    const locationList = locationsResult.map((location) => (
      {
        primary: (
          <div
            key={`location-item-primary-${location.id}`}
            role="button"
            tabIndex="-1"
            onClick={() => {
              if (location.isValid) {
                this.setLocation(location.id);
                if (width && width < 500) {
                  this.setState({ sidebarCollapsed: true });
                }
              }
            }}
            key={`location-title-${location.id}`}
          >
            {location.name}
          </div>
        ),
        secondary:
        (
          <div className="secondary" key={`location-item-secondary-${location.id}`}>
            <div>
              { location.address && <span>{location.address}</span> }
              { location.phoneNumber && <span>{t('locator_tel')}: {location.phoneNumber}</span> }
              { location.faxNumber && <span>{t('locator_fax')}: {location.faxNumber}</span> }
              { location.holidayTime && <span>{t('locator_holidays')}: {location.holidayTime}</span> }
              { location.eveningCounter && <span>{t('locator_eveningCounter')}: {location.eveningCounter}</span> }
              { location.normalWorkingWeekdays && <span>{t('locator_workingWeekdays')}: {location.normalWorkingWeekdays}</span> }
              { location.normalWorkingFridays && <span>{t('locator_workingFridays')}: {location.normalWorkingFridays}</span> }
              { location.eveningCounterFridays && <span>{t('locator_eveningCounterFridays')}: {location.eveningCounterFridays}</span> }
              { location.branchManager && <span>{t('locator_branchManager')}: {location.branchManager}</span> }
              { location.email && <span>{t('global_email')}: <a href={`mailto:${location.email}`}>{location.email}</a></span> }
            </div>
          </div>
        )
      }
    ));

    return (
      <div id="locator-map" className={sidebarCollapsed ? 'sidebar-collapsed' : ''}>
        <div className="map-sidebar">
          <div className="search-map-wrap">
            <form onSubmit={this.handleSearchSubmit}>
              <input
                name="search-term"
                type="text"
                placeholder="Search a location"
                className="search-map"
                value={search}
                onChange={(e) => {
                  const val = e.target.value;
                  if (val === '') {
                    this.filterLocations('');
                  }
                  this.setState({ search: val })
                }}
              />
              <BsSearch onClick={() => this.filterLocations(search)}/>
              <button style={{ opacity: 0, visibility: 'hidden', height: 0 }} type="submit">Search</button>
            </form>
          </div>
          <div
            className="title"
            role="button"
            tabIndex="-1"
            onClick={this.getNearestLocation}
          >
            {title}
          </div>
          <div className="locations">
            <ExpansionPanel
              isOrdered={false}
              compact
              items={locationList}
            />
            {
              activeMarker !== null && (
                <button type="button" onClick={this.setViewAll} className="view-all-btn">
                  {t('global_viewAll')}
                </button>
              )
            }
          </div>
          <div
            className={classnames('sidebar-toggle', { collapsed: sidebarCollapsed })}
            role="button"
            onClick={() => this.setState((prevState) => ({ sidebarCollapsed: !prevState.sidebarCollapsed }))}
            tabIndex="-1"
          >
            <FaCaretRight/>
          </div>
        </div>
        <div className="map-holder">
          <LoadScriptNext
            id="script-loader"
            googleMapsApiKey={process.env.GOOGLE_API_KEY}
            libraries={this.libraries}
          >
            <GoogleMap
              id="example-map"
              mapContainerStyle={{ width: '100%', height: '100%' }}
              defaultZoom={17}
              // center={{
              //   lat: locations[0].lat,
              //   lng: locations[0].lng
              // }}
              onLoad={(map) => {
                this.mapRef = map;
                map.setOptions({
                  maxZoom: 21,
                  minZoom: 7
                });
                this.bounds = new window.google.maps.LatLngBounds();
                this.populateMarkers();
              }}
            >
              {markers}
            </GoogleMap>
          </LoadScriptNext>
        </div>
      </div>
    );
  }
}

Locator.propTypes = {
  title: string.isRequired
};

export default connect()(withTranslation('common')(withSizes(({ width }) => ({ width }))(Locator)));
