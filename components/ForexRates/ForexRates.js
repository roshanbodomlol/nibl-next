import { useState, useEffect } from 'react';
import moment from 'moment';
import {
  Spin, DatePicker, Row, Col, notification
} from 'antd';
import Recaptcha from 'react-recaptcha';

import { withTranslation } from '../../locales/i18n';
import ContentHeader from '../ContentHeader';
import TableBody from '../TableBody';
import { generatePath } from '../../utilities/url.utils';
import { get } from '../../services/api.services';
import { API } from '../../constants';
import css from './ForexRates.module.scss';
import TabNav from '../TabNav';
import TabContent from '../TabContent';
import { groupBy } from 'lodash';
import { numeric, removeSpecialCharacters } from '../../utilities/string.utils';
import { isEmail } from '../../utilities/isEmail.util';


const ForexRates = ({ t }) => {
  const [loading, setLoading] = useState(false);
  const [rates, setRates] = useState([]);
  const [name, setName] = useState('');
  const [mobileNumber, setMobileNumber] = useState('');
  const [email, setEmail] = useState('');
  const [verified, setVerified] = useState(true);
  const [activeItemKey, setActiveItemKey] = useState('rates');

  const items = [
    {
      name: 'Exchange Rates',
      key: 'rates'
    },
    {
      name: 'Subscribe',
      key: 'subscribe'
    }
  ];

  const getRates = (d) => {
    const date = d.format('YYYY-MM-DD');
    get(generatePath(API.endPoints.FOREX_DATE, { date }))
      .then((response) => {
        setRates(response.pageContent);
        setLoading(false);
      })
      .catch();
  };

  const getCurrentRates = () => {
    get(API.endPoints.FOREX_DATE_CURRENT)
      .then((response) => {
        setRates(response.pageContent);
        setLoading(false);
      })
      .catch();
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (!verified) {
      return false;
    }

    setLoading(true);

    setName(removeSpecialCharacters(name));

    if (!isEmail(email)) {
      notification.open({
        description: 'Please enter a valid email address'
      });

      return false;
    }

    if (!mobileNumber.match(new RegExp(/^\d+$/)) && mobileNumber.length !== 10) {
      notification.open({
        description: 'Please enter a valid mobile number'
      });

      return false;
    }

    const data = {
      name: removeSpecialCharacters(name),
      mobileNumber,
      email,
    };

    fetch(`${process.env.API_URL}/${API.endPoints.FOREX_SUBSCRIBE}`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(response => response.json())
      .then((response) => {
        if (response.status) {
          notification.open({
            description: 'Submitted successfully'
          });
          setLoading(false);
        } else if (response.error === 'Email address already exists in the system') {
          notification.open({
            description: 'The email you provided already exists'
          });
          setLoading(false);
        } else {
          console.error(response.error);
          notification.open({
            description: 'Error occurred, please try again'
          });
          setLoading(false);
        }
      })
      .catch((error) => {
        console.error(error);
        notification.open({
          description: 'Error occurred, please try again'
        });
        setLoading(false);
      });
  };

  useEffect(() => {
    setLoading(true);
    getCurrentRates();
  }, []);

  // const occupations = [
  //   'Accounting/Finance',
  //   'Computer/IT related',
  //   'Consulting',
  //   'Customer Service/support',
  //   'Education/training',
  //   'Engineering',
  //   'Executive/senior management',
  //   'Farming',
  //   'General admin/supervisory',
  //   'Government/military',
  //   'Homemaker',
  //   'Manufacturing/production',
  //   'Professional-medical',
  //   'Research and development',
  //   'Retired/Ex-servicemen',
  //   'Sales/marketing/advertising',
  //   'Self-employed/owner',
  //   'Student',
  //   'Tradesman/craftsman',
  //   'Unemployed/between jobs'
  // ];

  const properRates = groupBy(rates, rate => rate.forexTime1);

  let dailyRatesGroups = [];

  Object.keys(properRates).forEach((key) => {
    const groupObj = properRates[key];
    const group = {
      time: moment(key).format('h:mm:ss a'),
      dailyRatesRows: groupObj.map((item, index) => {
        const decimalPlaces = (item.forexCurrency === 'JPY' || item.forexCurrency === 'KRW') ? 4 : 2;
        return (
          <TableBody key={`rate-row-${item.forexId}`}>
            <div className={css.tableGrid}>
              <Row>
                <Col span={2}>
                  <span>{index + 1}</span>
                </Col>
                <Col span={7}>
                  <span className={css.flag}>
                    <img src={`/svg/flags/${item.forexCurrency}.svg`} alt=""/>
                    {item.forexCurrency}
                  </span>
                </Col>
                <Col span={5}>
                  <span className={css.buying}>{Number(item.forexCash).toFixed(decimalPlaces)}</span>
                </Col>
                <Col span={5}>
                  <span className={css.others}>{Number(item.forexOthers).toFixed(decimalPlaces)}</span>
                </Col>
                <Col span={5}>
                  <span className={css.selling}>{Number(item.forexRates).toFixed(decimalPlaces)}</span>
                </Col>
              </Row>
            </div>
          </TableBody>
        );
      })
    };

    dailyRatesGroups.push(group);
  });

  return (
    <>
      <TabNav
        items={items}
        activeItemKey={activeItemKey}
        onClick={key => setActiveItemKey(key)}
      />
      <div style={{ padding: '30px 0' }}>
        <TabContent>
          <div isActive={activeItemKey === 'subscribe'}>
            <Row>
              <Col sm={{ span: 12, offset: 6 }} xs={24}>
                <div className={css['forex-subscribe']}>
                  <h1>Subscribe to get the latest updates on our rates</h1>
                  <form onSubmit={handleSubmit}>
                    <div>
                      <label htmlFor="name">Name *</label>
                      <input required type="text" id="name" value={name} onChange={e => setName(e.target.value)}/>
                    </div>
                    {/* <div>
                      <label htmlFor="address">Address *</label>
                      <input required type="text" id="address" value={address} onChange={e => setAddress(e.target.value)}/>
                    </div>
                    <div>
                      <label htmlFor="contact">Contact Number</label>
                      <input type="tel" id="contactNumber" value={contactNumber} onChange={e => setContactNumber(e.target.value)}/>
                    </div> */}
                    <div>
                      <label htmlFor="mobile">10 Digit Mobile Number</label>
                      <input required type="tel" id="mobileNumber" value={mobileNumber} onChange={e => setMobileNumber(e.target.value)}/>
                    </div>
                    <div>
                      <label htmlFor="email">Email Address *</label>
                      <input required type="email" id="email" value={email} onChange={e => setEmail(e.target.value)}/>
                    </div>
                    <div>
                      <Recaptcha
                        sitekey="6LdpNc8ZAAAAAKoBxHtA3KLhoQpMfoEiejUQ1-Z1"
                        render="explicit"
                        onloadCallback={() => setLoading(false)}
                        verifyCallback={() => setVerified(true)}
                        expiredCallback={() => setVerified(false)}
                      />
                    </div>
                    <div style={{ display: 'flex', justifyContent: 'center' }}>
                      <button type="submit" disabled={loading} className={css.button}>{t('global_send')}</button>
                    </div>
                  </form>
                </div>
              </Col>
            </Row>
          </div>
          <div isActive={activeItemKey === 'rates'}>
            <div className={css.top}>
              <p>{t('forex_selectDates')}</p>
              <div className={css.datePicker}>
                <DatePicker onChange={date => getRates(date)} allowClear={false}/>
              </div>
            </div>
            {
              rates[0] && (
                <div className={css.date}>
                  On {moment(rates[0].forexTime1).format('dddd, MMMM DD, YYYY')}
                </div>
              )
            }
            {
              loading
                ? (
                  <div className="page-loader __transparent __medium">
                    <Spin/>
                  </div>
                )
                : (
                  <>
                    {
                      dailyRatesGroups.length > 0
                        ? (
                          <>
                            {
                              dailyRatesGroups.map((rateGroup) => (
                                <div className={css.forexGroup}>
                                  <div className={css.date}>{rateGroup.time}</div>
                                  <ContentHeader grey>
                                    <div className={css.tableGrid}>
                                      <Row>
                                        <Col span={2}>
                                          <span>S.No</span>
                                        </Col>
                                        <Col span={7}>
                                          <span>{t('global_currency')}</span>
                                        </Col>
                                        <Col span={5}>
                                          <span className={css.buying}>{t('template_forex_buying')}</span>
                                        </Col>
                                        <Col span={5}>
                                          <span className={css.others}>{t('forex_buyingRate')}</span>
                                        </Col>
                                        <Col span={5}>
                                          <span className={css.selling}>{t('template_forex_selling')}</span>
                                        </Col>
                                      </Row>

                                    </div>
                                  </ContentHeader>
                                  {rateGroup.dailyRatesRows}
                                </div>
                              ))
                            }
                          </>
                        )
                        : (
                          <div className={css.empty}>
                            No rates for this date. Please select another date
                          </div>
                        )
                    }
                  </>
                )
            }
          </div>
        </TabContent>
      </div>
    </>
  );
};

export default withTranslation('common')(ForexRates);
