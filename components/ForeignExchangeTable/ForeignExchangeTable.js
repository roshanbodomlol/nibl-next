import React, { Component } from 'react';
import moment from 'moment';
import MomentUtils from '@date-io/moment';
import { MuiPickersUtilsProvider, DatePicker } from 'material-ui-pickers';
import DownIcon from '@material-ui/icons/ExpandMore';

import Styles from './foreignExTbl.module.scss';

// Components
import ContentHeader from '../ContentHeader';
import TableBody from '../TableBody';

// Images
import flag from '../../assets/img/usa.png';

class ForeignExchangeTable extends Component {
  constructor() {
    super();
    this.state = {
      selectedDate: moment()
    };
    this.pickerDialogRef = React.createRef();
  }

  openPickerDialog = () => {
    this.pickerDialogRef.current.open();
  }

  handleDateChange = (date) => {
    this.setState({ selectedDate: date });
  };

  openPickerDialog = () => {
    this.pickerDialogRef.current.open();
  }

  render() {
    const { selectedDate } = this.state;
    return (
      <div className={Styles.wrapper}>
        <div className={Styles.top}>
          <p>Select the date to view exchange rates</p>
          <div className={Styles.datePicker}>
            <div className={Styles.fakeDateInput}>
              <input
                required
                readOnly
                type="text"
                value={selectedDate && moment(selectedDate).format('YYYY')}
                style={{ width: 75 }}
                onClick={this.openPickerDialog}
              />
              <DownIcon color="primary" style={{ fontSize: 16 }}/>
            </div>
            <div className={Styles.fakeDateInput}>
              <input
                required
                readOnly
                type="text"
                value={selectedDate && moment(selectedDate).format('MM')}
                onClick={this.openPickerDialog}
              />
              <DownIcon color="primary" style={{ fontSize: 16 }}/>
            </div>
            <div className={Styles.fakeDateInput}>
              <input
                required
                readOnly
                type="text"
                value={selectedDate && moment(selectedDate).format('DD')}
                onClick={this.openPickerDialog}
              />
              <DownIcon color="primary" style={{ fontSize: 16 }}/>
            </div>
          </div>
          <div className="datePickerHide">
            <MuiPickersUtilsProvider utils={MomentUtils}>
              <DatePicker
                ref={this.pickerDialogRef}
                margin="normal"
                label="Date Of Birth"
                value={selectedDate}
                onChange={this.handleDateChange}
                style={{
                  width: '100%'
                }}
              />
            </MuiPickersUtilsProvider>
          </div>
          </div>
          <ContentHeader>
          <div className={Styles.tableGrid}>
            <span>S.No</span>
            <span>Currency</span>
            <span className={Styles.buying}>Buying Cash</span>
            <span className={Styles.others}>Buying Others</span>

            <span className={Styles.selling}>Selling Rate</span>
          </div>
          </ContentHeader>
          <TableBody>
          <div className={Styles.tableGrid}>
            <span>1</span>
            <span className={Styles.flag}>
              <img src={flag} alt=""/>
              USD
            </span>
            <span className={Styles.buying}>109.80</span>
            <span className={Styles.others}>110.35</span>
            <span className={Styles.selling}>110.95</span>
          </div>
          </TableBody>
          <TableBody>
          <div className={Styles.tableGrid}>
            <span>1</span>
            <span className={Styles.flag}>
              <img src={flag} alt=""/>
              USD
            </span>
            <span className={Styles.buying}>109.80</span>
            <span className={Styles.others}>110.35</span>
            <span className={Styles.selling}>110.95</span>
          </div>
          </TableBody>
          <TableBody>
          <div className={Styles.tableGrid}>
            <span>1</span>
            <span className={Styles.flag}>
              <img src={flag} alt=""/>
              USD
            </span>
            <span className={Styles.buying}>109.80</span>
            <span className={Styles.others}>110.35</span>
            <span className={Styles.selling}>110.95</span>
          </div>
          </TableBody>
          <TableBody>
          <div className={Styles.tableGrid}>
            <span>1</span>
            <span className={Styles.flag}>
              <img src={flag} alt=""/>
              USD
            </span>
            <span className={Styles.buying}>109.80</span>
            <span className={Styles.others}>110.35</span>
            <span className={Styles.selling}>110.95</span>
          </div>
          </TableBody>
          <TableBody>
          <div className={Styles.tableGrid}>
            <span>1</span>
            <span className={Styles.flag}>
              <img src={flag} alt=""/>
              USD
            </span>
            <span className={Styles.buying}>109.80</span>
            <span className={Styles.others}>110.35</span>
            <span className={Styles.selling}>110.95</span>
          </div>
          </TableBody>
          <TableBody>
          <div className={Styles.tableGrid}>
            <span>1</span>
            <span className={Styles.flag}>
              <img src={flag} alt=""/>
              USD
            </span>
            <span className={Styles.buying}>109.80</span>
            <span className={Styles.others}>110.35</span>
            <span className={Styles.selling}>110.95</span>
          </div>
          </TableBody>
      </div>
    );
  }
}

export default ForeignExchangeTable;
