import React, { Component } from 'react';
import { GoSearch } from 'react-icons/go';
import Router from 'next/router';
import { message, notification } from 'antd';
import { connect } from 'react-redux';

import { hideSearchBlock } from '../../redux/actions/common.actions';
import styles from './SearchBlock.module.scss';

class SearchBlock extends Component {
  state = {
    searchTerm: ''
  };

  componentDidMount() {
    document.getElementById('search-input').focus();
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { searchTerm } = this.state;
    const { dispatch } = this.props;

    if (searchTerm) {
      if (searchTerm.length < 3) {
        notification.open({
          description: 'Please enter at least 3 characters.'
        });
      } else {
        Router.push('/search/[q]', `/search/${encodeURI(searchTerm)}`);
        dispatch(hideSearchBlock());
      }
    }
  };

  render() {
    const { searchTerm } = this.state;

    return (
      <div className={styles.wrapper}>
        <div className={styles.inner}>
          <div className={styles.bar}>
            <div className={styles.icon}>
              <GoSearch/>
            </div>
            <div className={styles.entry}>
              <form onSubmit={this.handleSubmit}>
                <input
                  id="search-input"
                  type="text"
                  placeholder="Type and press Enter to search"
                  value={searchTerm}
                  onChange={e => this.setState({ searchTerm: e.target.value })}
                />
                <button type="submit" style={{ display: 'none' }}/>
              </form>
            </div>
          </div>
          {/* <div className={styles.results}>
            {
              searchResults.length > 0 && (
                <div className={styles.searchResults}>
                  {searchList}
                </div>
              )
            }
            {
              popularList.length > 0 && (
                <div className={styles.popular}>
                  <div className={styles.title}>Popular Searches</div>
                  <div className={styles.popularList}>
                    {popularList}
                  </div>
                </div>
              )
            }
          </div> */}
        </div>
      </div>
    );
  }
}

export default connect()(SearchBlock);
