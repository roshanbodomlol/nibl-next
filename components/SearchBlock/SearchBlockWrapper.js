import React from 'react';
import { connect } from 'react-redux';
import { objectOf, any } from 'prop-types';
import { CSSTransition } from 'react-transition-group';
import ClickAwayListener from 'react-click-away-listener';

import { hideSearchBlock } from '../../redux/actions/common.actions';
import SearchBlock from './SearchBlock';

const mapStateToProps = ({ common }) => ({ common });

const SearchBlockWrapper = ({ common, dispatch }) => (
  <CSSTransition
    in={common.searchBlockOpen}
    timeout={300}
    unmountOnExit
  >
    <ClickAwayListener onClickAway={() => { dispatch(hideSearchBlock()); }}>
      <SearchBlock/>
    </ClickAwayListener>
  </CSSTransition>
);

SearchBlockWrapper.propTypes = {
  common: objectOf(any).isRequired
};

export default connect(mapStateToProps)(SearchBlockWrapper);
