import { useState } from 'react';
import { MdExpandMore as ExpandIcon } from 'react-icons/md';
import cn from 'classnames';

import { i18n, withTranslation } from '../../locales/i18n';
import './LanguageSwitcher.scss';
import { GLOBALS } from '../../constants';

const LanguageSwitcher = ({ mobile }) => {
  const { language } = i18n;
  const [open, setOpen] = useState(false);

  const switchLanguage = (locale) => {
    localStorage.setItem(GLOBALS.LANG_KEY, locale);
    i18n.changeLanguage(locale);
    setOpen(false);
  };

  return (
    <div className={cn('language-switcher', { mobile })}>
      <div className="active" onClick={() => { setOpen(!open); }}>
        { language === 'en' ? 'EN' : 'NP' }
        <ExpandIcon color="primary"/>
      </div>
      <div className={cn('more', { open })}>
        <ul>
          <li role="button" tabIndex="-1" onClick={() => switchLanguage('en')}>EN</li>
          <li role="button" tabIndex="-1" onClick={() => switchLanguage('ne')}>NP</li>
        </ul>
      </div>
    </div>
  );
};

export default withTranslation()(LanguageSwitcher);
