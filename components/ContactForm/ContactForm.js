import React from 'react';
import PropTypes from 'prop-types';

import Styles from './contactForm.module.scss';

const ContactForm = ({ children }) => (
  <div className={Styles.wrapper}>
    <form className={Styles.form}>
      {children}
    </form>
  </div>
);

ContactForm.propTypes = {
  children: PropTypes.node.isRequired
};

export default ContactForm;
