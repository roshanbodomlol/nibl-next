import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ModalVideo from 'react-modal-video';
import 'react-modal-video/scss/modal-video.scss';

// import modalStyles from '../../styles/modal-video.scss';

// Component
import VideoCard from '../VideoCard';

class VideoModal extends Component {
  constructor() {
    super();
    this.state = {
      isOpen: false
    };
    this.openModal = this.openModal.bind(this);
  }

  openModal = () => {
    this.setState({ isOpen: true });
  }

  render() {
    const { isOpen } = this.state;
    const { videoId } = this.props;
    return (
      <>
        <ModalVideo channel="youtube" isOpen={isOpen} videoId={videoId} onClose={() => this.setState({ isOpen: false })} />
        <VideoCard
          onClick={this.openModal}
          videoThumb={`https://img.youtube.com/vi/${videoId}/0.jpg`}
          videoTitle="Lorem ipsum dolor sit amet, consectetuer"
        />
      </>
    );
  }
}

VideoModal.propTypes = {
  videoId: PropTypes.string.isRequired
};

export default VideoModal;
