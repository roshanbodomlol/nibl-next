import { useState } from 'react';
import Slider from 'rc-slider';
import { Select } from 'antd';

import { withTranslation } from '../../locales/i18n';
import GLOBALS from '../../constants/globals.constants';
import Button from '../ButtonRedBorder';

const { Option } = Select;
const { CALCULATOR } = GLOBALS;
const {
  INTEREST_MIN,
  INTEREST_MAX,
  TIME_MIN_MONTHS,
  TIME_MAX_MONTHS,
  TIME_MIN_YEARS,
  TIME_MAX_YEARS,
  DEFAULT_TIME_UNIT
} = CALCULATOR;

const Calculator = ({
  calculate, answerLabel, t, amountMin, amountMax
}) => {
  const AMOUNT_MIN = amountMin;
  const AMOUNT_MAX = amountMax;
  const [amount, setAmount] = useState(AMOUNT_MIN);
  const [interest, setInterest] = useState(INTEREST_MIN);
  const [time, setTime] = useState(DEFAULT_TIME_UNIT === 'm' ? TIME_MIN_MONTHS : TIME_MIN_YEARS);
  const [edited, setEdited] = useState(false);
  const [amountEdit, setAmountEdit] = useState(AMOUNT_MIN);
  const [timeEdit, setTimeEdit] = useState(DEFAULT_TIME_UNIT === 'm' ? TIME_MIN_MONTHS : TIME_MIN_YEARS);
  const [interestEdit, setInterestEdit] = useState(INTEREST_MIN);
  const [timeUnit, setTimeUnit] = useState(DEFAULT_TIME_UNIT);
  const [amountError, setAmountError] = useState(false);
  const [timeError, setTimeError] = useState(false);
  const [interestError, setInterestError] = useState(false);

  const handleCalculate = () => {
    let error = false;
    setAmountError(false);
    if (!amountEdit || amountEdit < AMOUNT_MIN || amountEdit > AMOUNT_MAX) {
      setAmountError(true);
      error = true;
    } else {
      setAmount(amountEdit);
    }

    if (!interest || interest < INTEREST_MIN || interest > INTEREST_MAX) {
      setTimeError(true);
      error = true;
    } else {
      setInterest(interestEdit);
    }

    if (!time || (timeUnit === 'y' && (time < TIME_MIN_YEARS || time > TIME_MAX_YEARS)) || (timeUnit === 'm' && (time < TIME_MIN_MONTHS || time > TIME_MAX_MONTHS))) {
      setInterestError(true);
      error = true;
    } else {
      setTime(timeEdit);
    }

    if (!error) setEdited(false);
  };

  const answer = calculate(amount, timeUnit === 'y' ? time : time / 12, interest);

  return (
    <>
      <div className="savings">
        {t('calculator_amount')}: {t('global_rs')}
        <input
          className="text-label"
          value={edited ? amountEdit : amount}
          type="number"
          onChange={(e) => {
            setAmountEdit(e.target.value);
            setEdited(true);
          }}
        />
        {
          amountError && <p className="error">{t('calculator_invalidAmount')}</p>
        }
        <Slider
          value={amount}
          min={AMOUNT_MIN}
          max={AMOUNT_MAX}
          onChange={(val) => {
            setAmount(val);
            setAmountEdit(val);
            setAmountError(false);
          }}
        />
        <div className="range">
          <span>{t('global_rs')} {AMOUNT_MIN}</span>
          <span>{t('global_rs')} {AMOUNT_MAX}</span>
        </div>
      </div>
      <div className="time">
        Time:
        <input
          type="number"
          className="text-label"
          value={edited ? timeEdit : time}
          style={{ width: 90 }}
          onChange={(e) => {
            setEdited(true);
            setTimeEdit(e.target.value);
          }}
        />
        <Select
          defaultValue={timeUnit}
          onChange={(val) => {
            if (val === 'y') {
              const newTime = time < 12 ? 1 : Math.floor(time / 12);
              setTimeUnit(val);
              setTime(newTime);
              setTimeEdit(newTime);
            } else {
              const newTime = Math.floor(time * 12);
              setTimeUnit(val);
              setTime(newTime);
              setTimeEdit(newTime);
            }
          }}
        >
          <Option value="y">{t('global_years')}</Option>
          <Option value="m">{t('global_months')}</Option>
        </Select>
        {
          timeError && <p className="error">{t('calculator_invalidTime')}</p>
        }
        <Slider
          value={time}
          min={timeUnit === 'm' ? TIME_MIN_MONTHS : TIME_MIN_YEARS}
          max={timeUnit === 'm' ? TIME_MAX_MONTHS : TIME_MAX_YEARS}
          onChange={val => {
            setTime(val);
            setTimeEdit(val);
            setTimeError(false);
          }}
        />
        <div className="range">
          <span>{timeUnit === 'm' ? TIME_MIN_MONTHS : TIME_MIN_YEARS} {timeUnit}</span>
          <span>{timeUnit === 'm' ? TIME_MAX_MONTHS : TIME_MAX_YEARS} {timeUnit}</span>
        </div>
      </div>
      <div className="interest">
        {t('global_rate')}:
        <input
          className="text-label"
          value={edited ? interestEdit : interest}
          style={{ width: 45 }}
          onChange={(e) => {
            setEdited(true);
            setInterestEdit(e.target.value);
          }}
        /> %
        {
          interestError && <p className="error">{t('calculator_invalidInterestRate')}</p>
        }
        <Slider
          value={interest}
          min={INTEREST_MIN}
          max={INTEREST_MAX}
          onChange={(val) => {
            setInterest(val);
            setInterestEdit(val);
            setInterestError(false);
          }}
        />
        <div className="range">
          <span>{INTEREST_MIN} %</span>
          <span>{INTEREST_MAX} %</span>
        </div>
      </div>
      {
        edited && (
          <div className="calc-button">
            <Button title="Calculate" onClick={handleCalculate}/>
          </div>
        )
      }
      <div className="calc-bottom">
        { answerLabel }: {t('global_rs')}
        <span className="text-label _result">
          {answer.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
        </span>
      </div>
    </>
  );
};

export default withTranslation('common')(Calculator);
