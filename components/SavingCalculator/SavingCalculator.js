import { useState } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import 'rc-slider/assets/index.css';

import { BigTitleBlock } from '../Layouts/Block';
import { EMI, RD } from './formulas';
import Calculator from './Calculator';
import './SavingCalculator.scss';

const SavingCalculator = ({ enterCount, compact }) => {
  const [activeOption, setActiveOption] = useState('emi');

  const clacClasses = cn('main', {
    'animated fadeIn': enterCount > 0
  });
  const resultClasses = cn('result', {
    'animated fadeIn': enterCount > 0
  });
  const wrapperClasses = cn('savings-calculator', {
    compact
  });

  let resultTitle = 'Your savings goal could be';
  if (activeOption === 'rd') {
    resultTitle = 'To reach your savings goal it will take:';
  }

  return (
    <div className={wrapperClasses}>
      <div className={clacClasses} style={{ animationDelay: '.3s' }}>
        <div className="head">
          <div
            className={cn('head-tab', { active: activeOption === 'emi' })}
            onClick={() => setActiveOption('emi')}
            role="button"
            tabIndex="-1"
          >
            EMI
          </div>
          <div
            className={cn('head-tab', { active: activeOption === 'rd' })}
            onClick={() => setActiveOption('rd')}
            role="button"
            tabIndex="-1"
          >
            Recurring Deposits
          </div>
        </div>
        <div className="body">
          <div className={activeOption === 'emi' ? 'calculator-inner active' : 'calculator-inner'}>
            <Calculator
              calculate={EMI}
              answerLabel="EMI Calculation"
              amountMin={1000}
              amountMax={10000000}
            />
          </div>
          <div className={activeOption === 'rd' ? 'calculator-inner active' : 'calculator-inner'}>
            <Calculator
              calculate={RD}
              amountMin={333}
              answerLabel="Recurring Deposits"
              amountMax={1000000000}
            />
          </div>
        </div>
        {
          !compact && (
            <div className={resultClasses} style={{ animationDelay: '.6s' }}>
              <BigTitleBlock title={resultTitle}>
                asd
              </BigTitleBlock>
            </div>
          )
        }
      </div>
    </div>
  );
};

SavingCalculator.propTypes = {
  enterCount: PropTypes.number.isRequired,
  compact: PropTypes.bool
};

SavingCalculator.defaultProps = {
  compact: false
};

export default SavingCalculator;
