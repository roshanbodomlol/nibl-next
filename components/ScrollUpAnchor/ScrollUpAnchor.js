import React, { Component } from 'react';
import classnames from 'classnames';
import { FiChevronsUp } from 'react-icons/fi';
import { debounce } from 'lodash';
import anime from 'animejs';
import $ from 'jquery';

import { GLOBALS } from '../../constants';
import css from './scrollup.module.scss';

class ScrollUpAnchor extends Component {
  state = {
    active: false
  }

  componentDidMount() {
    this.scrollToTop();

    window.addEventListener('scroll', debounce(() => {
      const scrollTop = window.scrollY;
      if (scrollTop > GLOBALS.SCROLL_ANCHOR_THRESHOLD) {
        this.setState({ active: true });
      } else {
        this.setState({ active: false });
      }
    }), GLOBALS.SCROLL_ANCHOR_DEBOUNCE);
  }

  scrollToTop = () => {
    // window.scrollTo({ top: 0, behavior: 'smooth' });
    $('body,html').animate(
      {
        scrollTop: 0
      },
      800
    );
  }

  render() {
    const { active } = this.state;

    const classes = classnames(css.wrapper, {
      [css.active]: active
    });

    return (
      <div
        className={classes}
        role="button"
        tabIndex="-1"
        onClick={this.scrollToTop}
      >
        <FiChevronsUp/>
      </div>
    );
  }
}

export default ScrollUpAnchor;
