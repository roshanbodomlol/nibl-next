import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Link from 'next/link';

import { withTranslation } from '../../locales/i18n';
import Styles from './cardImageAndText.module.scss';

const CardImageAndText = ({
  image, title, excerpt, link, t
}) => (
  <div className={Styles.wrapper}>
    <img src={image} alt=""/>
    <div className={classnames(Styles.cardText, '_card-text')}>
      <div className={Styles.title}>
        <span>{title}</span>
      </div>
      <p>
        {excerpt}
      </p>
    </div>
    <div className={Styles.link}>
      <Link href="/news-content/[id]" as={link}>
        <a>{t('global_readMore')}</a>
      </Link>
    </div>
  </div>
);

CardImageAndText.propTypes = {
  image: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  excerpt: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired
};

export default withTranslation('common')(CardImageAndText);
