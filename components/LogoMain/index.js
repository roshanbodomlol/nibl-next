import { connect } from 'react-redux';
import StaticLink from 'next/link';

const mapStateToProps = ({ common }) => ({ common });

const LogoMain = ({ common }) => (
  <StaticLink href="/">
    <a><img src={common.logos.main.logoImage} alt={common.logos.main.logoAltText}/></a>
  </StaticLink>
);

export default connect(mapStateToProps)(LogoMain);
