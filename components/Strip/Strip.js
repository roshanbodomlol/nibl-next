import { useState } from 'react';
import classnames from 'classnames';
import { string } from 'prop-types';
import { notification } from 'antd';
import _fetch from 'isomorphic-fetch';

import { API } from '../../constants';
import { isEmail } from '../../utilities/isEmail.util';
import Styles from './strip.module.scss';

const Strip = ({ text }) => {
  const [email, setEmail] = useState('');
  const [loading, setLoading] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();

    if (!isEmail(email)) {
      notification.open({
        message: '',
        description:
          'Please enter a valid email address.'
      });
      return false;
    }

    setLoading(true);

    const formData = new FormData();
    const body = { subscriberEmail: email };

    formData.append('subscriberEmail', email);
    _fetch(`${process.env.API_URL}/${API.endPoints.SUBSCRIBE}`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(response => response.json())
      .then((response) => {
        if (response.status) {
          setLoading(false);
          setEmail('');
          notification.open({
            message: 'Email subscription added',
            description:
              'Thank you for subscribing.'
          });
        } else throw new Error(response.error);
      })
      .catch((err) => {
        console.log(err);
        notification.open({
          description: err.message
        });
        setLoading(false);
      });
    // post(API.endPoints.SUBSCRIBE, body)
    //   .then(() => {
    //     setLoading(false);
    //     setEmail('');
    //     notification.open({
    //       message: 'Email subscription added',
    //       description:
    //         'Thank you for subscribing.'
    //     });
    //   })
    //   .catch(err => {
    //     console.log(err);
    //     // notification.open({
    //     //   description: err
    //     // });
    //     setLoading(false);
    //   });
  };

  return (
    <div className={classnames(Styles.wrapper, 'greyback')}>
      <div className={Styles.flexSection}>
        <span className={Styles.title}>{text}</span>
        <div className={Styles.block}>
          <form onSubmit={handleSubmit}>
            <input type="text" placeholder="Enter your email" value={email} onChange={e => setEmail(e.target.value)}/>
            <button className={Styles.button} type="submit" disabled={loading}>Subscribe</button>
          </form>
        </div>
      </div>
    </div>
  );
};

Strip.propTypes = {
  text: string
};

Strip.defaultProps = {
  text: ''
};

export default Strip;
