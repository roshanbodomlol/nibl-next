import React, { useState } from 'react';
import fetch from 'isomorphic-fetch';

import { BigTitleBlock } from '../Layouts/Block';
import ButtonRedBorder from '../ButtonRedBorder';
import Styles from './forexc.module.scss';

const ForexConverter = () => {
  const [loading, setLoading] = useState(false);
  const [from, setFrom] = useState('NPR');
  const [to, setTo] = useState('USD');
  const [amount, setAmount] = useState(null);
  const [value, setValue] = useState(null);

  const handleConversion = () => {
    setLoading(true);
    const query = `${from}_${to}`;
    fetch(`https://free.currconv.com/api/v7/convert?q=${query}&compact=ultra&apiKey=c13e13b36254f7667153`, {
      method: 'GET'
    })
      .then(response => response.json())
      .then((data) => {
        setValue(Object.values(data)[0]);
        setLoading(false);
      })
      .catch(e => console.log(e));
  };

  return (
    <BigTitleBlock title="Currency Conversion">
      <div className={Styles.elementsWrapper}>
        <input
          placeholder="Enter an amount"
          onChange={e => setAmount(e.target.value)}
          value={amount}
        />
        <div className={Styles.currency}>
          <div className={Styles.currencySelector}>
            <img src="/img/down-arrow.png" alt=""/>
            <select onChange={(e) => {
              setFrom(e.target.value);
              setValue(null);
            }}
            >
              <option value="NPR" selected>NPR</option>
              <option value="USD">USD</option>
              <option value="AUD">AUD</option>
              <option value="EUR">EUR</option>
              <option value="GBP">GBP</option>
              <option value="SGD">SGD</option>
              <option value="CNY">CNY</option>
              <option value="HKD">HKD</option>
              <option value="CAD">CAD</option>
              <option value="JPY">JPY</option>
              <option value="CHF">CHF</option>
            </select>
          </div>
          <div className={Styles.currencySelector}>
            <img src="/img/down-arrow.png" alt=""/>
            <select onChange={(e) => {
              setTo(e.target.value);
              setValue(null);
            }}
            >
              <option value="USD" selected>USD</option>
              <option value="NPR">NPR</option>
              <option value="AUD">AUD</option>
              <option value="EUR">EUR</option>
              <option value="GBP">GBP</option>
              <option value="SGD">SGD</option>
              <option value="CNY">CNY</option>
              <option value="HKD">HKD</option>
              <option value="CAD">CAD</option>
              <option value="JPY">JPY</option>
              <option value="CHF">CHF</option>
            </select>
          </div>
        </div>
        <ButtonRedBorder title="Apply Now" onClick={handleConversion} disabled={!!loading || !amount}/>
        {
          value && (
            <div className={Styles.number}>
              {`${(value * amount).toFixed(2)} ${to}`}
            </div>
          )
        }
      </div>
    </BigTitleBlock>
  );
};

export default ForexConverter;
