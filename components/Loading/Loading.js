import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Spin } from 'antd';

import styles from './Loading.module.scss';

const Loading = ({ fullHeight }) => {
  const classes = classnames(styles.wrapper, {
    [styles.full]: fullHeight
  });
  return (
    <div className={classes}>
      <Spin/>
    </div>
  );
};

Loading.propTypes = {
  fullHeight: PropTypes.bool
};

Loading.defaultProps = {
  fullHeight: false
};

export default Loading;
