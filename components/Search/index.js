import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { message } from 'antd';
import Link from 'next/link';

import { get } from '../../services/api.services';
import { API } from '../../constants';
import { generatePath } from '../../utilities/url.utils';
import { findPageByIDs } from '../../utilities/currentpage.util';
import Loading from '../Loading';

const mapStateToProps = ({ navigation }) => ({ navigation });

const Search = ({ navigation }) => {
  const [results, setResults] = useState([]);
  const [loading, setLoading] = useState(true);
  const router = useRouter();

  const { q } = router.query;

  const populate = () => {
    const routes = [];
    [...navigation.primary, ...navigation.secondary].forEach((page) => {
      const subpages = page.routes;
      subpages.forEach((subpage) => {
        if (subpage.routes) {
          routes.push(...subpage.routes);
        }
      });
    });

    const searchResults = routes.filter(route => route.name.toLowerCase().includes(q.toLowerCase()));
    setResults(searchResults.map(result => ({
      name: result.name,
      url: result.route,
      ...result
    })));
  };

  const getSearch = () => {
    setLoading(true);
    get(generatePath(API.endPoints.SEARCH, { keyword: q }))
      .then((res) => {
        setResults(res.content);
        setLoading(false);
      })
      .catch((e) => {
        setLoading(false);
        message.info('An unexpected error has occurred, please reload the page and try again');
      });
  };

  useEffect(() => {
    getSearch();
  }, []);

  useEffect(() => {
    getSearch();
  }, [q]);

  const searchList = results.map((term, index) => {
    const body = term.searchContent.replace(/(<([^>]+)>)/gi, '');
    const regEx = new RegExp(q, 'ig');
    const replaceMask = ` <span>${q}</span>`;

    const query = body.replace(regEx, replaceMask);
    const display = `${query.slice(0, 1000)}${query.length > 1000 ? '...' : ''}`;

    console.log('FINDIDS', navigation, term.menuId, term.subMenuId, term.pageId);
    const route = findPageByIDs(navigation, term.menuId, term.subMenuId, term.pageId);
    if (!route) {
      return null;
    }
    return (
      <div key={`pop-search-item-${index}`} className="search-item">
        <Link href="/[page]/[subpage]/[child]" as={route}><a>{term.searchTitle}</a></Link>
        <div className="search-body" dangerouslySetInnerHTML={{ __html: display }}/>
      </div>
    );
  });

  return (
    <div className="white-gradient main--content">
      <div className="container">
        <div className="heading">
          <h1>SEARCH RESULTS FOR {q}</h1>
        </div>
        {
          loading
            ? <Loading/>
            : (
              <div className="search-list">
                {
                  searchList.length > 0
                    ? searchList
                    : 'No results to show'
                }
              </div>
            )
        }
      </div>
    </div>
  );
};

export default connect(mapStateToProps)(Search);
