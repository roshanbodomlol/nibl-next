import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import Styles from './buttonRedborder.module.scss';

/**
 * @visibleName Red border button
 */

const ButtonRedBorder = ({
  title, onClick, disabled, type
}) => (
  <div className={classnames(Styles.wrapper, 'common-class-btn', { disabled })}>
    <button
      onClick={disabled ? undefined : onClick}
      type={type}
    >
      {title}
    </button>
  </div>
);

ButtonRedBorder.propTypes = {
  title: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
  type: PropTypes.string
};

ButtonRedBorder.defaultProps = {
  onClick: () => {},
  disabled: false,
  type: 'submit'
};

export default ButtonRedBorder;
