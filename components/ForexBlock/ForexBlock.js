import { Component } from 'react';
import { Spin, Row, Col } from 'antd';
import Link from 'next/link';
import moment from 'moment';

import { GLOBALS, API } from '../../constants';
import { get } from '../../services/api.services';
import Styles from './forexBlockStyles.module.scss';
import ContentHeader from '../ContentHeader';
import { groupBy } from 'lodash';

class ForexBlock extends Component {
  state = {
    items: [],
    loading: true
  };

  componentDidMount() {
    get(API.endPoints.FOREX_HOME)
      .then((response) => {
        const items = response.pageContent;
        const currencies = Object.keys(GLOBALS.FOREX_CURRENCIES);
        const filteredItems = items.filter(i => currencies.indexOf(i.forexCurrency) > -1);
        this.setState({ items: filteredItems, loading: false });
      })
      .catch((e) => {
        console.log('Error while trying to fetch FOREX', e);
      });
  }

  render() {
    const { items, loading } = this.state;

    const properRates = groupBy(items, rate => rate.forexTime1);

    let dailyRatesGroups = [];

    Object.keys(properRates).forEach((key) => {
      const groupObj = properRates[key];
      const group = {
        time: moment(key).format('h:mm:ss a'),
        dailyRatesRows: groupObj.map((item, i) => {
          const decimalPlaces = (item.forexCurrency === 'JPY' || item.forexCurrency === 'KRW') ? 4 : 2;
          return (
            <Row key={`forex-item-${i}`} gutter={15} type="flex">
              <Col span={5}>
                <div className={Styles.currency}>
                  {/* <img src={`/svg/flags/${item.forexCurrency}.svg`} alt=""/> */}
                  <span>{item.forexCurrency}</span>
                </div>
              </Col>
              <Col span={7} className={Styles.tableColCenter}>
                <span>{Number(item.forexCash).toFixed(decimalPlaces)}</span>
              </Col>
              <Col span={6} className={Styles.tableColCenter}>
                <span>{Number(item.forexOthers).toFixed(decimalPlaces)}</span>
              </Col>
              <Col span={6} className={Styles.tableColCenter}>
                <span>{Number(item.forexRates).toFixed(decimalPlaces)}</span>
              </Col>
            </Row>
          );
        })
      };

      dailyRatesGroups.push(group);
    });

    return (
      <>
        {
          loading
            ? (
              <div className="page-loader __transparent _medium">
                <Spin/>
              </div>
            )
            : (
              <>
                {
                  dailyRatesGroups.length > 0
                    ? (
                      <>
                        <div className={Styles['forex-block-table']}>
                          {
                            items[0] && (
                              <div className={Styles.date}>
                                On {moment(items[0].forexTime1).format('dddd, MMMM DD, YYYY, h:mm:ss a')}
                              </div>
                            )
                          }
                          
                          <div className={Styles['_body']}>
                            {
                              dailyRatesGroups.map((rateGroup) => (
                                <div style={{ marginBottom: 18 }}>
                                  <div className={Styles['_heading']}>
                                    <Row gutter={15}>
                                      <Col span={5}>
                                        <span style={{ textAlign: 'center', display: 'inline-block', width: '100%' }}>Currency</span>
                                      </Col>
                                      <Col span={7}>
                                        <span>Cash Buying</span>
                                      </Col>
                                      <Col span={6}>
                                        <span>Other Buying</span>
                                      </Col>
                                      <Col span={6}>
                                        <span>Selling Rates</span>
                                      </Col>
                                    </Row>
                                  </div>
                                  {rateGroup.dailyRatesRows}
                                </div>
                              ))
                            }
                          </div>
                        </div>
                        <Link href="/[page]/[subpage]/[child]" as="/personal-banking/rates-and-fees/foreign-exchange-rates">
                          <a className={Styles.button}>See More Exchange Rates</a>
                        </Link>
                      </>
                    )
                    : (
                      <span>No data available</span>
                    )
                }
              </>
            )
        }
      </>
    );
  }
}

export default ForexBlock;
