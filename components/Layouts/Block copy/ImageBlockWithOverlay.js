import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Styles from './style.module.scss';

const ImageBlockWithOverlay = ({ title, children, image, animatedClass, animationDelay }) => (
  <div className={classnames(Styles.imageWrapper, 'vertical-image', 'horizontal-image', 'commonWrapper', animatedClass)} style={{ animationDelay }}>
    <img src={image} alt="" />
    <div className={classnames(Styles.overlay, 'mob-overlay')}>
      <h4>{title}</h4>
      <div className={Styles.details}>
        {children}
        <a href="1">Read More</a>
      </div>
    </div>
  </div>
);

ImageBlockWithOverlay.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  image: PropTypes.string.isRequired,
  animatedClass: PropTypes.string.isRequired,
  animationDelay: PropTypes.string.isRequired
};

export default ImageBlockWithOverlay;
