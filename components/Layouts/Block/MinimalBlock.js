import React from 'react';
import PropTypes from 'prop-types';

import Styles from './style.module.scss';


const MinimalBlock = ({ title, content, image }) => (
  <div className={Styles.minimalBlockWrapper}>
    <div className={Styles.minimalBloackInner}>
      <div className={Styles.minimalBlockTitle}>
        <h4>{title}</h4>
        <img src={image} alt=""/>
      </div>
      <p>
        {content}
      </p>
      <a href="1">Learn More</a>
    </div>
  </div>
);

MinimalBlock.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired
};

export default MinimalBlock;
