import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { withTranslation } from '../../../locales/i18n';
import Image from '../../Image';
import Styles from './style.module.scss';

const ImageBlockWithOverlay = ({
  title, children, image, animatedClass, animationDelay, link, imageHeight, alt, t
}) => (
  <div className={classnames(Styles.imageWrapper, 'vertical-image', 'horizontal-image', 'commonWrapper', animatedClass)} style={{ animationDelay }}>
    <Image src={image} height={imageHeight} alt={alt}/>
    <div className={classnames(Styles.overlay, 'mob-overlay')}>
      <h4>{title}</h4>
      <div className={Styles.details}>
        {children}
        {
          link && <a href={link}>{t('global_readMore')}</a>
        }
      </div>
    </div>
  </div>
);

ImageBlockWithOverlay.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  image: PropTypes.string.isRequired,
  animatedClass: PropTypes.string.isRequired,
  animationDelay: PropTypes.string.isRequired
};

export default ImageBlockWithOverlay;
