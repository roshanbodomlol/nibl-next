import { useState, useEffect } from 'react';
import classnames from 'classnames';
import { MdChevronRight as ChevronRight } from 'react-icons/md';

import TrackVisibility from '../../../TrackVisibility';
import BigTitleBlock from '../BigTitleBlock';
import Styles from './styles.module.scss';

/**
 * @visibleName Big Title Block Set
 */

const BigTitleBlockSet = () => {
  const [bigTitleBlocks, setBigTitleBlocks] = useState([]);
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    setBigTitleBlocks([
      {
        isBigTitleBlock: true,
        hasArrows: false,
        title: 'Branches',
        firstItem: 'The 78 Branches, 109 ATMs, 7 Extension Counters and 9 Revenue Collection Counters of NIBL dedicated to Customer Service.',
        secondItem: null,
        thirdItem: null,
        fourthItem: null
      },
      {
        isBigTitleBlock: true,
        hasArrows: false,
        title: 'Corporate Office',
        firstItem: 'Durbar Marg, P.O. Box: 3412 Kathmandu Nepal',
        secondItem: 'Email: Info@nibl.com.np',
        thirdItem: 'Telephone: (977-1) 4228229',
        fourthItem: 'Fax: (977-1) 4226349, 4228927'
      },
      {
        isBigTitleBlock: true,
        hasArrows: false,
        title: 'Help Desk',
        firstItem: 'Toll Free Number : 1660-01-00070',
        secondItem: 'Mobile Number: +977-9851126440',
        thirdItem: 'Email: cardhelpdesk@nibl.com.np',
        fourthItem: null
      },
      {
        isBigTitleBlock: true,
        hasArrows: true,
        title: 'Latest News',
        firstItem: '32 AGM Press Release',
        secondItem: 'Press Release of Pashupati Extension Counter Opening',
        thirdItem: 'Press Release of EuroMoney Excellence-Best Bank Nepal 2018',
        fourthItem: null
      }
    ]);
  }, []);

  const animatedClass = classnames(Styles.bigTitleSetWrapper, {
    'animated fadeIn': visible
  });
  // Big Title blocks
  const bigBlocks = bigTitleBlocks.filter(bigBlock => bigBlock.isBigTitleBlock);
  const singleBigBlock = bigBlocks.map((bigBlock, index) => {
    if (!bigBlock.hasArrows) {
      return (
        <BigTitleBlock key={`block ${index}`} title={bigBlock.title}>
          <span>{bigBlock.firstItem}</span>
          <span>{bigBlock.secondItem}</span>
          <span>{bigBlock.thirdItem}</span>
          <span>{bigBlock.fourthItem}</span>
        </BigTitleBlock>
      );
    } return (
      <BigTitleBlock title={bigBlock.title}>
        <div className={Styles.withArrows}>
          <div className={Styles.flexSection}>
            <ChevronRight/>
            <span>{bigBlock.firstItem}</span>
          </div>
          <div className={Styles.flexSection}>
            <ChevronRight/>
            <span>{bigBlock.secondItem}</span>
          </div>
          <div className={Styles.flexSection}>
            <ChevronRight/>
            <span>{bigBlock.thirdItem}</span>
          </div>
          {/* <span>{bigBlock.fourthItem}</span> */}
        </div>
      </BigTitleBlock>
    );
  });
  return (
    <TrackVisibility threshold={0.1} onView={() => setVisible(true)}>
      <div className={animatedClass} style={{ animationDelay: '.5s' }}>
        <div className="container">
          <div className={Styles.bigTitleBlockSetFlex}>
            {singleBigBlock}
          </div>
        </div>
      </div>
    </TrackVisibility>
  );
};

export default BigTitleBlockSet;
