import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Styles from './styles.module.scss';

const Card = ({ title, content, color, image, link, animatedClass, animationDelay }) => (
  <div className={classnames(Styles.contentWrapper, Styles[color], 'commonWrapper', animatedClass)} style={{ animationDelay }}>
    <div className={Styles.mainContent}>
      <div>
        <img src={image} alt="" />
      </div>
      <div className={Styles.title}>
        <h4>{title}</h4>
      </div>
      <p>
        {content}
      </p>
      <a href="1">Read More</a>
    </div>
  </div>
);

Card.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
  animatedClass: PropTypes.string.isRequired,
  animationDelay: PropTypes.string.isRequired
};

export default Card;
