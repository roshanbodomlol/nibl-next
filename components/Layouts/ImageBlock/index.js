import React from 'react';
import PropTypes from 'prop-types';
import Styles from './styles.module.scss';

const ImageBlock = ({ title, image }) => (
  <div className="imageWrapper">
    <div className={Styles.contentWrapper}>
      <img src={image} alt="" />
      <div className={Styles.content}>
        <h4>{title}</h4>
        {/* <p>
          Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
          Aenean commodo ligula eget dolor. Aenean massa Cum sociis
          natoque penatibus et magnis dis parturient montes.
          <a href="1">more</a>
        </p> */}
      </div>
    </div>
  </div>
);

ImageBlock.propTypes = {
  title: PropTypes.string.isRequired
  // children: PropTypes.node.isRequired
};

export default ImageBlock;
