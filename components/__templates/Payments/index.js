import { useState, useEffect, useRef } from 'react';
import $ from 'jquery';

import Content from './Content';
import BannerBlock from '../../BannerBlock';
import TabNav from '../../TabNav';
import TabContent from '../../TabContent';
import Transformer from './transformer';
import Styles from './styles.module.scss';

const Payments = (props) => {
  const [activeItemKey, setItemActiveKey] = useState(null);
  const mainRef = useRef(null);

  const data = new Transformer(props);
  const {
    items,
    bannerImage,
    bannerTitle,
    bannerAlt,
    bannerCaption,
    bannerUrl
  } = data;

  const handleClick = (cat) => {
    setItemActiveKey(cat);
    // window.scrollTo({ top: window.scrollY + mainRef.current.getBoundingClientRect().top - 150, behavior: 'smooth' });
    $('body,html').animate(
      {
        scrollTop: $('#key-main').offset().top - 120
      },
      800
    );
  };

  return (
    <div className={Styles.wrapper}>
      <BannerBlock
        background={bannerImage}
        title={bannerTitle}
        description={bannerCaption}
        alt={bannerAlt}
        link={bannerUrl}
      />
      <div className="white-gradient" style={{ paddingBottom: 1 }}>
        <div className={Styles.iconBlockWrapper}>
          <div className="container">
            <div className={Styles.paymentsTabNav}>
              {
                items.map((item) => (
                  <div
                    className={Styles.navItem}
                    role="button"
                    tabIndex="-1"
                    onClick={() => { handleClick(item.contentCategory); }}
                  >
                    <div className={Styles.navImage} style={{ width: 115, height: 115 }}>
                      <img src={item.contentCategoryImage} alt={item.contentCategoryImageAlt}/>
                    </div>
                    <span>{item.contentCategory}</span>
                  </div>
                ))
              }
            </div>
            <div className={Styles.ebankingWrapper} ref={mainRef} id="key-main">
              <TabContent>
                {
                  items.map((item) => (
                    <Content isActive={activeItemKey === item.contentCategory} content={item} title={item.name}/>
                  ))
                }
              </TabContent>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Payments;
