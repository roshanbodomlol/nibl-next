import React from 'react';
import PropTypes from 'prop-types';

import { withTranslation } from '../../../../../locales/i18n';
import ContentHeader from '../../../../ContentHeader';
import TableBody from '../../../../TableBody';
import Styles from './schoolPayment.module.scss';

const SchoolPayment = ({ schools, t }) => {
  const schoolList = schools.map(schoolItem => (
    <TableBody>
      <span>1</span>
      <span>{schoolItem.title}</span>
      <span><a href="1">Pay bill</a></span>
    </TableBody>
  ));

  return (
    <div className={Styles.wrapper}>
      <div className={Styles.heading}>
      <h1>{t('payment_ntcTopup')}</h1>
      </div>
      <div className={Styles.contentHeaderWrapper}>
        <ContentHeader grey>
          <span>S.No</span>
          <span>{t('global_details')}</span>
          <span>{t('payment_payments')}</span>
        </ContentHeader>
      </div>
      <div className={Styles.tableRowWrapper}>
        {schoolList}
      </div>
    </div>
  );
};

SchoolPayment.propTypes = {
  schools: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default withTranslation('common')(SchoolPayment);
