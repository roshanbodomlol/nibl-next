import { Component } from 'react';
import PropTypes from 'prop-types';

import TabContent from '../../../TabContent';
import MobileTopUp from './MobileTopUp';
import SchoolPayment from './SchoolPayment';
import TabNav from '../../../TabNav';
import Styles from './styles.module.scss';

class Ebanking extends Component {
  state = {
    items: [
      {
        name: 'Mobile',
        id: 'mobile-topup',
        iconUrl: '/svg/mobile-topup.svg',
        secondIcon: '/svg/mobile-topup-red.svg'
      },
      {
        name: 'School',
        id: 'school',
        iconUrl: '/svg/school.svg',
        secondIcon: '/svg/school-red.svg'
      },
      {
        name: 'University',
        id: 'university',
        iconUrl: '/svg/edu-loan.svg',
        secondIcon: '/svg/edu-loan-red.svg'
      },
      {
        name: 'Insurance',
        id: 'insurance',
        iconUrl: '/svg/surakshya-bachat-dark.svg',
        secondIcon: '/svg/surakshya-bachat-red.svg'
      }
    ],
    activeItemId: 'mobile-topup'
  }

  render() {
    const { items, activeItemId } = this.state;
    const { ntcItems, ncellItems, schools } = this.props;
    return (
      <div className={Styles.wrapper}>
        <div className={Styles.tabNavWrapper}>
          <TabNav
            arrows
            items={items}
            activeItemId={activeItemId}
            onSelect={(id) => { this.setState({ activeItemId: id }); }}
          />
        </div>
        <div className={Styles.tabContentWrapper}>
          <TabContent>
            <MobileTopUp
              ntcItems={ntcItems}
              ncellItems={ncellItems}
              isActive={activeItemId === 'mobile-topup'}
            />
            <SchoolPayment schools={schools} isActive={activeItemId === 'school'}/>
          </TabContent>
        </div>
      </div>
    );
  }
}

Ebanking.propTypes = {
  ntcItems: PropTypes.arrayOf(PropTypes.object).isRequired,
  ncellItems: PropTypes.arrayOf(PropTypes.object).isRequired,
  schools: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default Ebanking;
