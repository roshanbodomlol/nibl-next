import React from 'react';
import PropTypes from 'prop-types';

import { withTranslation } from '../../../../../locales/i18n';
import ContentHeader from '../../../../ContentHeader';
import TableBody from '../../../../TableBody';
import Styles from './mobileTopUp.module.scss';

const MobileTopUp = ({ ntcItems, ncellItems, t }) => {
  const ntcList = ntcItems.map(ntcItem => (
    <TableBody>
      <span>1</span>
      <span>{ntcItem.title}</span>
      <span><a href="1">{t('payment_bill')}</a></span>
    </TableBody>
  ));

  const ncellList = ncellItems.map(ncellItem => (
    <TableBody>
      <span>1</span>
      <span>{ncellItem.title}</span>
      <span><a href="1">{t('payment_bill')}</a></span>
    </TableBody>
  ));

  return (
    <div className={Styles.wrapper}>
      <div className={Styles.heading}>
        <h1>{t('payment_ntcTopup')}</h1>
      </div>
      <div className={Styles.contentHeaderWrapper}>
        <ContentHeader grey>
          <span>S.No</span>
          <span>{t('global_details')}</span>
          <span>{t('payment_payments')}</span>
        </ContentHeader>
      </div>
      <div className={Styles.tableRowWrapper}>
        {ntcList}
      </div>
      <div className={Styles.heading}>
        <h1>{t('"payment_ncellTopup"')}</h1>
      </div>
      <div className={Styles.contentHeaderWrapper}>
        <ContentHeader grey>
          <span>S.No</span>
          <span>{t('global_details')}</span>
          <span>{t('payment_payments')}</span>
        </ContentHeader>
      </div>
      <div className={Styles.tableRowWrapper}>
        {ncellList}
      </div>
    </div>
  );
};

MobileTopUp.propTypes = {
  ntcItems: PropTypes.arrayOf(PropTypes.object).isRequired,
  ncellItems: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default withTranslation('common')(MobileTopUp);
