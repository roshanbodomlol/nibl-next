import Styles from './content.module.scss';
import css from '../styles.module.scss';

const Content = ({ content }) => (
  <div className="container">
    <div className={Styles.wrapper}>
      {
        // content.contentSection.map((section, index) => (
        //   <div key={`payment-conetnt-${index}`} style={{ marginBottom: 60 }}>
        //     <div className={Styles.heading}>
        //       <h1>{section.contentPageTitle}</h1>
        //     </div>
        //     <div className={Styles.contentHeaderWrapper}>
        //       <ContentHeader grey>
        //         <span>S.No</span>
        //         <span>{t('global_details')}</span>
        //         <span>{t('payment_payments')}</span>
        //       </ContentHeader>
        //     </div>
        //     <div className={Styles.tableRowWrapper}>
        //       {
        //         section.contentLinks.map((link, i) => (
        //           <TableBody key={`payment-sub-content-${i}`}>
        //             <span>{i + 1}</span>
        //             <span>{link.contentLinkTitle}</span>
        //             <span><a href={link.contentLinkUrl} target="_blank" rel="noopener noreferrer">{t('payment_bill')}</a></span>
        //           </TableBody>
        //         ))
        //       }
        //     </div>
        //   </div>
        // ))
      }
      {
        content.contentLinks.map((e) => (
          <a className={Styles.contentLink} href={e.contentLinkUrl} target="_blank" rel="noopener noreferrer">
            <div className={css.navItem}>
              <div className={css.navImage}>
                <img src={e.contentImage} alt={e.contentImageAlt}/>
              </div>
              <span>{e.contentLinkTitle}</span>
            </div>
          </a>
        ))
      }
    </div>
  </div>
);

export default Content;
