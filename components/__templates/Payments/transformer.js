export default class Transformer {
  constructor(APIObject) {
    this.bannerImage = APIObject.bannerDetails.bannerImage;
    this.bannerTitle = APIObject.bannerDetails.bannerTitle;
    this.bannerCaption = APIObject.bannerDetails.bannerCaption;
    this.bannerAlt = APIObject.bannerDetails.bannerAlt;
    this.bannerUrl = APIObject.bannerDetails.bannerUrl;
    this.items = APIObject.pageContent;
    this.activeKey = this.items[0] && this.items[0].contentCategory;
  }
}
