import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Styles from './reports.module.scss';

//  Components
import TabContent from '../../../TabContent';
import ReportsTable from './ReportsTable';
import Timeline from '../../../Timeline';

class ReportsContent extends Component {
  state = {
    items: [
      {
        year: 2012
      },
      {
        year: 2013
      },
      {
        year: 2014
      },
      {
        year: 2015
      }
    ],
    activeItemKey: 2012
  }

  render() {
    const { items, activeItemKey } = this.state;
    const { title } = this.props;
    return (
      <div className={Styles.Wrapper}>
        <div className="container">
          <div className={Styles.dateWrapper}>
            <Timeline
              onClick={(key) => { this.setState({ activeItemKey: key }); }}
              items={items}
              activeItemKey={activeItemKey}
            />
          </div>

          <div className={Styles.heading}>
            <h1>{title}</h1>
          </div>

          <div>
            <TabContent>
              <ReportsTable isActive={activeItemKey === 2012} />
              <ReportsTable isActive={activeItemKey === 2013} />
            </TabContent>
          </div>
        </div>
      </div>
    );
  }
}

ReportsContent.propTypes = {
  title: PropTypes.string.isRequired
};

export default ReportsContent;
