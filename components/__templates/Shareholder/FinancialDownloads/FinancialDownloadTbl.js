import React from 'react';
import Styles from './financialDownload.module.scss';

import ContentHeader from '../../../ContentHeader';
import ExpansionPanel from '../../../ExpansionPanel';

// Images
import downloadIcon from '../../../../assets/img/download.png';

const FinancialDownloadTbl = () => (
  <div className={Styles.accordianWrapper}>
    <ContentHeader>
      <div className={Styles.tableHeadTitles}>
        <span>S.No</span>
        <span>Details</span>
      </div>
    </ContentHeader>
    <ExpansionPanel
      isordered
      items={[
        {
          primary:
          (
            <div className={Styles.primaryPanel}>
              <div>
                <p>
                  Download NIBL Mobile (SMS) Banking Application
                </p>
              </div>
              <a href="1"><img src={downloadIcon} alt=""/></a>
            </div>
          ),
          secondary: <div>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </div>
        },
        {
          primary:
          (
            <div className={Styles.primaryPanel}>
              <div>
                <p>
                  Download NIBL Mobile (SMS) Banking Application
                </p>
              </div>
              <a href="1"><img src={downloadIcon} alt=""/></a>
            </div>
          ),
          secondary: <div>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </div>
        },
        {
          primary:
          (
            <div className={Styles.primaryPanel}>
              <div>
                <p>
                  Download NIBL Mobile (SMS) Banking Application
                </p>
              </div>
              <a href="1"><img src={downloadIcon} alt=""/></a>
            </div>
          ),
          secondary: <div>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </div>
        },
        {
          primary:
          (
            <div className={Styles.primaryPanel}>
              <div>
                <p>
                  Download NIBL Mobile (SMS) Banking Application
                </p>
              </div>
              <a href="1"><img src={downloadIcon} alt=""/></a>
            </div>
          ),
          secondary: <div>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </div>
        }
      ]}
    />
  </div>
);

export default FinancialDownloadTbl;
