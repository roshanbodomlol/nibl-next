import React, { Component } from 'react';

import Styles from './financialDownload.module.scss';

import TabContent from '../../../TabContent';
import Timeline from '../../../Timeline';

import FinancialDownloadTbl from './FinancialDownloadTbl';
import FinancialTransaction from './FinancialTransaction';
import { withTranslation } from '../../../../locales/i18n';

class FinancialDetails extends Component {
  state = {
    items: [
      {
        year: 2012
      },
      {
        year: 2013
      },
      {
        year: 2014
      },
      {
        year: 2015
      },
      {
        year: 2016
      }
    ],
    activeItemKey: 2012
  }

  render() {
    const { items, activeItemKey } = this.state;
    const { t } = this.props;

    return (
      <div className={Styles.wrapper}>
        <div className="container">
          <div className={Styles.tabNavWrapper}>
            <Timeline
              onClick={(key) => { this.setState({ activeItemKey: key }); }}
              items={items}
              activeItemKey={activeItemKey}
            />
          </div>
          <div className={Styles.heading}>
            <h1>{t('global_financial_details')}</h1>
          </div>
          <TabContent>
            <FinancialDownloadTbl isActive={activeItemKey === 2012}/>
            <FinancialDownloadTbl isActive={activeItemKey === 2013}/>
          </TabContent>
        </div>
        <div className={Styles.whiteBg}>
          <div className="container">
            <FinancialTransaction/>
          </div>
        </div>
      </div>
    );
  }
}

export default withTranslation('common')(FinancialDetails);
