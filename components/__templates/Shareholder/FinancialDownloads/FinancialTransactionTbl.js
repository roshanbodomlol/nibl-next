import React from 'react';
import Styles from './financialDownload.module.scss';

// Components
import ContentHeader from '../../../ContentHeader';
import TableBody from '../../../TableBody';

const FinancialTransactionTbl = () => (
  <div className={Styles.transactionTable}>
    <ContentHeader>
      <div className={Styles.tableHeadTitles}>
        <span>S.No</span>
        <span>Details</span>
        <span>Figures in NPR</span>
      </div>
    </ContentHeader>
    <TableBody>
      <span>1</span>
      <span>Net Worth</span>
      <span>16,287,751,617</span>
    </TableBody>
    <TableBody>
      <span>1</span>
      <span>Net Worth</span>
      <span>16,287,751,617</span>
    </TableBody>
    <TableBody>
      <span>1</span>
      <span>Net Worth</span>
      <span>16,287,751,617</span>
    </TableBody>
    <TableBody>
      <span>1</span>
      <span>Net Worth</span>
      <span>16,287,751,617</span>
    </TableBody>
    <TableBody>
      <span>1</span>
      <span>Net Worth</span>
      <span>16,287,751,617</span>
    </TableBody>
  </div>
);

export default FinancialTransactionTbl;