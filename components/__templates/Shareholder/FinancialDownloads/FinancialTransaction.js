import React, { Component } from 'react';
import Styles from './financialDownload.module.scss';

// Components
import Timeline from '../../../Timeline';
import TabContent from '../../../TabContent';
import FinancialTransactionTbl from './FinancialTransactionTbl';

class FinancialTransaction extends Component {
  state = {
    items: [
      {
        year: 2012
      },
      {
        year: 2013
      },
      {
        year: 2014
      },
      {
        year: 2015
      }
    ],
    activeItemKey: 2012
  }

  render() {
    const { items, activeItemKey } = this.state;
    return (
      <div className={Styles.transactionWrapper}>
        <div className={Styles.transactionTabNavWrapper}>
          <Timeline
            onClick={(key) => { this.setState({ activeItemKey: key }); }}
            items={items}
            activeItemKey={activeItemKey}
          />
        </div>
        <div className={Styles.heading}>
          <h1>Financial Transaction in the certain year</h1>
        </div>
        <TabContent>
          <FinancialTransactionTbl isActive={activeItemKey === 2012}/>
          <FinancialTransactionTbl isActive={activeItemKey === 2013}/>
        </TabContent>
      </div>
    );
  }
}

export default FinancialTransaction;
