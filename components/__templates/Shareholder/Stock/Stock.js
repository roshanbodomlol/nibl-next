import React from 'react';
import PropTypes from 'prop-types';
import Styles from './stock.module.scss';

// Components
import ContentCard from '../../../ContentCard';
import StockChart from './StockChart';

const Stock = ({ title, date }) => (
  <div className={Styles.wrapper}>
    <div className="container">
      <div className={Styles.heading}>
        <h1>{title}</h1>
      </div>
      <ContentCard>
        <h4>{date}</h4>
        <div className={Styles.flexSection}>
          <div className={Styles.flexItems}>
            <span>Paid-up Values Nrs</span>
            <span>Closing Nrs</span>
            <span>Listed Shares</span>
          </div>
          <div className={Styles.flexItems}>
            <span>100</span>
            <span>528.00</span>
            <span>106,264,357.00</span>
          </div>
        </div>
      </ContentCard>
      <div className={Styles.stockChart}>
        <StockChart/>
      </div>
    </div>
  </div>
);

Stock.propTypes = {
  title: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired
};

export default Stock;
