import BannerBlock from '../../BannerBlock';
import ContentCard from '../../ContentCard';
import './styles.scss';

const Stock = () => (
  <>
    <BannerBlock
      title=""
      description=""
      background="/img/mobtop-bg.jpg"
    />
    <div className="strategy-wrapper white-gradient">
      <div className="container">
        <h1>Stock</h1>
        <ContentCard>
          <p className="red">On Sunday, March 31, 2019</p>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae in odio suscipit recusandae quam, a eos aut minus rem, et quis? Pariatur libero praesentium quae, nihil mollitia quisquam sint asperiores.</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae soluta officiis labore eligendi laborum porro officia similique minus iste error impedit, consequuntur neque sapiente blanditiis natus suscipit cumque. Harum, officiis.</p>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque fugit ea minima nesciunt aperiam nam facere corporis et ullam. Non quia quae modi. Porro deleniti sit est, sapiente sequi vitae!</p>
        </ContentCard>
        <div className="stock-chart">
          <img src="/img/stock-img.png" alt=""/>
        </div>
      </div>
    </div>
  </>
);

export default Stock;
