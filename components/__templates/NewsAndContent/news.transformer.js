export default class NewsTransformer {
  constructor(APIObject) {
    this.bannerImg = APIObject.bannerDetails.bannerImage;
    this.bannerTitle = APIObject.bannerDetails.bannerTitle;
    this.bannerDescription = APIObject.bannerDetails.bannerCaption;
    this.bannerAlt = APIObject.bannerDetails.bannerAlt;
    this.bannerUrl = APIObject.bannerDetails.bannerUrl;
    this.title = APIObject.pageDetails.pageTitle;
    this.news = APIObject.pageContent.map(e => ({
      id: e.contentId,
      title: e.newsTitle,
      excerpt: e.newsCaption,
      content: e.newsContent,
      image: e.newsBanner
    }));
  }
}
