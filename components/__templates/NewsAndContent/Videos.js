import { Component } from 'react';
import { Spin, Row, Col } from 'antd';

import { withTranslation } from '../../../locales/i18n';
import { generatePath } from '../../../utilities/url.utils';
import { API } from '../../../constants';
import { get } from '../../../services/api.services';
import VideoModal from '../../VideoModal';
import Styles from './newsContent.module.scss';
import VideoTransformer from './videos.transformer';

class Videos extends Component {
  state = {
    videos: [],
    loading: true
  };

  componentDidMount() {
    const { menuId } = this.props;
    get(generatePath(API.endPoints.VIDEOS, { menuId }))
      .then((content) => {
        const videos = content.pageContent.map(e => new VideoTransformer(e));
        this.setState({ videos, loading: false });
      })
      .catch((e) => {
        console.log('Error while trying to fetch videos', e);
      });
  }

  render() {
    const { videos, loading } = this.state;
    const { t } = this.props;

    const videosList = videos.map(video => (
      <Col span={8} key={`video-${video.id}`}>
        <div className={Styles.videoCardWrapper}>
          <div className={Styles.videoCard}>
            <VideoModal videoId="9xwazD5SyVg"/>
          </div>
        </div>
      </Col>
    ));

    return (
      <div className={Styles.videosWrapper}>
        <div className="heading">
          <h1>{t('global_videos')}</h1>
        </div>
        {
          loading
            ? (
              <div className="page-loader __transparent">
                <Spin/>
              </div>
            )
            : (
              <Row type="flex">
                {videosList}
              </Row>
            )
        }
      </div>
    );
  }
}

export default withTranslation('common')(Videos);
