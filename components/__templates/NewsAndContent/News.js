import { Row, Col } from 'antd';

import CardImageAndText from '../../CardImageAndText';
import Styles from './newsContent.module.scss';

const News = ({ news, title }) => {
  const newsList = news.map((post, index) => (
    <Col sm={8} xs={24} key={`news-${index}`}>
      <div className={Styles.card}>
        <CardImageAndText
          image={post.image}
          title={post.title}
          excerpt={post.excerpt}
          link={`/news-content/${post.id}`}
        />
      </div>
    </Col>
  ));

  return (
    <div>
      <div className="heading">
        <h1>{title}</h1>
      </div>
      <div className={Styles.cardWrapper}>
        <Row type="flex">
          {newsList}
        </Row>
      </div>
    </div>
  );
};

export default News;
