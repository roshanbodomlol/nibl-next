import BannerBlock from '../../BannerBlock';
import News from './News';
import Transformer from './news.transformer';
import Styles from './newsContent.module.scss';

const NewsAndContent = (props) => {
  const data = new Transformer(props);

  const {
    bannerImg,
    bannerTitle,
    bannerDescription,
    bannerAlt,
    title,
    news,
    bannerUrl
  } = data;

  return (
    <div className={Styles.wrapper}>
      <BannerBlock
        background={bannerImg}
        title={bannerTitle}
        description={bannerDescription}
        alt={bannerAlt}
        link={bannerUrl}
      />
      <div className="white-gradient main--content">
        <div className="container">
          <News news={news} title={title}/>
        </div>
      </div>
    </div>
  );
};

export default NewsAndContent;
