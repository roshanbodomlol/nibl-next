import React from 'react';
import PropTypes from 'prop-types';

import { withTranslation } from '../../../locales/i18n';
import ContentHeader from '../../ContentHeader';
import ExpansionPanel from '../../ExpansionPanel';
import Styles from './downloads.module.scss';

const SavingAccount = ({ details, t }) => {
  const contentTable = details.map((detailsItem, index) => (
    {
      primary: (
        <div className={`${Styles.primaryPanel}`} key={`downloads-primary-${index}`}>
          <div>
            <p>
              {detailsItem.fileName}
            </p>
          </div>
          <a download href={`${detailsItem.uploadedFile}`}>
            <img src="/img/download.png" alt=""/>
          </a>
        </div>
      ),
      secondary: (
        <div key={`downloads-secondary-${index}`} dangerouslySetInnerHTML={{ __html: detailsItem.fileDescription }}/>
      )
    }
  ));

  return (
    <div className={Styles.savingAccount}>
      <ContentHeader grey>
        <div className={Styles.gridWrapper}>
          <span className={Styles.sn}>S.No</span>
          <span className={Styles.details}>{t('global_details')}</span>
        </div>
      </ContentHeader>
      <ExpansionPanel
        isOrdered
        items={contentTable}
      />
    </div>
  );
};

SavingAccount.propTypes = {
  details: PropTypes.arrayOf(PropTypes.object)
};

SavingAccount.defaultProps = {
  details: []
};

export default withTranslation('common')(SavingAccount);
