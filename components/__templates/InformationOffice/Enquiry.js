import { Row, Col } from 'antd';

import { withTranslation } from '../../../locales/i18n';
import ContentCard from '../../ContentCard';
import Styles from './infoOffice.module.scss';

const Enquiry = ({ items, t }) => (
  <div className={Styles.enquiry}>
    <div className="heading">
      <h1>{t('global_enquire')}</h1>
      {/* <a href="1" className={Styles.location}>
        <LocationOn/>
        <span>See in the map</span>
      </a> */}
    </div>
    <div className={Styles.flexBlock}>
      <Row type="flex" gutter={24}>
        {
          items.map(item => (
            <Col span={8}>
              <div className={Styles.block}>
                <ContentCard>
                  <div className={Styles.content}>
                    <h5>{item.contentTitle}</h5>
                    <div dangerouslySetInnerHTML={{ __html: item.contentDescriptionB }}/>
                  </div>
                </ContentCard>
              </div>
            </Col>
          ))
        }
      </Row>
    </div>
  </div>
);

export default withTranslation('common')(Enquiry);
