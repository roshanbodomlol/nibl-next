import { Row, Col } from 'antd';

import { withTranslation } from '../../../locales/i18n';
import ContentHeader from '../../ContentHeader';
import TableBody from '../../TableBody';
import Styles from './infoOffice.module.scss';

const StaffTbl = ({ items, t }) => (
  <div className={Styles.tableContainer}>
    <div className={Styles.tableInner}>
      <div className={Styles.staffTbl}>
        {/* <a href="1" className={Styles.location}>
          <LocationOn/>
          <span>See the branch location</span>
        </a> */}
        <ContentHeader>
          <div className={Styles.table}>
            <Row>
              <Col span={2}>
                <div>S.No</div>
              </Col>
              <Col span={7}>
                <div className={Styles.service}>{t('global_name')}</div>
              </Col>
              <Col span={7}>
                <div className={Styles.male}>{t('global_province')}</div>
              </Col>
              <Col span={7}>
                <div className={Styles.male}>{t('global_phone')}</div>
              </Col>
            </Row>
          </div>
        </ContentHeader>
        {
          items.map((item, index) => (
            <TableBody key={`info-content-table-${item.provinceId}`}>
              <div className={Styles.table}>
                <Row>
                  <Col span={2}>
                    <div>{index + 1}</div>
                  </Col>
                  <Col span={7}>
                    <div className={Styles.service}>{item.managerName}</div>
                  </Col>
                  <Col span={7}>
                    <div className={Styles.male}>{item.provinceName}</div>
                  </Col>
                  <Col span={7}>
                    <div className={Styles.male}>{item.phoneNumber}</div>
                  </Col>
                </Row>
              </div>
            </TableBody>
          ))
        }
      </div>
    </div>
  </div>
);

export default withTranslation('common')(StaffTbl);
