export default class Transformer {
  constructor(APIObject) {
    this.bannerImage = APIObject.bannerDetails.bannerImage;
    this.bannerTitle = APIObject.bannerDetails.bannerTitle;
    this.bannerDescription = APIObject.bannerDetails.bannerCaption;
    this.bannerAlt = APIObject.bannerDetails.bannerAlt;
    this.bannerUrl = APIObject.bannerDetails.bannerUrl;
    this.genderContent = APIObject.pageContent.find(c => c.contentName === 'Gender Wise Total Staff');
    this.provinceContent = APIObject.pageContent.find(c => c.contentName === 'Province Managers');
    this.enquiryContent = APIObject.pageContent.find(c => c.contentName === 'Enquiry');
    this.branchManagersContent = APIObject.pageContent.find(c => c.contentName === 'Branch Managers');
    this.extensionCountersContent = APIObject.pageContent.find(c => c.contentName === 'Extension Counter');
    [this.officer, this.legalOfficer] = APIObject.pageContent.find(c => c.contentName === 'Information Officer').contentInfo;
  }
}
