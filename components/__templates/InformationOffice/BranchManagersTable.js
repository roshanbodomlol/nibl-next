import { Row, Col } from 'antd';

import { withTranslation } from '../../../locales/i18n';
import ContentHeader from '../../ContentHeader';
import TableBody from '../../TableBody';
import Styles from './infoOffice.module.scss';

const BranchManagersTable = ({ items, t }) => (
  <div className={Styles.tableContainer}>
    <div className={Styles.tableInner}>
      <div className={Styles.staffTbl}>
        {/* <a href="1" className={Styles.location}>
          <LocationOn/>
          <span>See the branch location</span>
        </a> */}
        <ContentHeader>
          <div className={Styles.table}>
            <Row>
              <Col span={2}>
                <span className={Styles.sn}>S.No</span>
              </Col>
              <Col span={6}>
                <span className={Styles.managers}>{t('global_branch')}</span>
              </Col>
              <Col span={4}>
                <span className={Styles.provinceOffice}>{t('global_branchManager')}</span>
              </Col>
              <Col span={4}>
                <span className={Styles.provinceOffice}>{t('global_email')}</span>
              </Col>
              <Col span={4}>
                <span className={Styles.provinceOffice}>{t('global_phone')}</span>
              </Col>
              <Col span={4}>
                <span className={Styles.provinceOffice}>{t('locator_fax')}</span>
              </Col>
            </Row>
          </div>
        </ContentHeader>
        {
          items.map((item, index) => (
            <TableBody key={`info-content-table-${item.workforceId}`}>
              <div className={Styles.table}>
                <Row>
                  <Col span={2}>
                    <div>{index + 1}</div>
                  </Col>
                  <Col span={6}>
                    <div className={Styles.service}>{item.branchName}</div>
                  </Col>
                  <Col span={4}>
                    <div className={Styles.male}>{item.branchManager}</div>
                  </Col>
                  <Col span={4}>
                    <div className={Styles.total}>{item.email}</div>
                  </Col>
                  <Col span={4}>
                    <div className={Styles.female}>{item.phoneNumber}</div>
                  </Col>
                  <Col span={4}>
                    <div className={Styles.female}>{item.faxNumber}</div>
                  </Col>
                </Row>
              </div>
            </TableBody>
          ))
        }
      </div>
    </div>
  </div>
);

export default withTranslation('common')(BranchManagersTable);
