import { Component } from 'react';

import { withTranslation } from '../../../locales/i18n';
import TabNav from '../../TabNav';
import TabContent from '../../TabContent';
import BannerBlock from '../../BannerBlock';
import StaffTbl from './StaffTbl';
import ProvinceTbl from './ProvinceTbl';
import Enquiry from './Enquiry';
import BranchManagers from './BranchManagersTable';
import ExtensionCounters from './ExtensionCountersTable';
import Transformer from './transformer';
import Styles from './infoOffice.module.scss';

class InformationOffice extends Component {
  constructor(props) {
    super();
    const { t } = props;
    this.state = { // eslint-disable-line
      items: [
        {
          name: t('template_informationOffice_informationOfficer'),
          key: 'information-officer'
        },
        {
          name: 'Legal Officer',
          key: 'legal-officer'
        },
        {
          name: t('template_informationOffice_genderWiseStaff'),
          key: 'gender-wise-total-staff'
        },
        {
          name: t('template_informationOffice_provinceManager'),
          key: 'province-managers'
        },
        // {
        //   name: t('global_enquire'),
        //   key: 'enquiry'
        // },
        {
          name: t('template_informationOffice_branchManagers'),
          key: 'branch-managers'
        },
        {
          name: t('template_informationOffice_extensionCounter'),
          key: 'extension-counters'
        }
      ],
      activeItemKey: 'information-officer'
    };
  }

  render() {
    const { items, activeItemKey } = this.state;
    const data = new Transformer(this.props);
    const {
      bannerAlt,
      bannerDescription,
      bannerImage,
      bannerTitle,
      genderContent,
      provinceContent,
      enquiryContent,
      branchManagersContent,
      extensionCountersContent,
      officer,
      legalOfficer,
      bannerUrl
    } = data;

    const md = legalOfficer.designation;
    const ps = md.split('@');

    const legalHtml = ps.map((p) => {
      let line = p.replace('#', '<b>');
      line = line.replace('#', '</b>');
      return `<p>${line}</p>`;
    });

    const md2 = officer.designation;
    const ps2 = md2.split('@');

    const legalHtml2 = ps2.map((p) => {
      let line = p.replace('#', '<b>');
      line = line.replace('#', '</b>');
      return `<p>${line}</p>`;
    });

    const officerLink = officer.phoneNumber && officer.phoneNumber.split('###');

    return (
      <div className={Styles.wrapper}>
        <BannerBlock
          background={bannerImage}
          title={bannerTitle}
          description={bannerDescription}
          alt={bannerAlt}
          link={bannerUrl}
        />
        <div className="container">
          <div className={Styles.secondaryWrapper}>
            <TabNav
              onClick={(key) => { this.setState({ activeItemKey: key }); }}
              activeItemKey={activeItemKey}
              items={items}
            />
            <div className={Styles.tabContent}>
              <TabContent>
                <div isActive={activeItemKey === 'information-officer'} className={Styles.officer}>
                  <div className={Styles.inner}>
                    <img src={officer.image} alt=""/>
                    <div className={Styles.name}>{officer.name}</div>
                    <div className={Styles.markdown}>
                      {
                        legalHtml2.map((line) => <div dangerouslySetInnerHTML={{ __html: line }}/>)
                      }
                    </div>
                    <div className={Styles.email}>Email: <a href={`mailto:${officer.email}`}>{officer.email}</a></div>
                    <div><a download target="_blank" href={officerLink[1]}>{ officerLink[0] }</a></div>
                  </div>
                </div>
                <div isActive={activeItemKey === 'legal-officer'} className={Styles.officer}>
                  <div className={Styles.inner}>
                    <img src={legalOfficer.image} alt=""/>
                    <div className={Styles.name}>{legalOfficer.name}</div>
                    <div className={Styles.markdown}>
                      {
                        legalHtml.map((line) => <div dangerouslySetInnerHTML={{ __html: line }}/>)
                      }
                    </div>
                    <div className={Styles.email}>Email: <a href={`mailto:${legalOfficer.email}`}>{legalOfficer.email}</a></div>
                  </div>
                </div>
                <StaffTbl isActive={activeItemKey === 'gender-wise-total-staff'} items={genderContent.contentInfo}/>
                <ProvinceTbl isActive={activeItemKey === 'province-managers'} items={provinceContent.contentInfo}/>
                <Enquiry isActive={activeItemKey === 'enquiry'} items={enquiryContent.contentInfo}/>
                <BranchManagers isActive={activeItemKey === 'branch-managers'} items={branchManagersContent.contentInfo}/>
                <ExtensionCounters isActive={activeItemKey === 'extension-counters'} items={extensionCountersContent.contentInfo}/>
              </TabContent>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withTranslation('common')(InformationOffice);
