import { Row, Col } from 'antd';

import { withTranslation } from '../../../locales/i18n';
import ContentHeader from '../../ContentHeader';
import TableBody from '../../TableBody';
import Styles from './infoOffice.module.scss';

const StaffTbl = ({ items, t }) => (
  <div className={Styles.tableContainer}>
    <div className={Styles.tableInner}>
      <div className={Styles.staffTbl}>
        {/* <a href="1" className={Styles.location}>
          <LocationOn/>
          <span>See the branch location</span>
        </a> */}
        <ContentHeader>
          <div className={Styles.table}>
            <Row>
              <Col span={4}>
                <div>S.No</div>
              </Col>
              <Col span={5}>
                <div className={Styles.service}>{t('template_informationOffice_serviceStatus')}</div>
              </Col>
              <Col span={5}>
                <div className={Styles.male}>{t('global_male')}</div>
              </Col>
              <Col span={5}>
                <div className={Styles.female}>{t('global_female')}</div>
              </Col>
              <Col span={5}>
                <div className={Styles.total}>{t('global_total')}</div>
              </Col>
            </Row>
          </div>
        </ContentHeader>
        {
          items.map((item, index) => (
            <TableBody key={`info-content-table-${item.workforceId}`}>
              <div className={Styles.table}>
                <Row>
                  <Col span={4}>
                    <div>{index + 1}</div>
                  </Col>
                  <Col span={5}>
                    <div className={Styles.service}>{item.serviceStatus}</div>
                  </Col>
                  <Col span={5}>
                    <div className={Styles.male}>{item.maleCount}</div>
                  </Col>
                  <Col span={5}>
                    <div className={Styles.female}>{item.femaleCount}</div>
                  </Col>
                  <Col span={5}>
                    <div className={Styles.total}>{item.totalCount}</div>
                  </Col>
                </Row>
              </div>
            </TableBody>
          ))
        }
      </div>
    </div>
  </div>
);

export default withTranslation('common')(StaffTbl);
