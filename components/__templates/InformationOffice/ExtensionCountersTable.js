import { Row, Col } from 'antd';

import { withTranslation } from '../../../locales/i18n';
import ContentHeader from '../../ContentHeader';
import TableBody from '../../TableBody';
import Styles from './infoOffice.module.scss';

const BranchManagersTable = ({ items, t }) => (
  <div className={Styles.tableContainer}>
    <div className={Styles.tableInner}>
      <div className={Styles.staffTbl}>
        {/* <a href="1" className={Styles.location}>
          <LocationOn/>
          <span>See the branch location</span>
        </a> */}
        <ContentHeader>
          <div className={Styles.table}>
            <Row>
              <Col span={4}>
                <div>S.No</div>
              </Col>
              <Col span={10}>
                <div className={Styles.service}>Name</div>
              </Col>
              <Col span={10}>
                <div className={Styles.male}>Location</div>
              </Col>
            </Row>
          </div>
        </ContentHeader>
        {
          items.map((item, index) => (
            <TableBody key={`info-content-table-${item.workforceId}`}>
              <div className={Styles.table}>
                <Row>
                  <Col span={4}>
                    <div>{index + 1}</div>
                  </Col>
                  <Col span={10}>
                    <div className={Styles.service}>{item.branchName}</div>
                  </Col>
                  <Col span={10}>
                    <div className={Styles.male}>{item.address}</div>
                  </Col>
                </Row>
              </div>
            </TableBody>
          ))
        }
      </div>
    </div>
  </div>
);

export default withTranslation('common')(BranchManagersTable);
