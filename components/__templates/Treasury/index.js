import { Component } from 'react';

import BannerBlock from '../../BannerBlock';
import TabNav from '../../TabNav';
import TabContent from '../../TabContent';
import ForeignExchange from './ForeignExchange';
import ResearchAndDevelopment from './ResearchAndDevelopment';
import PageWithBasicContent from './PageWithBasicContent';
import Transformer from './transfomer';
import Styles from './treasuryInsidePage.module.scss';

class Treasury extends Component {
  state = {
    activeItemKey: null
  };

  componentDidMount() {
    const data = new Transformer(this.props);
    const { activeItemKey } = data;
    this.setState({ activeItemKey });
  }

  render() {
    const data = new Transformer(this.props);
    const { activeItemKey } = this.state;
    const { items } = data;

    return (
      <div className={Styles.wrapper}>
        <BannerBlock
          background="/img/mobtop-bg.jpg"
          title="Foreign Exchange"
          description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
        />
        <div className="white-gradient main--content">
          <div className="container">
            <div className={Styles.tabnavWrapper}>
              <TabNav
                onClick={(key) => { this.setState({ activeItemKey: key }); }}
                activeItemKey={activeItemKey}
                items={items}
              />
            </div>
          </div>
          <TabContent>
            <ForeignExchange title="Foreign Exchange" isActive={activeItemKey === 'foreign-exchange'}/>
            <PageWithBasicContent title="Fund Management" isActive={activeItemKey === 'fund-management'}/>
            <PageWithBasicContent title="Bills/ Bonds" isActive={activeItemKey === 'bills-bonds'}/>
            <PageWithBasicContent title="Inter-Bank Placements" isActive={activeItemKey === 'inter-bank-placements'}/>
            <ResearchAndDevelopment isActive={activeItemKey === 'research'}/>
            <PageWithBasicContent title="Forward Contracts" isActive={activeItemKey === 'forward-contracts'}/>
            <PageWithBasicContent title="NRB Reports" isActive={activeItemKey === 'nrb-reports'}/>
          </TabContent>
        </div>
      </div>
    );
  }
}

export default Treasury;
