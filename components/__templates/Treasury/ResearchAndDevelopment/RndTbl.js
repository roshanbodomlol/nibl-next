import PropTypes from 'prop-types';

import ExpansionPanel from '../../../ExpansionPanel';
import ContentHeader from '../../../ContentHeader';
import Styles from './rnd.module.scss';

const RndTbl = ({ title }) => (
  <div className={Styles.rndTbl}>
    <div className="heading">
      <h1>{title}</h1>
    </div>
    <div className={Styles.expansionPanel}>
      <ContentHeader>
        <div className={Styles.tblHead}>
          <span>S.No</span>
          <span>Details</span>
        </div>
      </ContentHeader>
      <ExpansionPanel
        isOrdered
        items={[
          {
            primary:
            (
              <div className={Styles.primaryPanel}>
                <div>
                  <p>
                    Download NIBL Mobile (SMS) Banking Application
                  </p>
                </div>
                <a href="1"><img src="/img/download.png" alt=""/></a>
              </div>
            ),
            secondary:
            (
              <div>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </div>
            )
          },
          {
            primary:
            (
              <div className={Styles.primaryPanel}>
                <div>
                  <p>
                    Download NIBL Mobile (SMS) Banking Application
                  </p>
                </div>
                <a href="1"><img src="/img/download.png" alt=""/></a>
              </div>
            ),
            secondary:
            (
              <div>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </div>
            )
          },
          {
            primary:
            (
              <div className={Styles.primaryPanel}>
                <div>
                  <p>
                    Download NIBL Mobile (SMS) Banking Application
                  </p>
                </div>
                <a href="1"><img src="/img/download.png" alt=""/></a>
              </div>
            ),
            secondary:
            (
              <div>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </div>
            )
          },
          {
            primary:
            (
              <div className={Styles.primaryPanel}>
                <div>
                  <p>
                    Download NIBL Mobile (SMS) Banking Application
                  </p>
                </div>
                <a href="1"><img src="/img/download.png" alt=""/></a>
              </div>
            ),
            secondary:
            (
              <div>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </div>
            )
          },
          {
            primary:
            (
              <div className={Styles.primaryPanel}>
                <div>
                  <p>
                    Download NIBL Mobile (SMS) Banking Application
                  </p>
                </div>
                <a href="1"><img src="/img/download.png" alt=""/></a>
              </div>
            ),
            secondary:
            (
              <div>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </div>
            )
          }
        ]}
      />
    </div>
  </div>
);

RndTbl.propTypes = {
  title: PropTypes.string.isRequired
};

export default RndTbl;
