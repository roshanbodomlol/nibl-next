import PropTypes from 'prop-types';

import ContentCard from '../../ContentCard';
import Styles from './treasuryInsidePage.module.scss';
import ForexRates from '../../ForexRates';

const ForeignExchange = ({ title }) => (
  <div className={Styles.foreignExWrapper}>
    <div className="container">
      <div className="heading">
        <h1>{title}</h1>
      </div>
      <ContentCard>
        <p>
          The department updates the exchange rates daily and
          as per the decisions of FEDAN. The exchange rates are
          based on the movements of INR against USD.
        </p>
        <p>
          The department maintains the foreign currency position
          as per the needs of the bank. Mostly, the currencies are
          squared off at day end to minimize the risks related to
          exchange rate fluctuations. Mostly spot transactions are
          carried out for sales/ purchase of the foreign currencies.
        </p>
      </ContentCard>
    </div>
    <div className="container">
      <div className={Styles.tblWrapper}>
        <ForexRates/>
      </div>
    </div>
  </div>
);

ForeignExchange.propTypes = {
  title: PropTypes.string.isRequired
};

export default ForeignExchange;
