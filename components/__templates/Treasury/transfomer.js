export default class Transformer {
  constructor() {
    this.items = [
      {
        name: 'Foreign Exchange',
        key: 'foreign-exchange'
      },
      {
        name: 'Fund Management',
        key: 'fund-management'
      },
      {
        name: 'Bills/ Bonds',
        key: 'bills-bonds'
      },
      {
        name: 'Inter-Bank Placements',
        key: 'inter-bank-placements'
      },
      {
        name: 'Research',
        key: 'research'
      },
      {
        name: 'Forward Contracts',
        key: 'forward-contracts'
      },
      {
        name: 'NRB Reports',
        key: 'nrb-reports'
      }
    ];
  }
}
