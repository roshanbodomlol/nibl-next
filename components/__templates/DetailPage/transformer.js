export default class Transformer {
  constructor(APIObject) {
    this.id = APIObject.pageContent[0].contentId;
    this.title = APIObject.pageContent[0].newsTitle;
    this.exerpt = APIObject.pageContent[0].newsCaption;
    this.content = APIObject.pageContent[0].newsDescription;
    this.image = APIObject.pageContent[0].newsBanner;
    this.date = APIObject.pageContent[0].newsDate;
    this.pageId = APIObject.pageContent[0].pageId;
  }
}
