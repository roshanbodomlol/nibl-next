import { Component } from 'react';
import {
  FacebookShareButton,
  FacebookIcon,
  TwitterShareButton,
  TwitterIcon,
  EmailShareButton,
  EmailIcon
} from 'react-share';
import { Row, Col } from 'antd';

import { withTranslation } from '../../../locales/i18n';
import { generatePath } from '../../../utilities/url.utils';
import CardImageAndText from '../../CardImageAndText';
import Styles from './detailPage.module.scss';
import { API } from '../../../constants';
import { get } from '../../../services/api.services';
import Transformer from './transformer';

class DetailPage extends Component {
  state = {
    news: []
  };

  limit = 3;

  componentDidMount() {
    this.getNews();
    typeof window !== 'undefined' && window.scrollTo({
      top: 0,
      behavior: 'smooth'
    });
  }

  // componentDidUpdate(prevProps) {
  //   const { match } = this.props;
  //   if (match.params.id !== prevProps.match.params.id) {
  //     this.setState({ //eslint-disable-line
  //       loading: true
  //     }, () => {
  //       window.scrollTo({
  //         top: 0,
  //         behavior: 'smooth'
  //       });
  //       this.getNews();
  //     });
  //   }
  // }

  getNews = () => {
    const data = new Transformer(this.props);
    const { pageId } = data;
    get(generatePath(API.endPoints.NEWS_FEATURED, { limit: this.limit, pageId }))
      .then((response) => {
        this.setState({
          news: response.pageContent.map(e => ({
            id: e.contentId,
            title: e.newsTitle,
            excerpt: e.newsCaption,
            content: e.newsDescription,
            image: e.newsBanner
          }))
        });
      })
      .catch((e) => {
        console.log('Error while trying to fetch Featured News', e);
      });
  }

  render() {
    const data = new Transformer(this.props);
    const { news } = this.state;
    const { title, content } = data;
    const { t } = this.props;

    const newsList = news.map((item, i) => (
      <Col span={8} key={`more-news-${i}`}>
        <div className={Styles.card}>
          <CardImageAndText
            image={item.image}
            title={item.title}
            excerpt={item.excerpt}
            link={`/news-content/${item.id}`}
          />
        </div>
      </Col>
    ));

    return (
      <div className={Styles.wrapper}>
        <div className="white-gradient main--content">
          <div className="container">
            <div className="heading">
              <h1>{title}</h1>
            </div>
            <div className={Styles.content}>
              <div className="wysiwyg" dangerouslySetInnerHTML={{ __html: content }}/>
              <div className={Styles.social}>
                <FacebookShareButton
                  url={typeof window !== 'undefined' && window.location}
                >
                  <FacebookIcon size={32} round/>
                </FacebookShareButton>
                <TwitterShareButton url={typeof window !== 'undefined' && window.location}>
                  <TwitterIcon size={32} round/>
                </TwitterShareButton>
                <EmailShareButton url={typeof window !== 'undefined' && window.location}>
                  <EmailIcon size={32} round/>
                </EmailShareButton>
              </div>
            </div>
          </div>
          {
            news.length > 0 && (
              <div className={Styles.moreNews}>
                <div className="container">
                  <div className="heading">
                    <h1>{t('template_detailPage_continue')}</h1>
                  </div>
                  <div className={Styles.cardWrapper}>
                    <Row type="flex" gutter={18}>
                      {newsList}
                    </Row>
                  </div>
                </div>
              </div>
            )
          }
        </div>
      </div>
    );
  }
}

export default withTranslation('common')(DetailPage);
