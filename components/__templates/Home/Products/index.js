import { useState, useEffect } from 'react';
import classnames from 'classnames';
import Link from 'next/link';
import { connect } from 'react-redux';
import { Spin, Row, Col } from 'antd';

import TrackVisibility from '../../../TrackVisibility';
import { findProductById } from '../../../../utilities/currentpage.util';
import ImageBlockWithOverlay from '../../../Layouts/Block/ImageBlockWithOverlay';
import { get } from '../../../../services/api.services';
import { API } from '../../../../constants';
import { generatePath } from '../../../../utilities/url.utils';
import Transformer from './transformer';
import Styles from '../style.module.scss';

const mapStateToProps = ({ navigation }) => ({ navigation });

const Products = ({ title, navigation }) => {
  const limit = 6;
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    get(generatePath(API.endPoints.PRODUCT_LIST, { isFeatured: true, limit }))
      .then((products) => {
        setPosts(products.map(product => new Transformer(product)));
        setLoading(false);
      })
      .catch((e) => {
        console.log('Error while trying to fetch Featured Products', e);
      });
  }, []);

  const animatedClass = classnames({ 'animated fadeIn': visible });

  let animationTime = 0.5;
  const featuredBlockList = posts.map((post, i) => {
    animationTime += 0.2;
    const navObj = findProductById(navigation, post.pageId);
    return (
      <Col
        key={`product-card-${i}`}
        lg={8}
        sm={12}
        xs={24}
      >
        <Link
          href="/[page]/[subpage]/[child]"
          as={navObj.route}
        >
          <a>
            <ImageBlockWithOverlay
              animatedClass={animatedClass}
              animationDelay={`${animationTime}s`}
              title={post.title}
              image={post.image}
              imageHeight={300}
            >
              <div dangerouslySetInnerHTML={{ __html: post.excerpt }}/>
            </ImageBlockWithOverlay>
          </a>
        </Link>
      </Col>
    );
  });

  return (
    <div className="white-gradient">
      <div className={classnames(Styles.productWrapper, 'product-wrapper')}>
        <div className="container">
          {
            !title && (
              <div className="heading">
                <h1>Products</h1>
              </div>
            )
          }
          {
            loading
              ? (
                <div className="page-loader __transparent __medium">
                  <Spin/>
                </div>
              )
              : (
                <div className={classnames(Styles.accountsWrapper, 'accounts-wrapper')}>
                  <div className={Styles.fullBlock}>
                    <TrackVisibility threshold={0.15} onView={() => setVisible(true)}>
                      <div className={Styles.halfBlockFlexed}>
                        <Row gutter={30} type="flex">
                          {featuredBlockList}
                        </Row>
                      </div>
                    </TrackVisibility>
                  </div>
                </div>
              )
          }
        </div>
      </div>
    </div>
  );
};

export default connect(mapStateToProps)(Products);
