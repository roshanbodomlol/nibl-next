export default class Transformer {
  constructor(ProductListAPIObject) {
    this.title = ProductListAPIObject.pageDetails.pageTitle;
    this.image = ProductListAPIObject.overviewContent.overviewImage;
    this.excerpt = ProductListAPIObject.overviewContent.overviewCaption;
    this.pageId = ProductListAPIObject.pageDetails.pageId;
  }
}
