import React from 'react';

import Styles from './style.module.scss';

const BlockSection = ({ title, children }) => (
  <div className="wrapper">
    <h4>{title}</h4>
    <div className={Styles.innerBlock}>
      {children}
    </div>
  </div>
);

export default BlockSection;
