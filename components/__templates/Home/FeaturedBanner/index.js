import { useState, useEffect } from 'react';
import SlickSlider from '../../../SlickSlider';

import Styles from '../style.module.scss';
import { get } from '../../../../services/api.services';
import { API } from '../../../../constants';

const settings = {
  dots: false,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  autoplay: true,
  fade: true
};

const FeaturedBanner = () => {
  const [banners, setBanners] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    get(API.endPoints.HOME_BANNER)
      .then((data) => {
        setBanners(data.pageContent);
        setLoading(false);
      })
      .catch(e => console.log(e));
  }, []);

  const slides = banners.map((banner, index) => (
    <a href={banner.bannerUrl}>
      <img style={{ width: '100%' }} key={`featured-banner-${index}`} src={banner.bannerImage} alt={banner.bannerAlt || banner.bannerTitle}/>
    </a>
  ));

  return (
    <div className={Styles.featuredBanner}>
      {
        !loading && (
          <SlickSlider settings={settings}>
            {slides}
          </SlickSlider>
        )
      }
    </div>
  );
};

export default FeaturedBanner;
