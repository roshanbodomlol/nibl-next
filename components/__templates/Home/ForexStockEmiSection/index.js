import cn from 'classnames';
import { Row, Col } from 'antd';

import { withTranslation } from '../../../../locales/i18n';
import ForexBlock from '../../../ForexBlock';
import StockBlock from '../../../StockBlock';
import EmiCaclBlock from '../../../EmiCalcBlock';
import Styles from './styles.module.scss';

const ForexStockEmiSection = ({ t }) => (
  <div className={Styles.wrapper}>
    <div className="container">
      <div className={Styles.flexBlock}>
        <Row type="flex" gutter={30}>
          <Col lg={8} sm={24} xs={24}>
            <div className="heading">
              <h1>{t('template_forex_forex')}</h1>
            </div>
            <div className={Styles.block}>
              <ForexBlock/>
            </div>
          </Col>
          <Col lg={8} sm={24} xs={24}>
            <div className="heading">
              <h1>{t('template_forex_stock')}</h1>
            </div>
            <div className={Styles.block}>
              <StockBlock/>
            </div>
          </Col>
          <Col lg={8} sm={24} xs={24}>
            <div className="heading">
              <h1>{t('template_calculator_title')}</h1>
            </div>
            <div className={cn(Styles.block, Styles.noPadding)}>
              <EmiCaclBlock/>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  </div>
);

export default withTranslation('common')(ForexStockEmiSection);
