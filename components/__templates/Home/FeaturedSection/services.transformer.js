import slugs from 'slugs';

export default class ServicesTransformer {
  constructor(SERVICESAPIOBJECT) {
    this.services = SERVICESAPIOBJECT.map((service) => (
      {
        isFeatured: false,
        title: service.pageDetails.pageTitle,
        excerpt: service.overviewContent.overviewCaption,
        slug: slugs(service.pageDetails.pageTitle),
        id: service.pageDetails.pageId,
        image: service.overviewContent.overviewImage
      }
    ));
  }
}
