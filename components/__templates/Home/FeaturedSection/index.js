import { useState, useEffect } from 'react';
import classnames from 'classnames';
import { Row, Col } from 'antd';
import { connect } from 'react-redux';
import Link from 'next/link';

import { withTranslation } from '../../../../locales/i18n';
import TrackVisiblity from '../../../TrackVisibility';
import { get } from '../../../../services/api.services';
import { API } from '../../../../constants';
import ImageBlockWithOverlay from '../../../Layouts/Block/ImageBlockWithOverlay';
import { generatePath } from '../../../../utilities/url.utils';
import Transformer from './services.transformer';
import Styles from '../style.module.scss';
import { findProductById } from '../../../../utilities/currentpage.util';

const mapStateToProps = ({ navigation }) => ({ navigation });

const Featured = ({ t, navigation }) => {
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    get(generatePath(API.endPoints.FEATURED_SERVICES, { isFeatured: true, limit: 6 }))
      .then(data => {
        const { services } = new Transformer(data);
        setPosts(services);
        setLoading(false);
      })
      .catch(e => console.log(e));
  }, []);

  const animatedClass = classnames({ 'animated fadeIn': visible });
  const nonFeaturedBlocks = posts.filter(post => !post.isFeatured);
  let animationTime = 0.5;

  const nonFeaturedBlockList = nonFeaturedBlocks.map((post, index) => {
    animationTime += 0.2;
    const navObj = findProductById(navigation, post.id);
    return (
      <Col
        key={`nonFeaturedImage-${index}`}
        lg={8}
        sm={12}
        xs={24}
      >
        <Link
          href="/[page]/[subpage]/[child]"
          as={navObj.route}
        >
          <a>
            <ImageBlockWithOverlay
              animatedClass={animatedClass}
              animationDelay={`${animationTime}s`}
              title={post.title}
              image={post.image}
              imageHeight={400}
            >
              <p>
                {post.excerpt}
                <br/>
              </p>
            </ImageBlockWithOverlay>
          </a>
        </Link>
      </Col>
    );
  });

  return (
    <div className="container">
      {
        loading
          ? ''
          : (
            <div className={Styles.featured}>
              <div className="heading">
                <h1>{t('template_featuredSection_title')}</h1>
              </div>
              <TrackVisiblity onView={() => setVisible(true)}>
                <div className={Styles.bigBlock}>
                  <div className={Styles.bigBlockLeft}>
                    <Row type="flex" gutter={30}>
                      {nonFeaturedBlockList}
                    </Row>
                  </div>
                </div>
              </TrackVisiblity>
            </div>
          )
      }
    </div>
  );
};

export default withTranslation('common')(connect(mapStateToProps)(Featured));
