import React, { Component } from 'react';
import Slider from 'react-slick';
import PropTypes from 'prop-types';
import { MdChevronLeft as ChevronLeft, MdChevronRight as ChevronRight } from 'react-icons/md';

import { withTranslation } from '../../../../locales/i18n';
import ButtonRedBorder from '../../../ButtonRedBorder';
import Styles from '../style.module.scss';

const FlagUsa = '/img/US.png';
const FlagUk = '/img/flag-gbp.png';
const FlagAus = '/img/AUD.png';
const FlagEuro = '/img/flag-eur.png';

const NextArrow = ({ className, style, onClick }) => (
  <button
    className={className}
    style={{ ...style, display: 'block', background: 'none' }}
    onClick={onClick}
  >
    <ChevronRight />
  </button>
);

const PrevArrow = ({ className, style, onClick }) => (
  <button
    className={className}
    style={{ ...style, display: 'block', background: 'none' }}
    onClick={onClick}
  >
    <ChevronLeft />
  </button>
);

class ForexSlider extends Component {
  state = {
    slider: []
  };

  componentDidMount() {
    this.setState(
      {
        slider: [
          {
            flag: FlagUsa,
            currency: 'USD',
            buying: '115.89',
            selling: '116.12'
          },
          {
            flag: FlagUk,
            currency: 'GBP',
            buying: '129.21',
            selling: '129.99'
          },
          {
            flag: FlagAus,
            currency: 'AUD',
            buying: '89.19',
            selling: '90.22'
          },
          {
            flag: FlagEuro,
            currency: 'Euro',
            buying: '110.89',
            selling: '111.12'
          },
          {
            flag: FlagEuro,
            currency: 'Euro',
            buying: '110.89',
            selling: '111.12'
          },
          {
            flag: FlagEuro,
            currency: 'Euro',
            buying: '110.89',
            selling: '111.12'
          }
        ]
      }
    );
  }

  render() {
    const { slider } = this.state;
    const { t } = this.props;

    const slides = slider.map(slide => (
      <div className={Styles.slide}>
        <img src={slide.flag} alt="" />
        <div className={Styles.forexValue}>
          <span>{slide.currency}</span>
          <span>
            {t('template_forex_buying')}:
            {slide.buying}
          </span>
          <span>
            {t('template_forex_selling')}:
            {slide.selling}
          </span>
        </div>
      </div>
    ));
    const settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 1,
      arrows: true,
      nextArrow: <NextArrow/>,
      prevArrow: <PrevArrow/>,
      responsive: [
        {
          breakpoint: 425,
          settings: {
            slidesToShow: 1
          }
        }
      ]
    };
    return (
      <div className="forexSlider">
        <div className={Styles.forexSlider}>
          <Slider {...settings}>
            {slides}
          </Slider>
        </div>
        <div className={Styles.more}>
          <ButtonRedBorder title="View"/>
        </div>
      </div>
    );
  }
}

NextArrow.propTypes = {
  className: PropTypes.string,
  style: PropTypes.objectOf(PropTypes.any),
  onClick: PropTypes.func
};

NextArrow.defaultProps = {
  className: '',
  style: {},
  onClick: null
};

PrevArrow.propTypes = {
  className: PropTypes.string,
  style: PropTypes.objectOf(PropTypes.any),
  onClick: PropTypes.func
};

PrevArrow.defaultProps = {
  className: '',
  style: {},
  onClick: null
};

export default withTranslation('common')(ForexSlider);
