import { connect } from 'react-redux';

import SliderSection from './SliderSection';
import Featured from './FeaturedSection';
import ForexStockEmiSection from './ForexStockEmiSection';
import FeaturedBanner from './FeaturedBanner';
import Products from './Products/index';
import LatestNews from './LatestNews';

const Home = () => {
  return (
    <>
      <SliderSection/>
      <div className="white-gradient">
        <Featured title="Featured"/>
        <ForexStockEmiSection/>
      </div>
      <FeaturedBanner/>
      <Products/>
      <LatestNews/>
    </>
  );
};

export default connect()(Home);
