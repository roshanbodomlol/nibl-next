import { useState } from 'react';
import moment from 'moment';
import classnames from 'classnames';

import { withTranslation } from '../../../../locales/i18n';
import TrackVisibility from '../../../TrackVisibility';
import { BigTitleBlock } from '../../../Layouts/Block';
import './StockSection.scss';

const StockSection = ({ t }) => {
  const [visible, setVisible] = useState(false);
  const stockExClass = classnames({ 'animated fadeIn': visible });

  return (
    <div className="container">
      <div className="stock-exchange">
        <TrackVisibility threshold={0.1} onView={() => setVisible(true)}>
          <div className={stockExClass}>
            <BigTitleBlock title="Stock Exchange">
              <div>
                <div className="today">
                  On {moment().format('dddd, MMMM DD, YYYY')}
                </div>
                <table>
                  <tr>
                    <td>{t('tempalte_stock_paidUpValue')}</td>
                    <td>100</td>
                  </tr>
                  <tr>
                    <td>Closing Nrs.</td>
                    <td>528.00</td>
                  </tr>
                  <tr>
                    <td>Listed Shares</td>
                    <td>106,264,343.00</td>
                  </tr>
                </table>
              </div>
            </BigTitleBlock>
          </div>
        </TrackVisibility>
        <div className="stock-graph">
          <a href="http://www.nepalstock.com/company/display/132" target="_blank" rel="noopener noreferrer">
            <img src="/img/stock.png" alt="" style={{ transform: 'scale(1.1)' }}/>
          </a>
        </div>
      </div>
    </div>
  );
};

export default withTranslation('common')(StockSection);
