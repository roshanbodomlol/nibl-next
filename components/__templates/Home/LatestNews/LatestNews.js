import { Component } from 'react';
import { Spin, Row, Col } from 'antd';
import Link from 'next/link';

import { withTranslation } from '../../../../locales/i18n';
import { generatePath } from '../../../../utilities/url.utils';
import { get } from '../../../../services/api.services';
import { API } from '../../../../constants';
import Styles from '../style.module.scss';

class LatestNews extends Component {
  state = {
    news: [],
    loading: true
  };

  limit = 3;

  componentDidMount() {
    get(generatePath(API.endPoints.NEWS_LATEST, { limit: this.limit }))
      .then((response) => {
        this.setState({
          news: response.pageContent.map(e => ({
            id: e.contentId,
            title: e.newsTitle,
            excerpt: e.newsCaption,
            content: e.newsDescription,
            image: e.newsBanner
          })),
          loading: false
        });
      })
      .catch((e) => {
        console.log('Error while trying to fetch Latest News', e);
      });
  }

  render() {
    const { news, loading } = this.state;
    const { t } = this.props;

    const newsList = news.map((item, i) => (
      <Col lg={8} sm={12} xs={24} key={`latest-news-block-${i}`}>
        <div className={Styles.newsSingle}>
          <img src={item.image} alt=""/>
          <div className={Styles.newsContent}>
            <div className={Styles.heading}>{item.title}</div>
            <p>
              {item.excerpt}
            </p>
            <div className={Styles.newsLink}>
              <Link href="/news-content/[id]" as={`/news-content/${item.id}`}>
                <a>{t('global_readMore')}</a>
              </Link>
            </div>
          </div>
        </div>
      </Col>
    ));
    return (
      <div className={Styles.newsWrapper}>
        <div className="container">
          <div className="heading">
            <h1>{t('template_news_latestNews')}</h1>
          </div>
          <div className={Styles.newsGrid}>
            {
              loading
                ? (
                  <div className="page-loader __transparent __medium">
                    <Spin/>
                  </div>
                )
                : (
                  <>
                    {
                      news.length > 0
                        ? (
                          <Row type="flex" gutter={30}>
                            {newsList}
                          </Row>
                        )
                        : (
                          <div className="page-loader __transparent __medium">
                            <span>{t('global_noDataAvailable')}</span>
                          </div>
                        )
                    }
                  </>
                )
            }
          </div>
        </div>
      </div>
    );
  }
}

export default withTranslation('common')(LatestNews);
