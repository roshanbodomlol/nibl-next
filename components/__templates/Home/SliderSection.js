import { useEffect, useState } from 'react';

import SlickSlider from '../../SlickSlider';
import { get } from '../../../services/api.services';
import { API } from '../../../constants';
import Styles from './style.module.scss';

const settings = {
  dots: true,
  infinite: true,
  speed: 900,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  autoplay: true,
  fade: true
};

const SliderSection = () => {
  const [sliders, setSliders] = useState([]);

  useEffect(() => {
    get(API.endPoints.HOME_SLIDER)
      .then(content => setSliders(content.pageContent))
      .catch(e => {
        console.log('Error occurred trying to fetch sliders', e);
      });
  }, []);

  const slides = sliders.map((slide, i) => (
    <div key={`slider-slide-${i}`}>
      <a href={slide.bannerUrl}>
        <img src={slide.bannerImage} alt={slide.bannerAltText}/>
      </a>
      <div className="big-container">
        <div className="slider-content">
          <p>{slide.bannerCaption}</p>
        </div>
      </div>
    </div>
  ));
  return (
    <div className="hero-slider">
      <div className={Styles.slider}>
        <SlickSlider settings={settings}>
          {slides}
        </SlickSlider>
      </div>
    </div>
  );
};

export default SliderSection;
