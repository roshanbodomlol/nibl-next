import React from 'react';
import Styles from '../style.module.scss';

import { BigTitleBlock } from '../../../Layouts/Block';

const Calculator = () => (
  <div className="container">
    <div className={Styles.calculator}>
      <BigTitleBlock title="EMI Calculator">
        <form action="">
          <div className={Styles.calculatorElements}>
            <span>Amount:</span>
            <div className={Styles.calcElementInner}>
              <input type="text" />
              <span>Rs.</span>
            </div>
          </div>
          <div className={Styles.calculatorElements}>
            <span>Duration:</span>
            <div className={Styles.calcElementInner}>
              <input type="text" />
              <span>month</span>
            </div>
          </div>
          <div className={Styles.calculatorElements}>
            <span>Interest Rate:</span>
            <div className={Styles.calcElementInner}>
              <input type="text" />
              <span>%</span>
            </div>
          </div>
          <div className={Styles.calculatorElements}>
            <span>EMI:</span>
            <div className={Styles.calcElementInner}>
              <input type="text" />
              <span>Rs.</span>
            </div>
          </div>
          <div className={Styles.button}>
            <button className="btnRed">Calculate</button>
            <button className="btnblue">Apply Now</button>
          </div>
        </form>
      </BigTitleBlock>
      <BigTitleBlock title="Stock Exchange">
        <div className={Styles.stockExchange}>
          <p>On Tuesday, Feb 05, 2019</p>
          <div className={Styles.stockExchangeInner}>
            <div>
              <span>Paid-up Value Nrs.</span>
              <span>Closing Nrs.</span>
              <span>Listed Shares</span>
            </div>
            <div>
              <span>100</span>
              <span>525.00</span>
              <span>106,264,357.00</span>
            </div>
          </div>
        </div>
      </BigTitleBlock>
    </div>
  </div>
);

export default Calculator;
