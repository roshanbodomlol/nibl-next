import { useState, useEffect } from 'react';
import classnames from 'classnames';

import TrackVisibility from '../../../TrackVisibility';
import MinimalBlock from '../../../Layouts/Block/MinimalBlock';
import homeLoanImg from '/svg/home-loan.svg';
import businessLoanImg from '/svg/business-loan.svg';
import autoLoanImg from '/svg/auto-loan.svg';
import Styles from '../style.module.scss';

const Loans = ({ title }) => {
  const [minimalBlocks, setMinimalBlocks] = useState([]);
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    setMinimalBlocks([
      {
        isMinimalBlock: true,
        title: 'Home Loan',
        icon: homeLoanImg,
        excerpt: 'Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.'
      },
      {
        isMinimalBlock: true,
        title: 'Business Loan',
        icon: businessLoanImg,
        excerpt: 'Aenean massa. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.'
      },
      {
        isMinimalBlock: true,
        title: 'Auto Loan',
        icon: autoLoanImg,
        excerpt: 'Aenean massa. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.'
      }
    ]);
  }, []);

  const animationClass = classnames(Styles.block, {
    'animated fadeIn': visible
  });

  const blocks = minimalBlocks.filter(block => block.isMinimalBlock);
  let animationTime = 0.5;
  const singleBlock = blocks.map((block, index) => {
    animationTime += 0.2;
    return (
      <div key={`block-${index}`} className={animationClass} style={{ animationDelay: `${animationTime}s` }}>
        <MinimalBlock
          image={block.icon}
          title={block.title}
          content={block.excerpt}
        />
      </div>
    );
  });

  return (
    <div className={Styles.loanWrapper}>
      <div className="container">
        <h1>{title}</h1>
        <TrackVisibility threshold={0.3} onView={() => setVisible(true)}>
          <div className={Styles.minimalBlockFlex}>
            {singleBlock}
          </div>
        </TrackVisibility>
      </div>
    </div>
  );
};

export default Loans;
