import { Component } from 'react';

import BannerBlock from '../../BannerBlock';
import Timeline from '../../Timeline';
import TabContent from '../../TabContent';
import FinancialDetails from '../../FinancialDetails';
import Transformer from './transformer';
import Styles from './styles.module.scss';

class Reports extends Component {
  state = {
    activeItemKey: ''
  }

  componentDidMount() {
    const data = new Transformer(this.props);
    const { activeItemKey } = data;
    this.setState({ activeItemKey });
  }

  render() {
    const data = new Transformer(this.props);
    const { activeItemKey } = this.state;

    const {
      bannerTitle,
      bannerImg,
      bannerDescription,
      bannerAlt,
      items,
      pageTitle,
      bannerUrl
    } = data;

    const financialTable = items.map(item => (
      <FinancialDetails
        key={item.year}
        details={item.details}
        isActive={activeItemKey === item.year}
        title={pageTitle}
      />
    ));

    return (
      <div className={Styles.wrapper}>
        <BannerBlock
          title={bannerTitle}
          description={bannerDescription}
          background={bannerImg}
          alt={bannerAlt}
          link={bannerUrl}
        />
        <div className="white-gradient reset">
          <div className="container">
            <Timeline
              onClick={(key) => { this.setState({ activeItemKey: key }); }}
              items={items}
              activeItemKey={activeItemKey}
            />
            <TabContent>
              {financialTable}
            </TabContent>
          </div>
        </div>
      </div>
    );
  }
}

export default Reports;
