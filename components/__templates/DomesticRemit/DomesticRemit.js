import { withTranslation } from '../../../locales/i18n';
import BannerBlock from '../../BannerBlock';
import LeftImageText from '../../LeftImageText';
import Styles from './domesticRemit.module.scss';
import Transformer from './transformer';

const DomesticRemit = ({ t, ...props }) => {
  const data = new Transformer(props);
  const {
    bannerImg,
    bannerTitle,
    bannerDescription,
    bannerAlt,
    title,
    domesticOverview,
    benefits,
    domesticImage,
    bannerUrl
  } = data;

  return (
    <div className={Styles.wrapper}>
      <BannerBlock
        background={bannerImg}
        title={bannerTitle}
        description={bannerDescription}
        alt={bannerAlt}
        link={bannerUrl}
      />
      <div className={Styles.greyInner}>
        <div className="white-gradient main--content">
          <div className="container">
            <div className={Styles.heading}>
              <h1>{title}</h1>
            </div>
            <LeftImageText image={domesticImage} title={title}>
              <div className="wysiwyg" dangerouslySetInnerHTML={{ __html: domesticOverview }}/>
            </LeftImageText>

            {/* <div className={Styles.heading}>
              <h1>NIBL Domestic Remitance</h1>
            </div>
            <div className={Styles.flexSection}>
              <Row gutter={24} type="flex">
                <Col span={12}>
                  <BigTitleBlock title="Sender Details">
                    <form action="submit">
                      <div className={Styles.formElements}>
                        <label>Name</label>
                        <input type="text" name="name"/>
                      </div>
                      <div className={Styles.formElements}>
                        <label>Sender Address:</label>
                        <input type="text" name="address"/>
                      </div>
                      <div className={Styles.formElements}>
                        <label>Sender Mobile No:</label>
                        <input type="text" name="contact"/>
                      </div>
                      <div className={Styles.buttons}>
                        <ButtonRedBorder title="Send"/>
                        <ButtonRedBorder title="Clear"/>
                      </div>
                    </form>
                  </BigTitleBlock>
                </Col>
                <Col span={12}>
                  <BigTitleBlock title="Receiver Details">
                    <form action="submit">
                      <div className={Styles.formElements}>
                        <label>Receiver Name</label>
                        <input type="text" name="rname"/>
                      </div>
                      <div className={Styles.formElements}>
                        <label>Receiver Address:</label>
                        <input type="text" name="raddress"/>
                      </div>
                      <div className={Styles.formElements}>
                        <label>Sender Mobile No:</label>
                        <input type="text" name="rcontact"/>
                      </div>
                      <div className={Styles.formElements}>
                        <label>ID No:</label>
                        <input type="text" name="rid"/>
                      </div>
                      <div className={Styles.formElements}>
                        <label>Amount to Send:</label>
                        <input type="text" name="ramount"/>
                      </div>
                      <div className={Styles.formElements}>
                        <label>Charge Amount NPR:</label>
                        <input type="text" name="charge_amount"/>
                      </div>
                      <div className={Styles.formElements}>
                        <label>Total Amount NPR:</label>
                        <input type="text" name="rcontact"/>
                      </div>
                      <div className={Styles.buttons}>
                        <ButtonRedBorder title="Send"/>
                        <ButtonRedBorder title="Clear"/>
                      </div>
                    </form>
                  </BigTitleBlock>
                </Col>
              </Row>
            </div> */}
          </div>
        </div>
        <div className={Styles.bottomSection} style={{ background: 'url(/img/objectives-back.jpg)' }}>
          <div className="container">
            <div className={Styles.bottomFlex}>
              <div className={Styles.benifits}>
                <h1>{t('template_domesticRemit_benefits')}</h1>
                <div className="wysiwyg" dangerouslySetInnerHTML={{ __html: benefits }}/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withTranslation('common')(DomesticRemit);
