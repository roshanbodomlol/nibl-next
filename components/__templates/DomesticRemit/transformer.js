export default class Transformer {
  constructor(APIObject) {
    this.bannerImg = APIObject.bannerDetails.bannerImage;
    this.bannerTitle = APIObject.bannerDetails.bannerTitle;
    this.bannerDescription = APIObject.bannerDetails.bannerCaption;
    this.bannerAlt = APIObject.bannerDetails.bannerAlt;
    this.bannerUrl = APIObject.bannerDetails.bannerUrl;
    this.title = APIObject.pageDetails.pageTitle;
    this.domesticOverview = APIObject.pageContent[0].overviewDescription;
    this.domesticImage = APIObject.pageContent[0].overviewImage;
    this.benefits = APIObject.pageContent[0].benefitDescription;
  }
}
