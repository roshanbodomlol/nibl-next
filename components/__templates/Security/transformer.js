import slugs from 'slugs';

export default class Transformer {
  constructor(APIOBJECT) {
    this.bannerImage = APIOBJECT.bannerDetails.bannerImage;
    this.bannerTitle = APIOBJECT.bannerDetails.bannerTitle;
    this.bannerCaption = APIOBJECT.bannerDetails.bannerCaption;
    this.bannerAlt = APIOBJECT.bannerDetails.bannerAlt;
    this.bannerUrl = APIOBJECT.bannerDetails.bannerUrl;
    this.items = APIOBJECT.pageContent.map((item) => (
      {
        name: item.contentCategory,
        content: item.contentDescription,
        key: slugs(item.contentCategory)
      }
    ));
    this.activeKey = this.items[0].key;
  }
}
