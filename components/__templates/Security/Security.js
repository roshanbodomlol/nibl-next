import { useState, useEffect } from 'react';

import BannerBlock from '../../BannerBlock';
import TabNav from '../../TabNav';
import TabContent from '../../TabContent';
import Content from './Content';
import Styles from './security.module.scss';
import Transformer from './transformer';

const Security = (props) => {
  const [activeItemKey, setActiveItemKey] = useState(null);

  const data = new Transformer(props);
  const {
    items, bannerAlt, bannerCaption, bannerImage, bannerTitle, activeKey,
    bannerUrl
  } = data;

  useEffect(() => {
    setActiveItemKey(activeKey);
  }, [activeKey]);

  return (
    <>
      <BannerBlock background={bannerImage} alt={bannerAlt} title={bannerTitle} description={bannerCaption} link={bannerUrl}/>
      <div className="white-gradient main--content">
        <div className="container">
          <div className={Styles.tabnavWrapper}>
            <TabNav
              stretch
              onClick={key => setActiveItemKey(key)}
              activeItemKey={activeItemKey}
              items={items}
            />
          </div>
          <TabContent>
            {
              items.map((item) => (
                <Content isActive={activeItemKey === item.key} content={item.content}/>
              ))
            }
          </TabContent>
        </div>
      </div>
    </>
  );
};

export default Security;
