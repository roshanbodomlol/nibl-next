import React, { Component } from 'react';

// Components
import TabNav from '../../../TabNav';
import TabContent from '../../../TabContent';
import BannerBlock from '../../../BannerBlock';
import BigTitleBlockSet from '../../../Layouts/Block/BigTitleBlockSet';

import Contents from './Contents';

// Images
import Banner from '../../../../assets/img/mobtop-bg.jpg';

class SecurityDetailPage extends Component {
  state = {
    items: [
      {
        name: 'Safe Browsing',
        key: 'safe-browsing'
      },
      {
        name: 'Phising',
        key: 'phising'
      },
      {
        name: 'Safe Banking',
        key: 'safe-banking'
      },
      {
        name: 'Email Fraud',
        key: 'email-fraud'
      },
      {
        name: 'ATM Skimmers',
        key: 'atm-skimmers'
      },
      {
        name: 'Online Safety',
        key: 'online-safety'
      },
      {
        name: 'News',
        key: 'news'
      },
      {
        name: 'Notices',
        key: 'notices'
      }
    ],
    activeItemKey: 'safe-browsing'
  }

  render() {
    const { items, activeItemKey } = this.state;
    return (
      <div>
        <BannerBlock
          background={Banner}
          title="Safe Browsing"
          description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
        />
        <div className="greyWrapper">
          <div className="container">
            <TabNav
              onClick={(key) => { this.setState({ activeItemKey: key }); }}
              activeItemKey={activeItemKey}
              items={items}
            />
            <TabContent>
              <Contents title="Safe Browsing" isActive={activeItemKey === 'safe-browsing'}/>
              <Contents title="Phising" isActive={activeItemKey === 'phising'}/>
              <Contents title="Safe Banking" isActive={activeItemKey === 'safe-banking'}/>
              <Contents title="Email Fraud" isActive={activeItemKey === 'email-fraud'}/>
              <Contents title="ATM Skimmers" isActive={activeItemKey === 'atm-skimmers'}/>
              <Contents title="Online Safety" isActive={activeItemKey === 'online-safety'}/>
              <Contents title="News" isActive={activeItemKey === 'news'}/>
              <Contents title="Notices" isActive={activeItemKey === 'notices'}/>
            </TabContent>
          </div>
        </div>
        <BigTitleBlockSet/>
      </div>
    );
  }
}

export default SecurityDetailPage;
