import ContentCard from '../../ContentCard';

const Content = ({ content }) => (
  <ContentCard>
    <div dangerouslySetInnerHTML={{ __html: content }}/>
  </ContentCard>
);

export default Content;
