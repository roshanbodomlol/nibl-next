import BannerBlock from '../../BannerBlock';
import BODTRANSFORM from './team.transformer';
import Styles from './teamImages.module.scss';

const TeamImages = (props) => {
  const data = new BODTRANSFORM(props);
  const {
    bods,
    bannerTitle,
    bannerCaption,
    bannerAlt,
    bannerImage,
    title,
    bannerUrl
  } = data;
  const bodList = bods.map((bod) => (
    <div role="button" tabIndex="-1" className={Styles.listElement}>
      <img src={bod.image} alt=""/>
      <div className={Styles.bodListText}>
        <span>{bod.name}</span>
        <span>{bod.designation}</span>
      </div>
    </div>
  ));
  return (
    <div className="white-gradient">
      <BannerBlock
        background={bannerImage}
        title={bannerTitle}
        description={bannerCaption}
        alt={bannerAlt}
        link={bannerUrl}
      />
      <div className={Styles.innerWrapper}>
        <div className="container">
          <div className={Styles.heading}>
            <span>{title}</span>
          </div>

          <div className={Styles.list}>
            {bodList}
          </div>
        </div>
      </div>
    </div>
  );
};

export default TeamImages;
