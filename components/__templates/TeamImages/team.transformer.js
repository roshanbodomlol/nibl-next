export default class TeamTransformer {
  constructor(BODAPIOBJECT) {
    this.bannerTitle = BODAPIOBJECT.bannerDetails.bannerTitle;
    this.bannerCaption = BODAPIOBJECT.bannerDetails.bannerCaption;
    this.bannerAlt = BODAPIOBJECT.bannerDetails.bannerAlt;
    this.bannerUrl = BODAPIOBJECT.bannerDetails.bannerUrl;
    this.bannerImage = BODAPIOBJECT.bannerDetails.bannerImage;
    this.bods = BODAPIOBJECT.pageContent.map(e => ({
      image: e.memberImage,
      name: e.memberName,
      designation: e.memberDesignation,
      description: e.memberDescription,
      facebook: e.memberFacebookLnk,
      twitter: e.memberTwitterLnk,
      linkedIn: e.memberLinkedinLnk
    }));
    this.title = BODAPIOBJECT.pageDetails.title;
  }
}
