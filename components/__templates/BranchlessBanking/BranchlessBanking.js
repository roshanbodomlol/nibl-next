import { Component } from 'react';

import BannerBlock from '../../BannerBlock';
import TabNav from '../../TabNav';
import TabContent from '../../TabContent';
import BranchlessBankingContent from './BranchlessBankingContent';
import Strip from '../../Strip';
import Styles from './branchlessBanking.module.scss';

class BranchlessBanking extends Component {
  state = {
    bannerImg: '',
    bannerTitle: '',
    bannerDescription: '',
    items: [],
    activeItemKey: ''
  }

  componentDidMount() {
    this.setState({
      bannerImg: '/img/mobtop-bg.jpg',
      bannerTitle: 'Banchless Banking',
      bannerDescription: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.',
      items: [
        {
          name: 'Branchless Banking',
          key: 'branchless-banking',
          title: 'Branchless Banking',
          content: 'NIBL has deployed a Branchless Banking System that is capable of providing Retail Banking features such as Cash Deposit/Withdrawal, Bill Payments, Fund Transfers and Inquiries to the NIBL Customers. Branchless Banking is an economical channel for delivering financial services without relying on the traditional bank branches. Branchless Banking provides basic banking services through NIBL Agents having Bio-metric POS devices (with finger print scanner). Branchless Banking customers are also provided with NIBL VISA Card, which along with their finger prints can be used to avail services through the POS. The Branchless Banking Service can be availed by New Customers by enrolling to this system and opening account with NIBL or availed by existing customers by simply enrolling to this system. Branchless Banking Services will be provided only in VDCs'
        },
        {
          name: 'How does it work',
          key: 'how-does-it-work',
          title: 'How does it work',
          content: 'NIBL has deployed a Branchless Banking System that is capable of providing Retail Banking features such as Cash Deposit/Withdrawal, Bill Payments, Fund Transfers and Inquiries to the NIBL Customers. Branchless Banking is an economical channel for delivering financial services without relying on the traditional bank branches. Branchless Banking provides basic banking services through NIBL Agents having Bio-metric POS devices (with finger print scanner). Branchless Banking customers are also provided with NIBL VISA Card, which along with their finger prints can be used to avail services through the POS. The Branchless Banking Service can be availed by New Customers by enrolling to this system and opening account with NIBL or availed by existing customers by simply enrolling to this system. Branchless Banking Services will be provided only in VDCs'
  
        }
      ],
      activeItemKey: 'branchless-banking'
    });
  }

  render() {
    const {
      bannerImg,
      bannerTitle,
      bannerDescription,
      items,
      activeItemKey
    } = this.state;

    const tabContentList = items.map(tabContent => (
      <BranchlessBankingContent
        title={tabContent.title}
        content={tabContent.content}
        isActive={activeItemKey === tabContent.key}
      />
    ));

    return (
      <div className={Styles.wrapper}>
        <BannerBlock
          title={bannerTitle}
          description={bannerDescription}
          background={bannerImg}
        />
        <div className="white-gradient main--content">
          <div className="container">
            <TabNav
              onClick={(key) => { this.setState({ activeItemKey: key }); }}
              activeItemKey={activeItemKey}
              items={items}
            />
            <TabContent>
              {tabContentList}
            </TabContent>
          </div>
        </div>
        <div className={Styles.strip}>
          <Strip noInput btnTitle="Download" text="Download the Branchless Banking Application form"/>
        </div>
      </div>
    );
  }
}

export default BranchlessBanking;
