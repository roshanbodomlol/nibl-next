import React from 'react';

import ForexRates from '../../ForexRates';
import BannerBlock from '../../BannerBlock';
import Transformer from './rates.transformer';
import Styles from './dailyRates.module.scss';

const DailyRates = (props) => {
  const data = new Transformer(props);
  const {
    bannerImg,
    bannerTitle,
    bannerDescription,
    bannerAlt,
    bannerUrl
  } = data;

  return (
    <div className={Styles.wrapper}>
      <BannerBlock
        title={bannerTitle}
        description={bannerDescription}
        background={bannerImg}
        alt={bannerAlt}
        link={bannerUrl}
      />
      <div className="white-gradient main--content">
        <div className="container">
          <ForexRates/>
        </div>
      </div>
    </div>
  );
};

export default DailyRates;
