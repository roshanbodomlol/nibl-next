import BannerBlock from '../../BannerBlock';
import TutorialVideos from './TutorialVideos';
import Transformer from './transformer';
import Styles from './tutorials.module.scss';

const Tutorials = (props) => {
  const data = new Transformer(props);
  const {
    bannerAlt,
    bannerCaption,
    bannerImage,
    bannerTitle,
    videos,
    bannerUrl
  } = data;
  return (
    <div className={Styles.wrapper}>
      <BannerBlock
        background={bannerImage}
        title={bannerTitle}
        description={bannerCaption}
        alt={bannerAlt}
        link={bannerUrl}
      />
      <div className="white-gradient main--content">
        <TutorialVideos videos={videos}/>
      </div>
    </div>
  );
};

export default Tutorials;
