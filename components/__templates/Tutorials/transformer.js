export default class Transformer {
  constructor(APIOBJECT) {
    this.bannerTitle = APIOBJECT.bannerDetails.bannerTitle;
    this.bannerImage = APIOBJECT.bannerDetails.bannerImage;
    this.bannerCaption = APIOBJECT.bannerDetails.bannerCaption;
    this.bannerAlt = APIOBJECT.bannerDetails.bannerAlt;
    this.bannerUrl = APIOBJECT.bannerDetails.bannerUrl;
    this.videos = APIOBJECT.pageContent.map(video => ({
      id: video.youtubeLinkId,
      title: video.videoName
    }));
  }
}
