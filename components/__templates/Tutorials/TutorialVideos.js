import React, { Component } from 'react';
import Slider from 'react-slick';
import ModalVideo from 'react-modal-video';
import 'react-modal-video/scss/modal-video.scss';

import VideoCard from '../../VideoCard';
// import SearchBar from '../../SearchBar';
import Styles from './tutorials.module.scss';

const settings = {
  dots: true,
  infinite: false,
  speed: 500,
  slidesToShow: 3,
  slidesToScroll: 1,
  arrows: false,
  rows: 2,
  responsive: [
    {
      breakpoint: 425,
      settings: {
        slidesToShow: 1
      }
    }
  ]
};

class TutorialVideos extends Component {
  state = {
    isOpen: false,
    videoId: null
  };

  openModal = (videoId) => {
    this.setState({ isOpen: true, videoId });
  }

  render() {
    const { isOpen, videoId } = this.state;
    const { videos } = this.props;

    const videoList = videos.map(video => (
      <div className={Styles.videoCard}>
        <VideoCard
          onClick={() => { this.openModal(video.id); }}
          videoThumb={`https://img.youtube.com/vi/${video.id}/0.jpg`}
          videoTitle={video.title}
        />
      </div>
    ));
    return (
      <div className={Styles.videoSection}>
        <ModalVideo channel="youtube" isOpen={isOpen} videoId={videoId} onClose={() => this.setState({ isOpen: false })} />
        <div className="container">
          {/* <SearchBar/> */}

          <div className={Styles.videoCardWrapper}>
            <Slider {...settings}>
              {videoList}
            </Slider>
          </div>
        </div>
      </div>
    );
  }
}

export default TutorialVideos;
