import Products from '../Home/Products';
import BannerBlock from '../../BannerBlock';
import './styles.scss';

const BlockListing = () => (
  <div className="block-listing-wrap">
    <BannerBlock
      title=""
      description=""
      background="/img/mobtop-bg.jpg"
    />
    <div className="white-gradient">
      <div className="main--content">
        <Products title="Accounts"/>
      </div>
    </div>
  </div>
);

export default BlockListing;
