import PropTypes from 'prop-types';
import { Row, Col } from 'antd';

import { withTranslation } from '../../../locales/i18n';
import ContentHeader from '../../ContentHeader';
import TableBody from '../../TableBody';
import Styles from './interestRates.module.scss';

const DepositsTable = ({ details, title, t }) => {
  const depositRates = details.map((detailsItem, index) => (
    <TableBody key={`rateTbl-${index}`}>
      <div className={Styles.gridWrapper}>
        <Row>
          <Col span={2}>
            <span className={Styles.sn}>{index + 1}</span>
          </Col>
          <Col span={10}>
            <span className={Styles.saving}>{detailsItem.contentAccount}</span>
          </Col>
          <Col span={10} style={{ textAlign: 'right' }}>
            <span className={Styles.annum}>{detailsItem.contentInterestRate}</span>
          </Col>
        </Row>
      </div>
    </TableBody>
  ));

  return (
    <div className={Styles.depositsTable}>
      <div className="heading">
        <h1>{title}</h1>
      </div>
      <ContentHeader>
        <div className={Styles.gridWrapper}>
          <Row>
            <Col span={2}>
              <span className={Styles.sn}>S.No</span>
            </Col>
            <Col span={10}>
              <span className={Styles.saving}>{title}</span>
            </Col>
            <Col span={10} style={{ textAlign: 'right' }}>
              <span className={Styles.annum}>{t('template_interestRates_perAnnum')}</span>
            </Col>
          </Row>
        </div>
      </ContentHeader>
      {depositRates}
      {/* <TableBody>
        <div className={Styles.gridWrapper}>
          <span className={Styles.sn}>1</span>
          <span className={Styles.saving}>Afnai Bachat Khata</span>
          <span className={Styles.annum}>3.50</span>
        </div>
      </TableBody>
      <TableBody>
        <div className={Styles.gridWrapper}>
          <span className={Styles.sn}>2</span>
          <span className={Styles.saving}>Afnai Bachat Khata</span>
          <span className={Styles.annum}>3.50</span>
        </div>
      </TableBody>
      <TableBody>
        <div className={Styles.gridWrapper}>
          <span className={Styles.sn}>3</span>
          <span className={Styles.saving}>Afnai Bachat Khata</span>
          <span className={Styles.annum}>3.50</span>
        </div>
      </TableBody>
      <TableBody>
        <div className={Styles.gridWrapper}>
          <span className={Styles.sn}>4</span>
          <span className={Styles.saving}>Afnai Bachat Khata</span>
          <span className={Styles.annum}>3.50</span>
        </div>
      </TableBody>
      <TableBody>
        <div className={Styles.gridWrapper}>
          <span className={Styles.sn}>5</span>
          <span className={Styles.saving}>Afnai Bachat Khata</span>
          <span className={Styles.annum}>3.50</span>
        </div>
      </TableBody>
      <TableBody>
        <div className={Styles.gridWrapper}>
          <span className={Styles.sn}>6</span>
          <span className={Styles.saving}>Afnai Bachat Khata</span>
          <span className={Styles.annum}>3.50</span>
        </div>
      </TableBody> */}
    </div>
  );
};

DepositsTable.propTypes = {
  details: PropTypes.arrayOf(PropTypes.object)
};

DepositsTable.defaultProps = {
  details: []
};

export default withTranslation('common')(DepositsTable);
