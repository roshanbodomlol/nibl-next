import { Component } from 'react';
import { Row, Col } from 'antd';

import { withTranslation } from '../../../locales/i18n';
import BannerBlock from '../../BannerBlock';
import ContentHeader from '../../ContentHeader';
import TableBody from '../../TableBody';
import TabNav from '../../TabNav';
import TabContent from '../../TabContent';
import DepositsTable from './DepositsTable';
import Transformer from './rates.transformer';

import Styles from './interestRates.module.scss';

class InterestRates extends Component {
  state = {
    activeItemKey: null
  };

  componentDidMount() {
    const data = new Transformer(this.props);
    this.setState({ activeItemKey: data.activeItemKey });
  }

  render() {
    const data = new Transformer(this.props);
    const { activeItemKey } = this.state;
    const {
      bannerImg,
      bannerTitle,
      bannerDescription,
      bannerAlt,
      items,
      pageRates,
      effectiveDate,
      baseRateImage,
      interestRateImage,
      bannerUrl,
      note
    } = data;
    const { t } = this.props;

    const baseRates = pageRates.map((ratesItem, index) => (
      <TableBody key={`baseRates-${index}`}>
        <div className={Styles.gridWrapper}>
          <Row gutter={24}>
            <Col span={2}>
              <span className={Styles.sn}>{index + 1}</span>
            </Col>
            <Col span={5}>
              <span className={Styles.saving}>{ratesItem.title}</span>
            </Col>
            <Col span={5}>
              <span className={Styles.annum}>{ratesItem.perAnnum}</span>
            </Col>
            <Col span={10}>
              <span>{ratesItem.description}</span>
            </Col>
          </Row>
        </div>
      </TableBody>
    ));

    const depositInterestRates = items.map((item, index) => (
      <DepositsTable
        key={`rates-${index}`}
        details={item.details}
        isActive={activeItemKey === item.key}
        title={item.name}
      />
    ));

    return (
      <div className={Styles.wrapper}>
        <BannerBlock
          title={bannerTitle}
          description={bannerDescription}
          background={bannerImg}
          alt={bannerAlt}
          link={bannerUrl}
        />
        <div className="white-gradient main--content">
          <div className="container">
            <div className="heading">
              <h1>{t('template_interestRates_baseRates')}</h1>
            </div>
            <Row gutter={30}>
              <Col md={24} xs={24}>
                <ContentHeader grey>
                  <div className={Styles.gridWrapper}>
                    <Row gutter={24}>
                      <Col span={2}>
                        <span className={Styles.sn}>S.No</span>
                      </Col>
                      <Col span={5}>
                        <span className={Styles.saving}>{t('template_interestRates_savingAccount')}</span>
                      </Col>
                      <Col span={5}>
                        <span className={Styles.annum}>%</span>
                      </Col>
                      <Col span={10}>
                        <span>{t('template_interestRates_description')}</span>
                      </Col>
                    </Row>
                  </div>
                </ContentHeader>
                {baseRates}
              </Col>
              {/* <Col md={12} xs={24}>
                <img src={baseRateImage} alt=""/>
              </Col> */}
            </Row>
          </div>
        </div>

        <div className={Styles.bottomSection}>
          <div className="container">
            <Row gutter={30}>
              <Col md={24} xs={24}>
                <div className={Styles.noteText}>
                  <span>{effectiveDate}</span><br/>
                </div>

                <TabNav
                  onClick={(key) => { this.setState({ activeItemKey: key }); }}
                  activeItemKey={activeItemKey}
                  items={items}
                />
                <div className={Styles.tabContent}>
                  <TabContent>
                    {depositInterestRates}
                  </TabContent>
                </div>
              </Col>
              {/* <Col md={12} xs={24}>
                <div className="_spaced_tp">
                  <img src={interestRateImage} alt=""/>
                </div>
              </Col> */}
            </Row>
            <div className={Styles.noteText} dangerouslySetInnerHTML={{ __html: note }}/>
          </div>
        </div>
      </div>
    );
  }
}

export default withTranslation('common')(InterestRates);
