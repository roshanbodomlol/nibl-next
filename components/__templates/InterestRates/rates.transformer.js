import { chain, zipObject } from 'lodash';
import slugs from 'slugs';

export default class Transformer {
  constructor(APIObject) {
    this.bannerImg = APIObject.bannerDetails.bannerImage;
    this.bannerTitle = APIObject.bannerDetails.bannerTitle;
    this.bannerDescription = APIObject.bannerDetails.bannerCaption;
    this.bannerAlt = APIObject.bannerDetails.bannerAlt;
    this.bannerUrl = APIObject.bannerDetails.bannerUrl;
    this.pageRates = APIObject.baseRates.map(rate => ({
      title: rate.contentTitle,
      perAnnum: rate.contentAmount,
      description: rate.contentDescription
    }));
    const allItems = chain(APIObject.interestRates)
      .groupBy('contentCategory')
      .toPairs()
      .map(currentData => zipObject(['name', 'details'], currentData))
      .value()
      .map(item => ({
        ...item,
        key: slugs(item.name)
      }));
    this.items = allItems.filter(item => item.key !== 'cards');
    this.activeItemKey = this.items[0] && this.items[0].key;
    this.effectiveDate = APIObject.pageContent.effectiveDate;
    this.baseRateImage = APIObject.pageContent.baseRateImage;
    this.interestRateImage = APIObject.pageContent.interestRateImage;
    this.note = APIObject.pageContent.descriptionNote;
  }
}
