import BannerBlock from '../../BannerBlock';
import ContentCard from '../../ContentCard';
import Transformer from './generalContent.transformer';

const FullWidthContentCard = (props) => {
  const data = new Transformer(props);
  const {
    bannerImg,
    bannerTitle,
    bannerDescription,
    bannerAlt,
    title,
    content,
    bannerUrl
  } = data;

  return (
    <div>
      <BannerBlock
        title={bannerTitle}
        description={bannerDescription}
        background={bannerImg}
        alt={bannerAlt}
        link={bannerUrl}
      />
      <div className="white-gradient main--content">
        <div className="container">
          <div className="heading">
            <h1>{title}</h1>
          </div>
          <ContentCard>
            <div className="wysiwyg" dangerouslySetInnerHTML={{ __html: content }}/>
          </ContentCard>
        </div>
      </div>
    </div>
  );
};

export default FullWidthContentCard;
