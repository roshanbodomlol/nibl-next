export default class Transformer {
  constructor(APIObject) {
    this.bannerImg = APIObject.bannerDetails.bannerImage;
    this.bannerDescription = APIObject.bannerDetails.bannerCaption;
    this.bannerTitle = APIObject.bannerDetails.bannerTitle;
    this.bannerAlt = APIObject.bannerDetails.bannerAlt;
    this.bannerUrl = APIObject.bannerDetails.bannerUrl;
    this.title = APIObject.pageDetails.pageTitle;
    this.content = APIObject.pageContent[0].contentDescription;
  }
}
