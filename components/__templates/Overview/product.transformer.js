export default class ProductTransformer {
  constructor(ProductApiObject) {
    this.bannerImg = ProductApiObject.bannerDetails.bannerImage;
    this.bannerTitle = ProductApiObject.bannerDetails.bannerAlt;
    this.bannerDesc = ProductApiObject.bannerDetails.bannerCaption;
    this.bannerLink = ProductApiObject.bannerDetails.bannerUrl;
    this.productTitle = ProductApiObject.pageDetails.pageTitle;
    this.overviewTitle = ProductApiObject.overviewContent.overviewTitle;
    this.productImage = ProductApiObject.overviewContent.overviewImage;
    this.productImageAlt = ProductApiObject.overviewContent.overviewImageAltText;
    this.productOverviewExcerpt = ProductApiObject.overviewContent.overviewCaption;
    this.productDetails = ProductApiObject.overviewContent.overviewDescription;
    this.firstTableTitle = ProductApiObject.featureContent.featureTitle;
    this.firstTableContent = ProductApiObject.featureContent.featureDescription;
    this.featuredBlockImage = ProductApiObject.featureContent.featureImage;
    this.featuredBlockImageCaption = ProductApiObject.featureContent.featureImageCaption;
    this.featuredBlockImageTitle = ProductApiObject.featureContent.featureImageTitle;
    this.featuredBlockImageLink = ProductApiObject.featureContent.featureImageDetailLink;
    this.featuredImageAlt = ProductApiObject.featureContent.featureImageAltText;
    this.secondTableTitle = ProductApiObject.benefitContent.benefitTitle;
    this.secondTableContent = ProductApiObject.benefitContent.benefitDescription;
    this.benefitsBlockImage = ProductApiObject.benefitContent.benefitImage;
    this.benefitsBlockImageCaption = ProductApiObject.benefitContent.benefitImageCaption;
    this.benefitsBlockImageTitle = ProductApiObject.benefitContent.benefitImageTitle;
    this.benefitsBlockImageLink = ProductApiObject.benefitContent.benefitImageDetailLink;
    this.benefitsImageAlt = ProductApiObject.benefitContent.benefitImageAltText;
    this.questions = [
      {
        question: ProductApiObject.faqContent.howToApplyQstn1,
        answer: ProductApiObject.faqContent.howToApplyAns1,
        link: ProductApiObject.faqContent.pageUploadedFile1
      },
      {
        question: ProductApiObject.faqContent.howToApplyQstn2,
        answer: ProductApiObject.faqContent.howToApplyAns2,
        link: ProductApiObject.faqContent.pageUploadedFile2
      }
    ];
    this.blocks = [
      {
        title: ProductApiObject.otherContent.toolTitle,
        description: ProductApiObject.otherContent.toolDescription,
        link: ProductApiObject.otherContent.toolLink
      },
      {
        title: ProductApiObject.otherContent.downloadTitle,
        description: ProductApiObject.otherContent.downloadDescription,
        link: ProductApiObject.otherContent.downloadLink
      },
      {
        title: ProductApiObject.otherContent.interestRateTitle,
        description: ProductApiObject.otherContent.interestRateDescription,
        link: ProductApiObject.otherContent.interestRateLink
      }
    ];
  }
}
