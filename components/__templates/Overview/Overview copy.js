import React, { Component } from 'react';
import { withRouter } from 'next/router';
import OnImagesLoaded from 'react-on-images-loaded';

import { withTranslation } from '../../../locales/i18n';
import BannerBlock from '../../BannerBlock';
import LeftImageText from '../../LeftImageText';
import FeaturesAndBenefits from '../../FeaturesAndBenefits';
import Strip from '../../Strip';
import GeneralBlocks from '../../generalBlocks';
import HowToApplyBlock from '../../HowToApply';
import TabNav from '../../TabNav';
import Loading from '../../Loading';
import ProductTransformer from './product.transformer';
import css from './overview.module.scss';

class Overview extends Component {
  state = {
    activeTab: 'details'
  };

  detailsRef = React.createRef();

  fnbRef = React.createRef();

  blocksRef = React.createRef();

  componentDidMount() {
    const { router } = this.props;
    router.events.on('hashChangeComplete', this.handleHashChange);
  }

  handleHashChange = url => {
    const tabQuery = url.split('#')[1];

    if (tabQuery) {
      this.handleTabClick(tabQuery);
    }
  };

  autoScroll = () => {
    const { router } = this.props;
    const tabQuery = router.asPath.split('#')[1];

    if (tabQuery) {
      this.handleTabClick(tabQuery);
    }
  };

  handleTabClick = (key) => {
    this.setState({ activeTab: key });
    if (key === 'details') {
      window.scrollTo({ top: window.scrollY + this.detailsRef.getBoundingClientRect().top - 120, behavior: 'smooth' });
    } else if (key === 'features') {
      window.scrollTo({ top: window.scrollY + this.fnbRef.featuresRef.getBoundingClientRect().top - 120, behavior: 'smooth' });
    } else if (key === 'benefits') {
      window.scrollTo({ top: window.scrollY + this.fnbRef.benefitsRef.getBoundingClientRect().top - 120, behavior: 'smooth' });
    } else if (key === 'how-to-apply') {
      window.scrollTo({ top: window.scrollY + this.blocksRef.getBoundingClientRect().top - 120, behavior: 'smooth' });
    }
  };

  render() {
    const { t, ...props } = this.props;
    const { activeTab } = this.state;
    const data = new ProductTransformer(props);
    const {
      bannerImg,
      bannerTitle,
      bannerDesc,
      bannerLink,
      productTitle,
      productImage,
      productImageAlt,
      productOverviewExcerpt,
      productDetails,
      firstTableTitle,
      firstTableContent,
      secondTableTitle,
      secondTableContent,
      blocks,
      loading,
      featuredBlockImage,
      featuredImageAlt,
      featuredBlockImageCaption,
      featuredBlockImageTitle,
      featuredBlockImageLink,
      benefitsImageAlt,
      benefitsBlockImage,
      benefitsBlockImageCaption,
      benefitsBlockImageTitle,
      benefitsBlockImageLink,
      questions
    } = data;

    return (
      <div className={css.wrapper}>
        {
          loading
            ? <Loading color="red" fullHeight/>
            : (
              <OnImagesLoaded onLoaded={this.autoScroll}>
                <BannerBlock
                  background={bannerImg}
                  title={bannerTitle}
                  description={bannerDesc}
                  link={bannerLink}
                  linkTitle="Apply Now"
                />
                <div className="white-gradient">
                  {
                    activeTab && (
                      <div className={css.tabs}>
                        <TabNav
                          items={[
                            {
                              name: 'Details',
                              key: 'details'
                            },
                            {
                              name: 'Features',
                              key: 'features'
                            },
                            {
                              name: 'Benefits',
                              key: 'benefits'
                            },
                            // {
                            //   name: 'How To Apply',
                            //   key: 'how-to-apply'
                            // }
                          ]}
                          activeItemKey={activeTab}
                          onClick={this.handleTabClick}
                        />
                      </div>
                    )
                  }
                  <div className={css.greyInner}>
                    <div className="container">
                      <div className={css.heading} ref={(c) => { this.detailsRef = c; }}>
                        <span>{t('template_overview_title')}</span>
                      </div>
                      <div className={css.contentWrapper}>
                        <LeftImageText
                          redTitle
                          title={productTitle}
                          excerpt={productOverviewExcerpt}
                          image={productImage}
                          imageHeight={400}
                          imageAlt={productImageAlt}
                        >
                          <div dangerouslySetInnerHTML={{ __html: productDetails }}/>
                          {/* <ButtonRedBorder title="Apply Now"/> */}
                        </LeftImageText>
                        <FeaturesAndBenefits
                          ref={(c) => { this.fnbRef = c; }}
                          firstTableTitle={firstTableTitle}
                          secondTableTitle={secondTableTitle}
                          firstTableContent={firstTableContent}
                          secondTableContent={secondTableContent}
                          features={firstTableContent}
                          benefits={secondTableContent}
                          featuredBlockImage={featuredBlockImage}
                          featuredImageAlt={featuredImageAlt}
                          featuredBlockImageCaption={featuredBlockImageCaption}
                          featuredBlockImageTitle={featuredBlockImageTitle}
                          featuredBlockImageLink={featuredBlockImageLink}
                          benefitsBlockImage={benefitsBlockImage}
                          benefitsImageAlt={benefitsImageAlt}
                          benefitsBlockImageCaption={benefitsBlockImageCaption}
                          benefitsBlockImageTitle={benefitsBlockImageTitle}
                          benefitsBlockImageLink={benefitsBlockImageLink}
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className={css.searchSectionWrapper}>
                  <Strip text="Subscribe to get our newest offers"/>
                </div>
                <div className="white-gradient main--content">
                  <div className="container" ref={(c) => { this.blocksRef = c; }}>
                    {/* <HowToApplyBlock
                      questions={questions}
                    /> */}
                    <GeneralBlocks blocks={blocks}/>
                    {/* // TODO - FIX */}
                    {/* <SliderWithVideo/> */}
                    {/* <div className={css.moreAcc}>
                      <div className="heading">
                        <h1>More Personal Accounts</h1>
                      </div>
                      <div className={css.gridBlock}>
                        <div className={css.block}>
                          <Card
                            title="Lotus Account"
                            content="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. "
                            image={icon}
                          />
                        </div>
                        <div className={css.block}>
                          <Card
                            title="Afnai Bachat Khata"
                            content="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. "
                            image={icon}
                          />
                        </div>
                        <div className={css.block}>
                          <Card
                            title="Saving Account"
                            content="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. "
                            image={icon}
                          />
                        </div>
                      </div>
                    </div> */}
                  </div>
                </div>
              </OnImagesLoaded>
            )
        }
      </div>
    );
  }
}

export default withTranslation('common')(withRouter(Overview));
