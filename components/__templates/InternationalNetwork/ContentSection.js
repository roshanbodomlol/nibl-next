import { Row, Col } from 'antd';

import { withTranslation } from '../../../locales/i18n';
import ContentCard from '../../ContentCard';
import Styles from './internationalNetwork.module.scss';

const ContentSection = ({ details, marketingItems, t }) => (
  <div className={Styles.contentSection}>
    <div className="heading">
      <h1>{t('template_internationalNetwork_countryRepresentatives')}</h1>
    </div>
    <ContentCard>
      <div className={Styles.contentCard}>
        <Row type="flex" gutter={18}>
          {
            details.map((item, index) => (
              <Col sm={8} xs={24} key={`item-${index}`}>
                <div className={Styles.content}>
                  <span style={{ color: '#ed1c24', fontSize: 14 }}>{item.contentTitle}</span>
                  <div dangerouslySetInnerHTML={{ __html: item.contentDescription }}/>
                </div>
              </Col>
            ))
          }
        </Row>
      </div>
    </ContentCard>
    {
      !!marketingItems && marketingItems.length > 0 && (
        <>
          <div className="heading _heading">
            <h1>{t('template_internationalNetwork_marketingRepresentatives')}</h1>
          </div>
          <ContentCard>
            <div className={Styles.contentCard}>
              <Row type="flex">
                {
                  marketingItems.map((item, i) => (
                    <Col sm={8} xs={24} key={`iitem-${i}`}>
                      <div className={Styles.contentElement}>
                        <span>{t('global_name')}: {item.contentName}</span>
                        <span>{t('global_mobile')}: {item.contentMobile}</span>
                        <span><a href={`mailto:${item.contentEmail}`}>{t('global_email')}: {item.contentEmail}</a></span>
                      </div>
                    </Col>
                  ))
                }
              </Row>
            </div>
          </ContentCard>
        </>
      )
    }
  </div>
);

export default withTranslation('common')(ContentSection);
