import { Component } from 'react';

import BannerBlock from '../../BannerBlock';
import TabNav from '../../TabNav';
import TabContent from '../../TabContent';
import ContentSection from './ContentSection';
import Styles from './internationalNetwork.module.scss';
import Transformer from './transformer';

class InternationalNetwork extends Component {
  state = {
    activeItemKey: null
  };

  componentDidMount() {
    const data = new Transformer(this.props);
    const { activeItemKey } = data;
    this.setState({ activeItemKey });
  }

  render() {
    const data = new Transformer(this.props);
    const {
      items, bannerDescription, bannerImg, bannerAlt,
      bannerTitle, marketingItems, bannerUrl
    } = data;
    const { activeItemKey } = this.state;

    items.forEach((item) => {
      const marketingItem = marketingItems.find(e => e.name === item.name);
      item.marketingDetails = marketingItem ? marketingItem.details : [];
    });

    return (
      <div className={Styles.wrapper}>
        <BannerBlock
          title={bannerTitle}
          description={bannerDescription}
          background={bannerImg}
          alt={bannerAlt}
          link={bannerUrl}
        />
        <div className="white-gradient main--content">
          <div className="container">
            <TabNav
              onClick={(key) => { this.setState({ activeItemKey: key }); }}
              activeItemKey={activeItemKey}
              items={items}
            />
            <div className={Styles.tabContent}>
              <TabContent>
                {
                  items.map((item, index) => (
                    <div key={`int-network-${index}`} isActive={activeItemKey === item.key}>
                      <ContentSection details={item.details} marketingItems={item.marketingDetails}/>
                    </div>
                  ))
                }
              </TabContent>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default InternationalNetwork;
