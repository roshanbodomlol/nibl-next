export default class Transformer {
  constructor(RESEARCHAPIOBJ) {
    this.bannerImg = RESEARCHAPIOBJ.bannerDetails.bannerImage;
    this.bannerTitle = RESEARCHAPIOBJ.bannerDetails.bannerTitle;
    this.bannerDescription = RESEARCHAPIOBJ.bannerDetails.bannerCaption;
    this.bannerAlt = RESEARCHAPIOBJ.bannerDetails.bannerAlt;
    this.items = [
      {
        year: 2012,
        details: [
          {
            primary: 'Download NIBL Mobile (SMS) Banking Application 2012?',
            secondary: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo lasd',
            downloadLink: '#'
          },
          {
            primary: 'Download NIBL Mobile (SMS) Banking Application?',
            secondary: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo lasd',
            downloadLink: '#'
          },
          {
            primary: 'Download NIBL Mobile (SMS) Banking Application?',
            secondary: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo lasd',
            downloadLink: '#'
          },
          {
            primary: 'Download NIBL Mobile (SMS) Banking Application?',
            secondary: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo lasd',
            downloadLink: '#'
          }
        ]
      },
      {
        year: 2013,
        details: [
          {
            primary: 'Download NIBL Mobile (SMS) Banking Application 2013?',
            secondary: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo lasd',
            downloadLink: '#'
          },
          {
            primary: 'Download NIBL Mobile (SMS) Banking Application?',
            secondary: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo lasd',
            downloadLink: '#'
          }
        ]
      },
      {
        year: 2014
      },
      {
        year: 2015
      }
    ];
    this.activeItemKey = 2012;
  }
}
