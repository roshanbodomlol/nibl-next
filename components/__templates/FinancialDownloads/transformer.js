import { chain, zipObject } from 'lodash';

export default class Transformer {
  constructor(RESEARCHAPIOBJ) {
    this.bannerImg = RESEARCHAPIOBJ.bannerDetails.bannerImage;
    this.bannerTitle = RESEARCHAPIOBJ.bannerDetails.bannerTitle;
    this.bannerDescription = RESEARCHAPIOBJ.bannerDetails.bannerCaption;
    this.bannerAlt = RESEARCHAPIOBJ.bannerDetails.bannerAlt;
    this.bannerUrl = RESEARCHAPIOBJ.bannerDetails.bannerUrl;
    this.items = chain(RESEARCHAPIOBJ.pageContent)
      .groupBy('contentYear')
      .toPairs()
      .map(currentData => zipObject(['year', 'details'], currentData))
      .value()
      .map(item => ({
        ...item,
        details: item.details.map(e => ({
          primary: e.contentTitle,
          secondary: e.contentDescription,
          downloadLink: e.contentFile
        }))
      }));
    this.activeItemKey = this.items[0] && this.items[0].year;
  }
}
