import { Component } from 'react';

import { withTranslation } from '../../../locales/i18n';
import BannerBlock from '../../BannerBlock';
import Timeline from '../../Timeline';
import TabContent from '../../TabContent';
import FinancialDetails from '../../FinancialDetails';
import Transformer from './transformer';
import Styles from './styles.module.scss';

class FinancialDownloads extends Component {
  state = {
    activeItemKey: ''
  };

  componentDidMount() {
    const data = new Transformer(this.props);
    const { activeItemKey } = data;
    this.setState({ activeItemKey });
  }

  render() {
    const data = new Transformer(this.props);
    const {
      bannerTitle,
      bannerImg,
      bannerDescription,
      items,
      bannerUrl,
      bannerAlt
    } = data;
    const { activeItemKey } = this.state;
    const { t, pageNetWorth } = this.props;

    const financialTable = items.map(item => (
      <FinancialDetails
        key={item.year}
        year={item.year}
        details={item.details}
        isActive={activeItemKey === item.year}
        title="FINANCIAL DETAILS"
        showNetWorth
        netWorthData={pageNetWorth}
      />
    ));

    return (
      <div className={Styles.wrapper}>
        <BannerBlock
          title={bannerTitle}
          description={bannerDescription}
          background={bannerImg}
          link={bannerUrl}
          alt={bannerAlt}
        />
        <div className="white-gradient reset">
          <div className="container">
            {
              financialTable.length > 0
                ? (
                  <>
                    <Timeline
                      onClick={(key) => { this.setState({ activeItemKey: key }); }}
                      items={items}
                      activeItemKey={activeItemKey}
                    />
                    <TabContent>
                      {financialTable}
                    </TabContent>
                  </>
                )
                : (
                  <p>{t('global_noDataAvailable')}</p>
                )
            }
          </div>
        </div>
      </div>
    );
  }
}

export default withTranslation('common')(FinancialDownloads);
