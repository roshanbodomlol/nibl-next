import MapWithWhiteCard from '../../MapWithWhiteCard';
import BannerBlock from '../../BannerBlock';
import Transformer from './transformer';

const CorporateOffice = (props) => {
  const data = new Transformer(props);
  const {
    bannerAlt,
    bannerDescription,
    bannerImage,
    bannerTitle,
    contactInformation,
    iframe,
    bannerUrl
  } = data;
  return (
    <div>
      <BannerBlock
        title={bannerTitle}
        description={bannerDescription}
        background={bannerImage}
        alt={bannerAlt}
        link={bannerUrl}
      />
      <div className="white-gradient main--content">
        <div className="container">
          <MapWithWhiteCard content={contactInformation} map={iframe}/>
        </div>
      </div>
    </div>
  );
};

export default CorporateOffice;
