export default class Transformer {
  constructor(APIObject) {
    this.bannerImage = APIObject.bannerDetails.bannerImage;
    this.bannerDescription = APIObject.bannerDetails.bannerCaption;
    this.bannerTitle = APIObject.bannerDetails.bannerTitle;
    this.bannerAlt = APIObject.bannerDetails.bannerAlt;
    this.bannerUrl = APIObject.bannerDetails.bannerUrl;
    this.contactInformation = APIObject.pageContent[0].officeContent;
    this.iframe = APIObject.pageContent[0].officeLocation;
  }
}
