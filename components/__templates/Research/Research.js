import { Component } from 'react';
import { Spin } from 'antd';

import { withTranslation } from '../../../locales/i18n';
import BannerBlock from '../../BannerBlock';
import TabNav from '../../TabNav';
import TabContent from '../../TabContent';
import SavingAccount from './SavingAccount';
import Transformer from './transformer';

import Styles from './research.module.scss';

class Research extends Component {
  state = {
    activeItemKey: ''
  };

  limit = -1;

  componentDidMount() {
    const data = new Transformer(this.props);
    this.setState({ activeItemKey: data.activeItemKey });
  }

  handleNext = () => {
    const { items, activeItemKey } = this.state;

    let index = items.findIndex(item => item.key === activeItemKey);

    if (index === items.length - 1) {
      index = 0;
    } else {
      index++;
    }

    this.setState({ activeItemKey: items[index].key });
  }

  handlePrevious = () => {
    const { items, activeItemKey } = this.state;

    let index = items.findIndex(item => item.key === activeItemKey);

    if (index === 0) {
      index = items.length - 1;
    } else {
      index--;
    }

    this.setState({ activeItemKey: items[index].key });
  }

  render() {
    const data = new Transformer(this.props);
    const { t } = this.props;

    const {
      loading,
      bannerImg,
      bannerTitle,
      bannerDescription,
      bannerAlt,
      pageContent,
      pageTitle,
      items,
      bannerUrl
    } = data;
    const { activeItemKey } = this.state;

    const contentTable = items.map((item, index) => (
      <SavingAccount
        key={`contentTable-${index}`}
        details={item.details}
        isActive={activeItemKey === item.key}
      />
    ));

    return (
      <div className={Styles.wrapper}>
        {
          loading
            ? (
              <div className="page-loader">
                <Spin/>
              </div>
            )
            : (
              <>
                <BannerBlock
                  title={bannerTitle}
                  description={bannerDescription}
                  background={bannerImg}
                  alt={bannerAlt}
                  link={bannerUrl}
                />
                <div className="white-gradient reset">
                  <div className="container">
                    {
                      pageContent && (
                        <>
                          <h1>{pageTitle}</h1>
                          <div className="wysiwyg">
                            {pageContent}
                          </div>
                        </>
                      )
                    }
                    <TabNav
                      onClick={(key) => { this.setState({ activeItemKey: key }); }}
                      activeItemKey={activeItemKey}
                      items={items}
                      onLeftArrowClick={this.handlePrevious}
                      onRightArrowClick={this.handleNext}
                    />
                    <div className="heading">
                      <h1>{t('global_downloads')}</h1>
                    </div>
                    {
                      contentTable.length > 0
                        ? (
                          <TabContent>
                            {contentTable}
                          </TabContent>
                        )
                        : (
                          <p>{t('global_noDataAvailable')}</p>
                        )
                    }
                  </div>
                </div>
              </>
            )
        }
      </div>
    );
  }
}

export default withTranslation('common')(Research);
