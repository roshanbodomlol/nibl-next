import PropTypes from 'prop-types';

import { withTranslation } from '../../../locales/i18n';
import ContentHeader from '../../ContentHeader';
import ExpansionPanel from '../../ExpansionPanel';
import Styles from './research.module.scss';

const SavingAccount = ({ details, t }) => {
  const contentTable = details.map((detailsItem, index) => (
    {
      primary: (
        <div className={`${Styles.primaryPanel}`} key={`downloads-primary-${index}`}>
          <div>
            <p>
              {detailsItem.contentTitle}
            </p>
          </div>
          <a download href={`${detailsItem.contentFile}`}>
            <img src="/img/download.png" alt=""/>
          </a>
        </div>
      ),
      secondary: (
        <div key={`downloads-secondary-${index}`}>
          {detailsItem.contentDescription}
        </div>
      )
    }
  ));

  return (
    <div className={Styles.savingAccount}>
      <ContentHeader grey>
        <div className={Styles.gridWrapper}>
          <span className={Styles.sn}>S.No</span>
          <span className={Styles.details}>{t('global_details')}</span>
        </div>
      </ContentHeader>
      <ExpansionPanel
        isOrdered
        items={contentTable}
      />
    </div>
  );
};

SavingAccount.propTypes = {
  details: PropTypes.arrayOf(PropTypes.object)
};

SavingAccount.defaultProps = {
  details: []
};

export default withTranslation('common')(SavingAccount);
