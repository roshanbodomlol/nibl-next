import slugs from 'slugs';
import { chain, zipObject } from 'lodash';

export default class Transformer {
  constructor(ResearchAPIObject) {
    this.bannerImg = ResearchAPIObject.bannerDetails.bannerImage;
    this.bannerTitle = ResearchAPIObject.bannerDetails.bannerTitle;
    this.bannerDescription = ResearchAPIObject.bannerDetails.bannerCaption;
    this.bannerAlt = ResearchAPIObject.bannerDetails.bannerAlt;
    this.bannerUrl = ResearchAPIObject.bannerDetails.bannerUrl;
    this.items = chain(ResearchAPIObject.pageContent)
      .groupBy('contentCategory')
      .toPairs()
      .map(currentData => zipObject(['name', 'details'], currentData))
      .value()
      .map(item => ({
        ...item,
        key: slugs(item.name)
      }));
    this.activeItemKey = this.items[0] && this.items[0].key;
    this.pageContent = ResearchAPIObject.pageOverview.contentOverview;
    this.pageTitle = ResearchAPIObject.pageDetails.pageTitle;
  }
}
