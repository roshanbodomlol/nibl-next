import { Row, Col } from 'antd';

import { withTranslation } from '../../../locales/i18n';
import BannerBlock from '../../BannerBlock';
import ContentHeader from '../../ContentHeader';
import TableBody from '../../TableBody';
import Transformer from './transformer';
import Styles from './styles.module.scss';

const CorrespondentBank = ({ t, ...props }) => {
  const data = new Transformer(props);
  const {
    bannerImg,
    bannerTitle,
    bannerDescription,
    bannerAlt,
    pageTitle,
    details,
    bannerUrl
  } = data;

  const correspondentBankTbl = details.map((item, index) => (
    <TableBody>
      <div className={Styles.tableHeadTitles}>
        <Row>
          <Col span={2}>
            <span className={Styles.sn}>{index + 1}</span>
          </Col>
          <Col span={8}>
            <span className={Styles.currency}>
              <img className={Styles.flag} src={item.flag} alt=""/>
              {item.currency}
            </span>
          </Col>
          <Col span={8}>
            <span className={Styles.bank}>{item.bank}</span>
          </Col>
          <Col span={6}>
            <span className={Styles.address}>{item.swiftAddress}</span>
          </Col>
        </Row>
      </div>
    </TableBody>
  ));

  return (
    <div className={Styles.wrapper}>
      <BannerBlock
        background={bannerImg}
        title={bannerTitle}
        description={bannerDescription}
        alt={bannerAlt}
        link={bannerUrl}
      />
      <div className="white-gradient">
        <div className={Styles.innerWrapper}>
          <div className="container">
            <div className="heading">
              <h1>{pageTitle}</h1>
            </div>
            <ContentHeader grey>
              <div className={Styles.tableHeadTitles}>
                <Row>
                  <Col span={2}>
                    <span className={Styles.sn}>S.No</span>
                  </Col>
                  <Col span={8}>
                    <span className={Styles.currency}>{t('global_country')}/{t('global_currency')}</span>
                  </Col>
                  <Col span={8}>
                    <span className={Styles.bank}>Bank</span>
                  </Col>
                  <Col span={6}>
                    <span className={Styles.address}>{t('template_correspondentBank_swift')}</span>
                  </Col>
                </Row>
              </div>
            </ContentHeader>
            {correspondentBankTbl}
          </div>
        </div>
      </div>
    </div>
  );
};

export default withTranslation('common')(CorrespondentBank);
