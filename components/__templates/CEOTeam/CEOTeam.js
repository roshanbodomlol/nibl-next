import BannerBlock from '../../BannerBlock';
import BODTRANSFORM from './bod.transformer';
import BODList from '../../BODList';
import Styles from './boardOfDirectors.module.scss';

const CEOTeam = (props) => {
  const data = new BODTRANSFORM(props);

  const {
    bods,
    bannerTitle,
    bannerCaption,
    bannerAlt,
    bannerImage,
    pageTitle,
    bannerUrl
  } = data;

  return (
    <div className="white-gradient">
      <BannerBlock
        background={bannerImage}
        title={bannerTitle}
        description={bannerCaption}
        alt={bannerAlt}
        link={bannerUrl}
      />
      <div className={Styles.innerWrapper}>
        <div className="container">
          <BODList ceo bods={bods} title={pageTitle}/>
        </div>
      </div>
    </div>
  );
};

export default CEOTeam;
