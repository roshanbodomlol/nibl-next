export default class COMPLIANCETransfomer {
  constructor(COMPLIANCEAPIOBJECT) {
    this.bannerImg = COMPLIANCEAPIOBJECT.bannerDetails.bannerImage;
    this.bannerTitle = COMPLIANCEAPIOBJECT.bannerDetails.bannerTitle;
    this.bannerDescription = COMPLIANCEAPIOBJECT.bannerDetails.bannerDescription;
    this.bannerAlt = COMPLIANCEAPIOBJECT.bannerDetails.bannerAlt;
    this.bannerUrl = COMPLIANCEAPIOBJECT.bannerDetails.bannerUrl;
    this.pageTitle = COMPLIANCEAPIOBJECT.pageDetails.pageTitle;

    this.posts = COMPLIANCEAPIOBJECT.pageContent.map(e => ({
      title: e.Title,
      image: e.Image,
      description: e.Description,
      btnText: 'Download Form',
      btnLink: e.UploadedFile
    }));
  }
}
