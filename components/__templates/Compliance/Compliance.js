import BannerBlock from '../../BannerBlock';
import LeftImageText from '../../LeftImageText';
import LeftTextImage from '../../LeftTextImage';
import COMPLIANCETRANSFORM from './compliance.transformer';
import Styles from './compliance.module.scss';

const Aml = (props) => {
  const data = new COMPLIANCETRANSFORM(props);
  const {
    bannerImg,
    bannerTitle,
    bannerDescription,
    bannerAlt,
    pageTitle,
    posts,
    bannerUrl
  } = data;

  const postsList = posts.map((post, index) => {
    if (index % 2 === 0) {
      return (
        <LeftImageText key={`post ${index}`} title={post.title} image={post.image}>
          <>
            <div dangerouslySetInnerHTML={{ __html: post.description }}/>
            <div className={Styles.btn}>
              <a className="button-link" href={post.btnLink} download>{post.btnText}</a>
            </div>
          </>
        </LeftImageText>
      );
    }
    return (
      <LeftTextImage key={`post ${index}`} title={post.title} image={post.image}>
        <div dangerouslySetInnerHTML={{ __html: post.description }}/>
        <div className={Styles.btn}>
          <a className="button-link" href={post.btnLink} download>{post.btnText}</a>
        </div>
      </LeftTextImage>
    );
  });

  return (
    <div className={Styles.wrapper}>
      <BannerBlock
        title={bannerTitle}
        background={bannerImg}
        description={bannerDescription}
        alt={bannerAlt}
        link={bannerUrl}
      />
      <div className="white-gradient">
        <div className={Styles.contentWrapper}>
          <div className="container">
            <div className={Styles.heading}>
              <h1>{pageTitle}</h1>
            </div>
            {postsList}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Aml;
