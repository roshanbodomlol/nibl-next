import { Row, Col } from 'antd';

import { withTranslation } from '../../../locales/i18n';
import BannerBlock from '../../BannerBlock';
import TableBody from '../../TableBody';
import Transformer from './transformer';
import Styles from './managerDetails.module.scss';
import ContentHeader from '../../ContentHeader';

const ManagerDetails = ({ t, ...props }) => {
  const data = new Transformer(props);

  const {
    bannerAlt,
    bannerCaption,
    bannerImage,
    bannerTitle,
    items,
    bannerUrl
  } = data;

  const tableContent = items.map((detail, i) => (
    <TableBody key={`team-member-table-${i}`}>
      <div className={Styles.tblGrid}>
        <Row>
          <Col span={2}>
            <span className={Styles.sn}>{i + 1}</span>
          </Col>
          <Col span={6}>
            <span className={Styles.managers}>{detail.memberBranch}</span>
          </Col>
          <Col span={4}>
            <span className={Styles.managers}>{detail.memberName}</span>
          </Col>
          <Col span={4}>
            <span className={Styles.provinceOffice}>{detail.memberEmail}</span>
          </Col>
          <Col span={4}>
            <span className={Styles.provinceOffice}>{detail.memberPhone}</span>
          </Col>
          <Col span={4}>
            <span className={Styles.provinceOffice}>{detail.memberFax}</span>
          </Col>
        </Row>
      </div>
    </TableBody>
  ));
  return (
    <>
      <BannerBlock background={bannerImage} alt={bannerAlt} title={bannerTitle} description={bannerCaption} link={bannerUrl}/>
      <div className="white-gradient main--content">
        <div className="container">
          {
            tableContent.length > 0
              ? (
                <div className={Styles.tableContainer}>
                  <div className={Styles.tableInner}>
                    <ContentHeader>
                      <div className={Styles.tblGrid}>
                        <Row>
                          <Col span={2}>
                            <span className={Styles.sn}>S.No</span>
                          </Col>
                          <Col span={6}>
                            <span className={Styles.managers}>{t('global_branch')}</span>
                          </Col>
                          <Col span={4}>
                            <span className={Styles.provinceOffice}>{t('global_branchManager')}</span>
                          </Col>
                          <Col span={4}>
                            <span className={Styles.provinceOffice}>{t('global_email')}</span>
                          </Col>
                          <Col span={4}>
                            <span className={Styles.provinceOffice}>{t('global_phone')}</span>
                          </Col>
                          <Col span={4}>
                            <span className={Styles.provinceOffice}>{t('locator_fax')}</span>
                          </Col>
                        </Row>
                      </div>
                    </ContentHeader>
                    {tableContent}
                  </div>
                </div>
              )
              : (
                <p>{t('global_noDataAvailable')}</p>
              )
          }
        </div>
      </div>
    </>
  );
};

export default withTranslation('common')(ManagerDetails);
