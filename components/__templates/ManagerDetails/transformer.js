export default class Transformer {
  constructor(APIObject) {
    this.bannerImage = APIObject.bannerDetails.bannerImage;
    this.bannerTitle = APIObject.bannerDetails.bannerTitle;
    this.bannerAlt = APIObject.bannerDetails.bannerAlt;
    this.bannerCaption = APIObject.bannerDetails.bannerCaption;
    this.bannerUrl = APIObject.bannerDetails.bannerUrl;
    this.items = APIObject.pageContent;
  }
}
