import Styles from './managementTeam.module.scss';

import { withTranslation } from '../../../locales/i18n';
import ContentHeader from '../../ContentHeader';

const ManagementTeamTbl = ({ content, t }) => (
  <div>
    <ContentHeader>
      <div className={Styles.tblGrid}>
        <span className={Styles.sn}>S.No</span>
        <span className={Styles.province}>{t('global_province')}</span>
        <span className={Styles.managers}>{t('global_managers')}</span>
        <span className={Styles.provinceOffice}>{t('template_informationOffice_provinceManager')}</span>
      </div>
    </ContentHeader>
    {content}
  </div>
);

export default withTranslation('common')(ManagementTeamTbl);
