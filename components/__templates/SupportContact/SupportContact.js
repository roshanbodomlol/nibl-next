import { useState } from 'react';
import { Row, Col, notification, message } from 'antd';
import Recaptcha from 'react-recaptcha';

import { withTranslation } from '../../../locales/i18n';
import Styles from './supportContact.module.scss';
import { API } from '../../../constants';
import { numeric, removeSpecialCharacters } from '../../../utilities/string.utils';
// import { post } from '../../../services/api.services';

const SupportContact = ({ t }) => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [callbackRequest, setCallbackRequest] = useState(false);
  const [emailAddress, setEmailAddress] = useState('');
  const [yourQuery, setYourQuery] = useState('');
  const [loading, setLoading] = useState(false);
  const [verified, setVerified] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();

    if (!verified) {
      notification.open({
        description: 'Please submit the captcha'
      });
      return false;
    }

    if (!firstName) {
      message.info('First name is required');
      return false;
    }

    setFirstName(removeSpecialCharacters(firstName));

    if (!lastName) {
      message.info('Last name is required');
      return false;
    }

    setLastName(removeSpecialCharacters(lastName));

    if (!phoneNumber) {
      message.info('Phone Number is required');
      return false;
    }

    setPhoneNumber(numeric(phoneNumber));

    if (!emailAddress) {
      message.info('Email Address is required');
      return false;
    }

    const data = {
      firstName: removeSpecialCharacters(firstName),
      lastName: removeSpecialCharacters(lastName),
      phoneNumber: numeric(phoneNumber),
      emailAddress,
      yourQuery
      // permanentAddress,
      // callbackRequest,
    };

    setLoading(true);

    fetch(`${process.env.API_URL}/${API.endPoints.SEND_MAIL}`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(response => response.json())
      .then((response) => {
        if (response.status) {
          notification.open({
            description: 'Submitted successfully'
          });
          setLoading(false);
        } else {
          console.error(response.error);
          notification.open({
            description: 'Error occurred, please try again'
          });
          setLoading(false);
        }
      })
      .catch((error) => {
        console.error(error);
        notification.open({
          description: 'Error occurred, please try again'
        });
        setLoading(false);
      });
  };

  return (
    <div className={Styles.wrapper}>
      <div className="container">
        <div className={Styles.flexBlock}>
          <div className={Styles.leftBlock}>
            <div className={Styles.generalTexts}>
              <span>{t('global_contactUsQuery')}</span>
            </div>
          </div>
          <div className={Styles.rightBlock}>
            <div className={Styles.wrapper}>
              <form className={Styles.form} onSubmit={handleSubmit}>
                <h2>{t('template_contact_title')}</h2>
                <Row gutter={24}>
                  <Col lg={12} md={24} xs={24}>
                    <div className={Styles.element}>
                      <span>{t('template_contact_firstName')}*</span>
                      <input required type="text" value={firstName} onChange={e => setFirstName(e.target.value)}/>
                    </div>
                  </Col>
                  <Col lg={12} md={24} xs={24}>
                    <div className={Styles.element}>
                      <span>{t('template_contact_lastName')}*</span>
                      <input required type="text" value={lastName} onChange={e => setLastName(e.target.value)}/>
                    </div>
                  </Col>
                </Row>

                <Row gutter={24}>
                  {/* <Col span={12}>
                    <div className={Styles.element}>
                      <span>{t('template_contact_address')}*</span>
                      <input required type="text" value={permanentAddress} onChange={e => setPermanentAddress(e.target.value)}/>
                    </div>
                  </Col> */}
                  {/* <Col span={12}>
                    <div className={Styles.element}>
                      <span>{t('template_contact_temporaryAddress')}</span>
                      <input type="text"/>
                    </div>
                  </Col> */}
                </Row>

                <Row gutter={24}>
                  <Col span={12}>
                    <div className={Styles.element}>
                      <span>{t('template_contact_phoneNumber')}*</span>
                      <input required type="tel" value={phoneNumber} onChange={e => setPhoneNumber(e.target.value)}/>
                    </div>
                  </Col>
                  <Col span={12}>
                    <div className={Styles.elementCheckbox}>
                      <input type="checkbox" checked={callbackRequest} onChange={(e) => setCallbackRequest(e.target.checked)}/>
                      {t('template_contact_callback')}
                    </div>
                  </Col>
                </Row>

                <Row gutter={24}>
                  <Col lg={12} md={24} xs={24}>
                    <div className={Styles.element}>
                      <span>{t('global_email')}*</span>
                      <input required type="email" value={emailAddress} onChange={e => setEmailAddress(e.target.value)}/>
                    </div>
                  </Col>
                  {/* <Col span={12}>
                    <div className={Styles.elementDepartment}>
                      <span>{t('global_department')}</span>
                      <select>
                        <option>HR</option>
                        <option>{t('global_loan')}</option>
                        <option>CSD</option>
                        <option>{t('global_cash')}</option>
                      </select>
                    </div>
                  </Col> */}
                </Row>

                <Row gutter={24}>
                  <Col span={24}>
                    <div className={Styles.elementQuery}>
                      <span>{t('template_contact_query')}</span>
                      <textarea rows="5" onChange={e => setYourQuery(e.target.value)}>
                        {yourQuery}
                      </textarea>
                    </div>
                  </Col>
                </Row>

                <Recaptcha
                  sitekey="6LdpNc8ZAAAAAKoBxHtA3KLhoQpMfoEiejUQ1-Z1"
                  render="explicit"
                  onloadCallback={() => setLoading(false)}
                  verifyCallback={() => setVerified(true)}
                  expiredCallback={() => setVerified(false)}
                />
                <div><small>* Required fields</small></div>

                <button type="submit" disabled={loading} className={Styles.button}>{t('global_send')}</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withTranslation('common')(SupportContact);
