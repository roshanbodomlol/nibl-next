import {
  chain, zipObject
} from 'lodash';

export default class Transformer {
  constructor(APIObject) {
    [this.weekdayInValleyItems] = chain(APIObject.pageContent.weekdayInValley)
      .groupBy('categoryName')
      .toPairs()
      .map(data => zipObject(['name', 'details'], data))
      .value()
      .map(item => ({ ...item, key: item.name }));
    [this.weekdayOutValleyItems] = chain(APIObject.pageContent.weekdayOutValley)
      .groupBy('categoryName')
      .toPairs()
      .map(data => zipObject(['name', 'details'], data))
      .value()
      .map(item => ({ ...item, key: item.name }));
    [this.holidayCounterItems] = chain(APIObject.pageContent.holidayCounter)
      .groupBy('categoryName')
      .toPairs()
      .map(data => zipObject(['name', 'details'], data))
      .value()
      .map(item => ({ ...item, key: item.name }));
    [this.eveningInValleyItems] = chain(APIObject.pageContent.eveningInValley)
      .groupBy('categoryName')
      .toPairs()
      .map(data => zipObject(['name', 'details'], data))
      .value()
      .map(item => ({ ...item, key: item.name }));
    [this.eveningOutValleyItems] = chain(APIObject.pageContent.eveningOutValley)
      .groupBy('categoryName')
      .toPairs()
      .map(data => zipObject(['name', 'details'], data))
      .value()
      .map(item => ({ ...item, key: item.name }));
    this.activeItemKey = this.weekdayInValleyItems.key;
    this.bannerImage = APIObject.bannerDetails.bannerImage;
    this.bannerCaption = APIObject.bannerDetails.bannerCaption;
    this.bannerAlt = APIObject.bannerAlt;
    this.bannerTitle = APIObject.bannerTitle;
    this.bannerUrl = APIObject.bannerUrl;
  }
}
