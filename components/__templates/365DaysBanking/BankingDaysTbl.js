import { Row, Col } from 'antd';

import { withTranslation } from '../../../locales/i18n';
import Styles from './bankingDays.module.scss';
import ContentHeader from '../../ContentHeader';
import TableBody from '../../TableBody';

const BankingDaysTbl = ({ t, type, item }) => {
  switch (type) {
    case 1: {
      return (
        <>
          <ContentHeader>
            <div className={Styles.gridBlock}>
              <Row>
                <Col span={3}>
                  <span>S.No</span>
                </Col>
                <Col span={9}>
                  <span>Branch Name</span>
                </Col>
                <Col span={6}>
                  <span className={Styles.day}>Weekday Counter</span>
                </Col>
                <Col span={6}>
                  <span className={Styles.total}>Friday Counter</span>
                </Col>
              </Row>
            </div>
          </ContentHeader>
          {
            item.details.map((e, i) => (
              <TableBody>
                <div className={Styles.gridBlock}>
                  <Row>
                    <Col span={3}>
                      <span>{i + 1}</span>
                    </Col>
                    <Col span={9}>{e.branchName}</Col>
                    <Col span={6}>
                      <span className={Styles.day}>{e.weekdayCounter}</span>
                    </Col>
                    <Col span={6}>
                      <span className={Styles.total}>{e.fridayCounter}</span>
                    </Col>
                  </Row>
                </div>
              </TableBody>
            ))
          }
        </>
      );
    }

    case 2: {
      return (
        <>
          <ContentHeader>
            <div className={Styles.gridBlock}>
              <Row>
                <Col span={3}>
                  <span>S.No</span>
                </Col>
                <Col span={13}>
                  <span>Branch Name</span>
                </Col>
                <Col span={8}>
                  <span className={Styles.day}>Weekday Evening Counter</span>
                </Col>
              </Row>
            </div>
          </ContentHeader>
          {
            item.details.map((e, i) => (
              <TableBody>
                <div className={Styles.gridBlock}>
                  <Row>
                    <Col span={3}>
                      <span>{i + 1}</span>
                    </Col>
                    <Col span={13}>{e.branchName}</Col>
                    <Col span={8}>
                      <span className={Styles.day}>{e.holidayCounter}</span>
                    </Col>
                  </Row>
                </div>
              </TableBody>
            ))
          }
        </>
      );
    }

    case 3: {
      return (
        <>
          <ContentHeader>
            <div className={Styles.gridBlock}>
              <Row>
                <Col span={3}>
                  <span>S.No</span>
                </Col>
                <Col span={9}>
                  <span>Branch Name</span>
                </Col>
                <Col span={6}>
                  <span className={Styles.day}>Weekday Evening Counter</span>
                </Col>
                <Col span={6}>
                  <span className={Styles.total}>Friday Evening Counter</span>
                </Col>
              </Row>
            </div>
          </ContentHeader>
          {
            item.details.map((e, i) => (
              <TableBody>
                <div className={Styles.gridBlock}>
                  <Row>
                    <Col span={3}>
                      <span>{i + 1}</span>
                    </Col>
                    <Col span={9}>{e.branchName}</Col>
                    <Col span={6}>
                      <span className={Styles.day}>{e.weekdayEveningCounter}</span>
                    </Col>
                    <Col span={6}>
                      <span className={Styles.total}>{e.fridayEveningCounter}</span>
                    </Col>
                  </Row>
                </div>
              </TableBody>
            ))
          }
        </>
      );
    }

    default: {
      return null;
    }
  }
};

export default withTranslation('common')(BankingDaysTbl);
