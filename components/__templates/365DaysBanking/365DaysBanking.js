import { Component } from 'react';

import { withTranslation } from '../../../locales/i18n';
import BannerBlock from '../../BannerBlock';
import TabNav from '../../TabNav';
import TabContent from '../../TabContent';
import BankingDaysTbl from './BankingDaysTbl';
import Styles from './bankingDays.module.scss';
import Transformer from './transformer';

class BankingDays extends Component {
  state = {
    activeItemKey: ''
  }

  componentDidMount() {
    const data = new Transformer(this.props);
    const { activeItemKey } = data;
    this.setState({ activeItemKey });
  }

  render() {
    const { activeItemKey } = this.state;
    const data = new Transformer(this.props);
    const {
      weekdayInValleyItems, weekdayOutValleyItems, holidayCounterItems,
      eveningInValleyItems, eveningOutValleyItems, bannerAlt, bannerCaption,
      bannerImage, bannerTitle, bannerUrl
    } = data;

    return (
      <>
        <BannerBlock
          background={bannerImage}
          alt={bannerAlt}
          description={bannerCaption}
          title={bannerTitle}
          link={bannerUrl}
        />
        <div className="white-gradeint main--content">
          <div className="container">
            <div className={Styles.tabnavWrapper}>
              <TabNav
                arrows
                stretch
                onClick={(key) => { this.setState({ activeItemKey: key }); }}
                activeItemKey={activeItemKey}
                items={[weekdayInValleyItems, weekdayOutValleyItems, holidayCounterItems,
                  eveningInValleyItems, eveningOutValleyItems]}
              />
            </div>
            <TabContent>
              <BankingDaysTbl type={1} item={weekdayInValleyItems} isActive={activeItemKey === weekdayInValleyItems.key}/>
              <BankingDaysTbl type={1} item={weekdayOutValleyItems} isActive={activeItemKey === weekdayOutValleyItems.key}/>
              <BankingDaysTbl type={2} item={holidayCounterItems} isActive={activeItemKey === holidayCounterItems.key}/>
              <BankingDaysTbl type={3} item={eveningInValleyItems} isActive={activeItemKey === eveningInValleyItems.key}/>
              <BankingDaysTbl type={3} item={eveningOutValleyItems} isActive={activeItemKey === eveningOutValleyItems.key}/>
            </TabContent>
          </div>
        </div>
      </>
    );
  }
}

export default withTranslation('common')(BankingDays);
