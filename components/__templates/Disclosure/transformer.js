import { chain, zipObject } from 'lodash';

export default class Transformer {
  constructor(APIObject) {
    this.bannerTitle = APIObject.bannerDetails.bannerTitle;
    this.bannerDescription = APIObject.bannerDetails.bannerCaption;
    this.bannerImg = APIObject.bannerDetails.bannerImage;
    this.bannerAlt = APIObject.bannerDetails.bannerAlt;
    this.bannerUrl = APIObject.bannerDetails.bannerUrl;
    this.pageTitle = APIObject.pageDetails.pageTitle;
    this.items = chain(APIObject.pageContent)
      .groupBy('disclosureYear')
      .toPairs()
      .map(currentData => zipObject(['year', 'details'], currentData))
      .value()
      .map(item => ({
        ...item,
        details: item.details.map(e => ({
          primary: e.disclosureTitle,
          secondary: e.disclosureDescription,
          downloadLink: e.uploadedFile
        }))
      }));
    this.activeItemKey = this.items.length > 0 && this.items[0].year;
  }
}
