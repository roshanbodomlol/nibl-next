export default class Transformer {
  constructor(APIObject) {
    this.bannerImg = APIObject.bannerDetails.bannerImage;
    this.bannerTitle = APIObject.bannerDetails.bannerTitle;
    this.bannerDescription = APIObject.bannerDetails.bannerCaption;
    this.bannerUrl = APIObject.bannerDetails.bannerUrl;
    this.productImage = APIObject.pageContent[0].overviewImage;
    this.contentDescription = APIObject.pageContent[0].overviewDescription;
    this.features = APIObject.pageContent[0].featureDescription;
    this.featuresTitle = APIObject.pageContent[0].featureTitle;
    this.downloadItems = APIObject.pageDownloads.map(e => ({
      primary: e.fileName,
      secondary: e.fileDescription,
      downloadLink: e.fileUploaded
    }));
  }
}
