import { withTranslation } from '../../../locales/i18n';
import BannerBlock from '../../BannerBlock';
import LeftImageText from '../../LeftImageText';
import ContentCard from '../../ContentCard';
import ContentHeader from '../../ContentHeader';
import ExpansionPanel from '../../ExpansionPanel';
import Transformer from './transformer';
import Styles from './mobileBanking.module.scss';

const MobileBanking = ({ t, ...props }) => {
  const data = new Transformer(props);
  const {
    bannerImg,
    bannerTitle,
    bannerDescription,
    contentDescription,
    downloadItems,
    productImage,
    features,
    featuresTitle,
    bannerUrl
  } = data;

  const downloadList = downloadItems.map(downloadItem => (
    {
      primary: (
        <div className={Styles.expansionPanel}>
          <div>
            <p>{downloadItem.primary}</p>
          </div>
          <a href={downloadItem.downloadLink}><img src="/img/download.png" alt=""/></a>
        </div>
      ),
      secondary: (
        <div>
          {downloadItem.secondary}
        </div>
      )
    }
  ));

  return (
    <div className={Styles.wrapper}>
      <BannerBlock
        background={bannerImg}
        title={bannerTitle}
        description={bannerDescription}
        link={bannerUrl}
      />
      <div className="white-gradient" style={{ paddingBottom: 1 }}>
        <div className={Styles.greyBack}>
          <div className="container">
            <div className={Styles.heading}>
              <h1>{t('template_mobileBanking_title')}</h1>
            </div>
            <div className={Styles.contentWrapper}>
              <LeftImageText title={featuresTitle} image={productImage}>
                <div dangerouslySetInnerHTML={{ __html: contentDescription }}/>
              </LeftImageText>
            </div>
            {
              features && (
                <>
                  <div className={Styles.heading}>
                    <h1>{t('global_features')}</h1>
                  </div>
                  <ContentCard>
                    <div className={Styles.features}>
                      <div className="wysiwyg" dangerouslySetInnerHTML={{ __html: features }}/>
                    </div>
                  </ContentCard>
                </>
              )
            }
            {
              downloadItems.length > 0 && (
                <>
                  <div className={Styles.contentHeaderWrapper}>
                    <ContentHeader grey>
                      <span>S.No</span>
                      <span>{t('global_details')}</span>
                    </ContentHeader>
                  </div>
                  <div className={Styles.expansionPanelWrapper}>
                    <ExpansionPanel
                      isOrdered
                      items={downloadList}
                    />
                  </div>
                </>
              )
            }
          </div>
        </div>
      </div>
    </div>
  );
};

export default withTranslation('common')(MobileBanking);
