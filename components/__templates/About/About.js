import BannerBlock from '../../BannerBlock';
import AboutContent from './AboutContent';
import ValuesAndEthics from './ValuesAndEthics';
import StrategicObjectives from './StrategicObjectives';
import Transformer from './transformer';

import Styles from './about.module.scss';

const About = (props) => {
  const data = new Transformer(props);
  const {
    bannerImg,
    bannerTitle,
    bannerCaption,
    overview,
    mission,
    leftImage,
    rightImage,
    valuesAndEthicsTitle,
    valuesAndEthics,
    strategicObjectives,
    bannerAlt,
    missionTitle,
    overviewTitle,
    valueEthicTitle,
    objectiveTitle,
    bannerUrl
  } = data;

  return (
    <div className={Styles.wrapper}>
      <BannerBlock
        background={bannerImg}
        title={bannerTitle}
        description={bannerCaption}
        alt={bannerAlt}
        link={bannerUrl}
      />

      <div className="white-gradient">
        <div className={Styles.greyBack}>
          <div className="container">
            <AboutContent
              overview={overview}
              mission={mission}
              leftImage={leftImage}
              rightImage={rightImage}
              missionTitle={missionTitle}
              overviewTitle={overviewTitle}
            />
          </div>
        </div>

        <div className={Styles.valuesEthicWrapper}>
          <div className="container">
            <ValuesAndEthics
              title={valueEthicTitle}
              excerpt={valuesAndEthicsTitle}
              valuesAndEthics={valuesAndEthics}
            />
          </div>
        </div>
      </div>

      <StrategicObjectives title={objectiveTitle}>
        <div className="wysiwyg" dangerouslySetInnerHTML={{ __html: strategicObjectives }}/>
      </StrategicObjectives>
    </div>
  );
};

export default About;
