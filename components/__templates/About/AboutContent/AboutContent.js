import React from 'react';
import PropTypes from 'prop-types';

import { withTranslation } from '../../../../locales/i18n';
import LeftImageText from '../../../LeftImageText';
import LeftTextImage from '../../../LeftTextImage';
import Styles from './aboutcontent.module.scss';

const AboutContent = ({
  overview, mission, leftImage, rightImage, t, missionTitle, overviewTitle
}) => (
  <div className={Styles.wrapper}>
    <h1>{t('template_about_title')}</h1>
    <LeftImageText title={overviewTitle} image={leftImage}>
      <div dangerouslySetInnerHTML={{ __html: overview }}/>
    </LeftImageText>

    <LeftTextImage title={missionTitle} image={rightImage}>
      <div dangerouslySetInnerHTML={{ __html: mission }}/>
    </LeftTextImage>
  </div>
);

AboutContent.propTypes = {
  overview: PropTypes.string.isRequired,
  mission: PropTypes.string.isRequired,
  leftImage: PropTypes.string.isRequired,
  rightImage: PropTypes.string.isRequired
};

export default withTranslation('common')(AboutContent);
