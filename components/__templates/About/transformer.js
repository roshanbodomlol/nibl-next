export default class Transformer {
  constructor(APIObject) {
    this.overview = APIObject.pageContent[0].overviewDescription;
    this.mission = APIObject.pageContent[0].missionDescription;
    this.leftImage = APIObject.pageContent[0].overviewImage;
    this.rightImage = APIObject.pageContent[0].missionImage;
    this.bannerImg = APIObject.bannerDetails.bannerImage;
    this.bannerTitle = APIObject.bannerDetails.bannerTitle;
    this.bannerCaption = APIObject.bannerDetails.bannerCaption;
    this.bannerAlt = APIObject.bannerDetails.bannerAlt;
    this.bannerUrl = APIObject.bannerDetails.bannerUrl;
    this.valuesAndEthicsTitle = APIObject.pageContent[0].valueEthicDescription;
    this.valuesAndEthics = APIObject.pageValueEthics.map(e => ({
      icon: e.icon,
      title: e.title,
      content: e.description
    }));
    this.strategicObjectives = APIObject.pageContent[0].objectiveDescription;
    this.missionTitle = APIObject.pageContent[0].missionTitle;
    this.overviewTitle = APIObject.pageContent[0].overviewTitle;
    this.valueEthicTitle = APIObject.pageContent[0].valueEthicTitle;
    this.objectiveTitle = APIObject.pageContent[0].objectiveTitle;
  }
}
