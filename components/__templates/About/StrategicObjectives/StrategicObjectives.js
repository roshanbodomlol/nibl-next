import React from 'react';

import { withTranslation } from '../../../../locales/i18n';
import Styles from './strategicobjectives.module.scss';

const StrategicObjectives = ({ children, t, title }) => (
  <div className={Styles.wrapper}>
    <div className="container">
      <div className={Styles.outer}>
        <div className={Styles.contentWrapper}>
          <div className={Styles.heading}>
            <h1>{title}</h1>
          </div>
          <div className={Styles.contentList}>
            {children}
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default withTranslation('common')(StrategicObjectives);
