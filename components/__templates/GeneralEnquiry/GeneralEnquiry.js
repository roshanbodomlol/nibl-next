import ContentCard from '../../ContentCard';
import Transformer from './transformer';
import Styles from './generalEnquiry.module.scss';
import { Row, Col } from 'antd';

const GeneralEnquiry = (props) => {
  const data = new Transformer(props);
  const {
    title,
    contentLeft,
    contentRight
  } = data;
  return (
    <div className="white-gradient main--content">
      <div className="container">
        <div className="heading">
          <h1>{title}</h1>
        </div>

        <ContentCard>
          <div className={Styles.gridBlock}>
            <Row gutter={24}>
              <Col lg={12} sm={24}>
                <div className={Styles.leftBlock}>
                  <div className="wysiwyg" dangerouslySetInnerHTML={{ __html: contentLeft }}/>
                </div>
              </Col>
              <Col lg={12} sm={24}>
                <div className={Styles.rightBlock}>
                  <div className={Styles.innerRight}>
                    <div className="wysiwyg" dangerouslySetInnerHTML={{ __html: contentRight }}/>
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </ContentCard>
      </div>
    </div>
  );
};

export default GeneralEnquiry;
