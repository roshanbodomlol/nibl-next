export default class Transformer {
  constructor(APIObject) {
    this.title = APIObject.pageDetails.pageTitle;
    this.contentLeft = APIObject.pageContent[0].contentDescriptionA;
    this.contentRight = APIObject.pageContent[0].contentDescriptionB;
  }
}
