import { withTranslation } from '../../../locales/i18n';
import Locator from '../../Locator';
import BranchTransformer from './branch.transformer';
import Styles from './branchLocation.module.scss';

const BranchLocation = ({ t, ...props }) => {
  const data = new BranchTransformer(props);
  const { items } = data;

  return (
    <div className={Styles.wrapper}>
      {
        items.length > 0
          ? <Locator title="Click here to see the nearest Branch location" locations={items}/>
          : (
            <div className="page-loader">
              <span>{t('global_noDataAvailable')}</span>
            </div>
          )
      }
    </div>
  );
};

export default withTranslation('common')(BranchLocation);
