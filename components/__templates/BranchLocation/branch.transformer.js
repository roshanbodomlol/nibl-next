export default class BranchTransformer {
  constructor(ATMApiObject) {
    this.items = ATMApiObject.pageContent.map(e => (
      {
        id: e.branchId,
        name: e.locationName,
        address: e.address,
        lat: parseFloat(e.latitude),
        lng: parseFloat(e.longitude),
        phoneNumber: e.phoneNumber,
        faxNumber: e.faxNumber,
        isInsideValley: e.isInsideValley,
        isValid: !!e.latitude && !!e.longitude,
        holidayTime: e.holidayTime,
        eveningCounter: e.eveningCounter,
        normalWorkingWeekdays: e.normalWorkingWeekdays,
        normalWorkingFridays: e.normalWorkingFridays,
        branchManager: e.branchManager,
        email: e.email,
        eveningCounterFridays: e.eveningCounterFridays
      }
    ));
  }
}
