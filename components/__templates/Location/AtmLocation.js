import { withTranslation } from '../../../locales/i18n';
import Locator from '../../Locator';
import ATMTransformer from './atm.transformer';
import Styles from './atmLocation.module.scss';

const AtmLocation = ({ t, ...props }) => {
  const data = new ATMTransformer(props);
  const { items } = data;

  return (
    <div className={Styles.wrapper}>
      {
        items.length > 0
          ? <Locator title="Click here to see the nearest ATM location" locations={items}/>
          : (
            <div className="page-loader">
              <span>{t('global_noDataAvailable')}</span>
            </div>
          )
      }
    </div>
  );
};

export default withTranslation('common')(AtmLocation);
