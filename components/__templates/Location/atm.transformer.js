export default class ATMTransformer {
  constructor(ATMApiObject) {
    this.items = ATMApiObject.pageContent.map(e => (
      {
        id: e.atmId,
        name: e.locationName,
        address: e.address,
        lat: parseFloat(e.latitude),
        lng: parseFloat(e.longitude),
        phoneNumber: e.phoneNumber,
        faxNumber: e.faxNumber,
        isInsideValley: e.isInsideValley,
        isValid: !!e.latitude && !!e.longitude
      }
    ));
  }
}
