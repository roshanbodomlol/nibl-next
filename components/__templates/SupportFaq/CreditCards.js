import React from 'react';
import Styles from './supportFaq.module.scss';

// Components
import SearchBar from '../../SearchBar';
import ExpansionPanel from '../../ExpansionPanel';

const CreditCards = () => (
  <div className={Styles.sectionWrapper}>
    <SearchBar/>
    <div className="heading">
      <h1>Credit Card FAQ</h1>
    </div>
    <ExpansionPanel
      isOrdered
      items={[
        {
          primary:
          (
            <div className={Styles.primaryPanel}>
              <div>
                <span>
                  Which browsers support NIBL eBanking?
                </span>
              </div>
            </div>
          ),
          secondary:
          (
            <div>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </div>
          )
        },
        {
          primary:
          (
            <div className={Styles.primaryPanel}>
              <div>
                <span>
                  Download NIBL Mobile (SMS) Banking Application
                </span>
              </div>
            </div>
          ),
          secondary:
          (
            <div>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </div>
          )
        },
        {
          primary:
          (
            <div className={Styles.primaryPanel}>
              <div>
                <span>
                  Download NIBL Mobile (SMS) Banking Application
                </span>
              </div>
            </div>
          ),
          secondary:
          (
            <div>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </div>
          )
        },
        {
          primary:
          (
            <div className={Styles.primaryPanel}>
              <div>
                <span>
                  Download NIBL Mobile (SMS) Banking Application
                </span>
              </div>
            </div>
          ),
          secondary:
          (
            <div>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </div>
          )
        },
        {
          primary:
          (
            <div className={Styles.primaryPanel}>
              <div>
                <span>
                  Download NIBL Mobile (SMS) Banking Application
                </span>
              </div>
            </div>
          ),
          secondary:
          (
            <div>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </div>
          )
        },
        {
          primary:
          (
            <div className={Styles.primaryPanel}>
              <div>
                <span>
                  Download NIBL Mobile (SMS) Banking Application
                </span>
              </div>
            </div>
          ),
          secondary:
          (
            <div>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </div>
          )
        }
      ]}
    />
  </div>
);

export default CreditCards;
