import React from 'react';

// import SearchBar from '../../SearchBar';
import ExpansionPanel from '../../ExpansionPanel';
import Styles from './supportFaq.module.scss';

const Tab = ({ items }) => {
  const itemsList = items.map(e => ({
    primary:
    (
      <div className={Styles.primaryPanel}>
        <div>
          <span>{e.faqQuestion}</span>
        </div>
      </div>
    ),
    secondary:
    (
      <div dangerouslySetInnerHTML={{ __html: e.faqAnswer }}/>
    )
  }));
  return (
    <div className={Styles.sectionWrapper}>
      <ExpansionPanel
        isOrdered
        items={itemsList}
      />
    </div>
  );
};

export default Tab;
