import { Component } from 'react';

import TabNav from '../../TabNav';
import TabContent from '../../TabContent';
import Tab from './Tab';
import BannerBlock from '../../BannerBlock';
import Transformer from './transformer';
import Styles from './supportFaq.module.scss';

class SupportFaq extends Component {
  state = {
    activeItemKey: ''
  }

  componentDidMount() {
    const data = new Transformer(this.props);
    const { activeItemKey } = data;
    this.setState({ activeItemKey });
  }

  render() {
    const data = new Transformer(this.props);
    const { items, bannerAlt, bannerCaption, bannerImage, bannerTitle, loading, pageTitle, bannerUrl } = data;
    const { activeItemKey } = this.state;

    const tabs = items.map((e, i) => (
      <Tab items={e.details} key={`faq-tab-${i}`} isActive={activeItemKey === e.key}/>
    ));
    return (
      <div>
        <BannerBlock
          background={bannerImage}
          alt={bannerAlt}
          description={bannerCaption}
          title={bannerTitle}
          link={bannerUrl}
        />
        <div className="white-gradient main--content">
          <div className="container">
            <div className={Styles.tabNavWrapper}>
              <TabNav
                onClick={(key) => { this.setState({ activeItemKey: key }); }}
                activeItemKey={activeItemKey}
                items={items}
              />
            </div>
            <div className="heading">
              <h1>{pageTitle}</h1>
            </div>
            <TabContent>
              {tabs}
            </TabContent>
          </div>
        </div>
      </div>
    );
  }
}

export default SupportFaq;
