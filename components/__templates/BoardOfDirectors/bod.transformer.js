export default class BODTransformer {
  constructor(BODAPIOBJECT) {
    this.bannerTitle = BODAPIOBJECT.bannerDetails.bannerTitle;
    this.bannerCaption = BODAPIOBJECT.bannerDetails.bannerCaption;
    this.bannerAlt = BODAPIOBJECT.bannerDetails.bannerAlt;
    this.bannerImage = BODAPIOBJECT.bannerDetails.bannerImage;
    this.bannerUrl = BODAPIOBJECT.bannerDetails.bannerUrl;
    this.pageTitle = BODAPIOBJECT.pageDetails.pageTitle;
    this.bods = BODAPIOBJECT.pageContent.map(e => ({
      image: e.memberImage,
      name: e.memberName,
      designation: e.memberDesignation,
      description: e.memberDescription,
      facebook: e.memberFacebookLnk,
      twitter: e.memberTwitterLnk,
      linkedIn: e.memberLinkedinLnk,
      id: e.contentId
    }));
  }
}
