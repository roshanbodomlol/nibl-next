import { useState, useEffect } from 'react';
import Link from 'next/link';

import { withTranslation } from '../../../locales/i18n';
import ContentCard from '../../ContentCard';
import { get } from '../../../services/api.services';
import { API } from '../../../constants';
import Loading from '../../Loading';

const QuickLinks = ({ t }) => {
  const [links, setLinks] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    get(API.endPoints.QUICK_LINKS)
      .then(({ pageContent }) => {
        setLinks(pageContent);
        setLoading(false);
      })
      .catch(e => console.error(e));
  }, []);

  const linkList = links.map((link) => {
    if (link.quicklinkSlug.startsWith('http')) {
      return (
        <li key={`quick-link-${link.quicklinkId}`}>
          <a href={link.quicklinkSlug} target="_blank" rel="noopener noreferrer">{link.pageTitle}</a>
        </li>
      );
    }
    if (link.quicklinkSlug.split('/').length === 3) {
      return (
        <li key={`quick-link-${link.quicklinkId}`}>
          <Link href="[page]/[subpage]/[child]" as={`/${link.quicklinkSlug}`}>
            <a>{link.pageTitle}</a>
          </Link>
        </li>
      );
    }

    return (
      <li key={`quick-link-${link.quicklinkId}`}>
        <a href={`/${link.quicklinkSlug}`}>{link.pageTitle}</a>
      </li>
    );
  });

  return (
    <div>
      <div className="white-gradient main--content">
        <div className="container">
          <div className="heading">
            <h1>{t('template_quickLinks_title')}</h1>
          </div>
          <div className="quick-links">
            <ContentCard>
              {
                loading
                  ? <Loading/>
                  : (
                    <ul>
                      {linkList}
                    </ul>
                  )
                }
            </ContentCard>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withTranslation('common')(QuickLinks);
