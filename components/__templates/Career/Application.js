import { useState } from 'react';
import ReCAPTCHA from 'react-google-recaptcha';
import { notification } from 'antd';

import { post } from '../../../services/api.services';
import { API } from '../../../constants';
import { withTranslation } from '../../../locales/i18n';
import Form from '../../Form';
import ButtonRedBorder from '../../ButtonRedBorder';
import Styles from './career.module.scss';

const captchaSiteKey = '6LdpNc8ZAAAAAKoBxHtA3KLhoQpMfoEiejUQ1-Z1';
const captchaSecretKey = '6LdpNc8ZAAAAAGzLUAtNuwH_9CMRi_iYB9BSozsA';

const Application = ({ pageContent, t }) => {
  const [name, setName] = useState('');
  const [position, setPosition] = useState('');
  const [cv, setCV] = useState(null);
  const [captchaResponse, setCaptchaResponse] = useState(null);
  const [loading, setLoading] = useState(false);

  const handleSubmit = () => {
    setLoading(true);
    if (!name || !position || !cv || !captchaResponse) {
      notification.open({
        description: 'Please fill all fields.'
      });
      setLoading(false);
    }

    // continue
    post(API.endPoints.CAREER_FORM, {
      name,
      position,
      cv,
      captchaResponse
    })
      .then(() => {
        notification.open({
          description: 'Submitted successfully'
        });
        setLoading(false);
      })
      .catch(e => console.log(e));
  };

  return (
    <div>
      <Form
        encType="multipart/form-data"
        background="/img/big-banner.jpg"
        title={pageContent[0].positionTitle}
        description={pageContent[0].positionDescription}
        secondaryTitle=""
        formTitle="Vacancy Application"
        onSubmit={handleSubmit}
      >
        <div className={Styles.formElement}>
          <span>{t('template_career_applicantName')}:</span>
          <input className={Styles.name} type="text" onChange={e => setName(e.target.value)}/>
        </div>

        <div className={Styles.formElement}>
          <span>{t('template_career_appliedPosition')}:</span>
          <select onChange={e => setPosition(e.target.value)}>
            <option>Manager</option>
            <option>Assistant Manager</option>
          </select>
        </div>

        <div className={Styles.formElement}>
          <span>Curriculum Vitae:</span>
          <input className={Styles.file} type="file" onChange={e => setCV(e.target.files[0])}/>
        </div>

        <div className={Styles.formElement}>
          <span/>
          <ReCAPTCHA
            sitekey={captchaSiteKey}
            onChange={res => setCaptchaResponse(res)}
            onloadCallback={() => {}}
          />
        </div>

        <div className={Styles.formElement}>
          <span/>
          <div>
            <div className={Styles.button}>
              <ButtonRedBorder
                title={t('global_send')}
                disabled={loading}
              />
            </div>
          </div>
        </div>
      </Form>
    </div>
  );
};

export default withTranslation('common')(Application);
