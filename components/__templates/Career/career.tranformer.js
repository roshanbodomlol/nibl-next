export default class CAREERTransformer {
  constructor(CAREEROBJECT) {
    this.title = CAREEROBJECT.pageDetails.pageTitle;
    this.bannerTitle = CAREEROBJECT.bannerDetails.bannerTitle;
    this.bannerCaption = CAREEROBJECT.bannerDetails.bannerCaption;
    this.bannerAlt = CAREEROBJECT.bannerDetails.bannerAlt;
    this.bannerImage = CAREEROBJECT.bannerDetails.bannerImage;
    this.bannerUrl = CAREEROBJECT.bannerDetails.bannerUrl;
    this.pageDescription = CAREEROBJECT.pageContent[0].description;
    this.vacancies = CAREEROBJECT.pageVacancy;
  }
}
