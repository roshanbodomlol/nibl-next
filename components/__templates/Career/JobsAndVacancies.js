import Link from 'next/link';
import moment from 'moment';
import { Row, Col } from 'antd';

import { withTranslation } from '../../../locales/i18n';
import ContentCard from '../../ContentCard';
import SearchBar from '../../SearchBar';
import ContentHeader from '../../ContentHeader';
import ExpansionPanel from '../../ExpansionPanel';
import Styles from './career.module.scss';

const JobsAndVacancies = ({ title, vacancies, description, t }) => {
  const vacancyList = vacancies.map((vacancy, index) => (
    {
      primary:
      (
        <div className={Styles.expansionGrid}>
          <Row>
            <Col span={2}>
              <div className={Styles.number}>
                {index + 1}
              </div>
            </Col>
            <Col span={8}>
              <span className={Styles.expansionPosition}>{vacancy.positionTitle}</span>
            </Col>
            <Col span={8}>
              <span className={Styles.expansionDeadline}>{moment(vacancy.positionDeadLine).format('Do MMMM, YYYY')}</span>
            </Col>
          </Row>
        </div>
      ),
      secondary:
      (
        <div className={Styles.expanded}>
          <div dangerouslySetInnerHTML={{ __html: vacancy.positionDescription }}/>
        </div>
      )
    }
  ));

  // <Col span={6}>
  //   <div className={Styles.apply}>
  //     <Link href="/career/application/[id]" as={`/career/application/${vacancy.vacancyId}`}>
  //       <a>Apply Now</a>
  //     </Link>
  //   </div>
  // </Col>
  return (
    <div className={Styles.innerWrapper}>
      <div className="heading">
        <h1>{title}</h1>
      </div>
      <ContentCard>
        <div dangerouslySetInnerHTML={{ __html: description }}/>

        {/* <div className={Styles.address}>
          <span>Head, Human Resource</span>
          <span>Nepal Investment Bank Ltd</span>
          <span>Durbar Marg, P.O. Box: 3412</span>
          <span>Kathmandu, Nepal</span>
        </div> */}
      </ContentCard>

      {
        !!vacancyList && vacancyList.length > 0 && (
          <div className={Styles.vacanciesSection}>
            <div className="heading">
              <h1>{t('template_career_vacancies')}</h1>
            </div>

            <div className={Styles.tableContainer}>
              <div className={Styles.tableInner}>
                <ContentHeader>
                  <div className={Styles.tableHead}>
                    <Row>
                      <Col span={2}>
                        <div className={Styles.sn}>
                          S.No
                        </div>
                      </Col>
                      <Col span={8}>
                        <div className={Styles.position}>
                          {t('template_career_position')}
                        </div>
                      </Col>
                      <Col span={8}>
                        <div className={Styles.deadline}>
                          {t('template_career_deadline')}
                        </div>
                      </Col>
                    </Row>
                  </div>
                </ContentHeader>
                <ExpansionPanel
                  items={vacancyList}
                />
              </div>
            </div>
          </div>
        )
      }
    </div>
  );
};

export default withTranslation('common')(JobsAndVacancies);
