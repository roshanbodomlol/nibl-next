import BannerBlock from '../../BannerBlock';
import JobsAndVacancies from './JobsAndVacancies';
import Transformer from './career.tranformer';
import Styles from './career.module.scss';

const Career = (props) => {
  const data = new Transformer(props);
  const {
    vacancies,
    bannerAlt,
    bannerCaption,
    bannerTitle,
    bannerImage,
    pageDescription,
    title,
    bannerUrl
  } = data;
  return (
    <div className={Styles.wrapper}>
      <BannerBlock
        background={bannerImage}
        alt={bannerAlt}
        description={bannerCaption}
        title={bannerTitle}
        link={bannerUrl}
      />
      <div className="white-gradient main--content">
        <div className="container">
          <JobsAndVacancies vacancies={vacancies} description={pageDescription} title={title}/>
        </div>
      </div>
    </div>
  );
};

export default Career;
