import Styles from './career.module.scss';
import BODList from '../../BODList';

const bods = [
  {
    image: '/img/chairman-img.jpg',
    name: 'Jon Doe',
    designation: 'Chairman',
    description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.',
    facebook: 'fb.com',
    twitter: 'twitter.com',
    linkedin: 'linkedin.com'
  },
  {
    image: '/img/bod1.jpg',
    name: 'Jane Dove',
    designation: 'Vice Chairman',
    description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.',
    facebook: 'fb.com',
    twitter: 'twitter.com',
    linkedin: 'linkedin.com'
  },
  {
    image: '/img/bod3.jpg',
    name: 'Joe clover',
    designation: 'Vice Chairman',
    description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.',
    facebook: 'fb.com',
    twitter: 'twitter.com',
    linkedin: 'linkedin.com'
  },
  {
    image: '/img/bod4.jpg',
    name: 'James Dean',
    designation: 'Vice Chairman',
    description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.',
    facebook: 'fb.com',
    twitter: 'twitter.com',
    linkedin: 'linkedin.com'
  },
  {
    image: '/img/bod5.jpg',
    name: 'Dove Johns',
    designation: 'Vice Chairman',
    description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.',
    facebook: 'fb.com',
    twitter: 'twitter.com',
    linkedin: 'linkedin.com'
  },
  {
    image: '/img/bod3.jpg',
    name: 'Ken Thompson',
    designation: 'Vice Chairman',
    description: '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.</p>',
    facebook: 'fb.com',
    twitter: 'twitter.com',
    linkedin: 'linkedin.com'
  }
];

const MeetOurEmployees = () => (
  <div className={Styles.innerWrapper}>
    <div className="container">
      <BODList bods={bods} title="Meet Our Employees"/>
    </div>
  </div>
);

export default MeetOurEmployees;
