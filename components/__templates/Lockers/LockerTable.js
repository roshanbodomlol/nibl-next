import { Row, Col } from 'antd';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { withTranslation } from '../../../locales/i18n';
import ContentHeader from '../../ContentHeader';
import Styles from './lockers.module.scss';
import TableBody from '../../TableBody';

const LockerTable = ({ details, t }) => {
  const lockerRow = details.map(detailItem => (
    <TableBody>
      <div className={Styles.topHeading}>
        <Row>
          <Col span={3}>
            <div>{detailItem.lockerType}</div>
          </Col>
          <Col span={6}>
            <div className={Styles.hwd}>
              <span>{detailItem.height}</span>
              <span>{detailItem.width}</span>
              <span>{detailItem.depth}</span>
            </div>
          </Col>
          <Col span={3}>
            <div className={Styles.deposits}>{detailItem.keyDeposits}</div>
          </Col>
          <Col span={12}>
            <div className={Styles.anualCharges}>
              <span>{detailItem.annualChargeOutsideValley}</span>
              <span>{detailItem.annualChargeInsideValley}</span>
            </div>
          </Col>
        </Row>
      </div>
    </TableBody>
  ));

  return (
    <div className={classnames(Styles.tableWrapper, 'table-wrapper')}>
      <ContentHeader grey>
        <div className={Styles.tableHead}>
          <div className={Styles.topHeading}>
            <Row>
              <Col span={12}>
                <div className={Styles.dimension}>{t('template_locker_dimensionInches')}</div>
              </Col>
              <Col span={12}>
                <div className={Styles.charges}>{t('template_locker_chargesNPR')}</div>
              </Col>
            </Row>
            <Row>
              <Col span={3}>
                <div>{t('template_locker_type')}</div>
              </Col>
              <Col span={6}>
                <div className={Styles.hwd}>
                  <span>{t('global_height')}</span>
                  <span>{t('global_width')}</span>
                  <span>{t('global_depth')}</span>
                </div>
              </Col>
              <Col span={3}>
                <div className={Styles.deposits}>{t('template_locker_keyDeposits')}</div>
              </Col>
              <Col span={12}>
                <div className={Styles.anualCharges}>
                  <span>{t('template_locker_chargesOutsideValley')}</span>
                  <span>{t('template_locker_chargesInsideValley')}</span>
                </div>
              </Col>
            </Row>
          </div>
        </div>
      </ContentHeader>
      <div className={Styles.tableBodyWrapper}>
        {lockerRow}
      </div>
    </div>
  );
};

LockerTable.propTypes = {
  details: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default withTranslation('common')(LockerTable);
