import slugs from 'slugs';
import { chain, zipObject } from 'lodash';

export default class LockerTransformer {
  constructor(LockerAPIObject) {
    this.bannerImg = LockerAPIObject.bannerDetails.bannerImage;
    this.bannerTitle = LockerAPIObject.bannerDetails.bannerTitle;
    this.bannerDescription = LockerAPIObject.bannerDetails.bannerCaption;
    this.bannerAlt = LockerAPIObject.bannerDetails.bannerAlt;
    this.bannerUrl = LockerAPIObject.bannerDetails.bannerUrl;
    this.contentDescription = LockerAPIObject.pageContent.overviewDescription;
    this.lockerImage = LockerAPIObject.pageContent.overviewImage;
    this.lockerImageAlt = LockerAPIObject.pageContent.overviewAltText;
    this.lockerFeatures = {
      content: LockerAPIObject.pageContent.featureDescription,
      image: {
        url: LockerAPIObject.pageContent.featureImage,
        alt: LockerAPIObject.pageContent.featureImageAltText,
        title: LockerAPIObject.pageContent.featureImageTitle,
        description: LockerAPIObject.pageContent.featureImageCaption,
        link: LockerAPIObject.pageContent.featureImageDetailLink
      }
    };
    this.lockerRequirements = {
      content: LockerAPIObject.pageContent.requirementDescription,
      image: {
        url: LockerAPIObject.pageContent.requirementImage,
        alt: LockerAPIObject.pageContent.requirementImageAltText,
        title: LockerAPIObject.pageContent.requirementImageTitle,
        description: LockerAPIObject.pageContent.requirementImageCaption,
        link: LockerAPIObject.pageContent.lockerRequirementsImageDetailLink
      }
    };

    // populate items
    this.items = chain(LockerAPIObject.pageLockerSizes)
      .groupBy('categoryName')
      .toPairs()
      .map(currentData => zipObject(['name', 'details'], currentData))
      .value()
      .map(item => ({
        ...item,
        key: slugs(item.name)
      }));
    this.activeItemKey = this.items[0] && this.items[0].key;

    this.availableLockers = chain(LockerAPIObject.pageLockerAvailable)
      .groupBy('branchName')
      .toPairs()
      .map(currentData => zipObject(['branch', 'details'], currentData))
      .value();
    this.lockerTimings = LockerAPIObject.pageLockerTiminng;
    this.featureTitle = LockerAPIObject.pageContent.featureTitle;
    this.overviewTitle = LockerAPIObject.pageContent.overviewTitle;
    this.requirementTitle = LockerAPIObject.pageContent.requirementTitle;
  }
}
