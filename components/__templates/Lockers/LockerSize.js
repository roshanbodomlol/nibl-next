import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { withTranslation } from '../../../locales/i18n';
import TabNav from '../../TabNav';
import TabContent from '../../TabContent';
import LockerTable from './LockerTable';
import Styles from './lockers.module.scss';

class LockerSize extends Component {
  state = {
    activeItemKey: null
  };

  componentDidMount() {
    const { items } = this.props;
    if (items.length > 0) {
      this.setState({ activeItemKey: items[0].key });
    }
  }

  render() {
    const { activeItemKey } = this.state;
    const { items, t } = this.props;
    const lockerList = items.map(item => (
      <LockerTable key={`locker-${item.key}`} details={item.details} isActive={activeItemKey === item.key}/>
    ));

    return (
      <div className={Styles.lockerSizeWrapper}>
        <div className="container">
          <div className={Styles.heading}>
            <h1>{t('template_locker_sizes')}</h1>
          </div>
          <TabNav
            onClick={(key) => { this.setState({ activeItemKey: key }); }}
            activeItemKey={activeItemKey}
            items={items}
          />
          <TabContent>
            {lockerList}
          </TabContent>
        </div>
      </div>
    );
  }
}

LockerSize.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object)
};

LockerSize.defaultProps = {
  items: []
};

export default withTranslation('common')(LockerSize);
