import { Row, Col } from 'antd';
import { useEffect, useRef, useState } from 'react';
import { useRouter } from 'next/router';
import $ from 'jquery';

import { withTranslation } from '../../../locales/i18n';
import BannerBlock from '../../BannerBlock';
import LeftImageText from '../../LeftImageText';
import FeaturesAndBenefits from '../../FeaturesAndBenefits';
import LockerSize from './LockerSize';
import TableBody from '../../TableBody';
import LockerTransformer from './lockers.transformer';
import Styles from './lockers.module.scss';
import ContentHeader from '../../ContentHeader';
import TabNav from '../../TabNav';

const Lockers = ({ t, ...props }) => {
  const data = new LockerTransformer(props);
  const {
    bannerImg,
    bannerAlt,
    bannerTitle,
    bannerDescription,
    contentDescription,
    lockerFeatures,
    lockerRequirements,
    items,
    lockerImage,
    lockerImageAlt,
    availableLockers,
    lockerTimings,
    featureTitle,
    overviewTitle,
    requirementTitle,
    bannerUrl
  } = data;

  const router = useRouter();

  const timingsRef = useRef(null);
  const availableRef = useRef(null);
  const facilityRef = useRef(null);
  const sizesRef = useRef(null);
  const [activeTab, setActiveTab] = useState('facility');

  const handleTabClick = (key) => {
    setActiveTab(key);
    $('body,html').animate(
      {
        scrollTop: $(`#key-${key}`).offset().top - 120
      },
      800
    );
    // if (key === 'timings') {
    //   window.scrollTo({ top: window.scrollY + timingsRef.current.getBoundingClientRect().top - 120, behavior: 'smooth' });
    // } else if (key === 'available') {
    //   window.scrollTo({ top: window.scrollY + availableRef.current.getBoundingClientRect().top - 120, behavior: 'smooth' });
    // } else if (key === 'facility') {
    //   window.scrollTo({ top: window.scrollY + facilityRef.current.getBoundingClientRect().top - 120, behavior: 'smooth' });
    // } else if (key === 'sizes') {
    //   window.scrollTo({ top: window.scrollY + sizesRef.current.getBoundingClientRect().top - 120, behavior: 'smooth' });
    // }
  };

  const handleHashChange = url => {
    const tabQuery = url.split('#')[1];

    if (tabQuery) {
      handleTabClick(tabQuery);
    }
  };

  useEffect(() => {
    router.events.on('hashChangeComplete', handleHashChange);
  }, []);

  const tabItems = [
    { name: 'Facility', key: 'facility' },
    { name: 'Locker Sizes', key: 'sizes' },
    { name: 'Locker Timings', key: 'timings' },
    { name: 'Available Lockers', key: 'available' }
  ];

  return (
    <div className={Styles.wrapper}>
      <BannerBlock
        background={bannerImg}
        title={bannerTitle}
        description={bannerDescription}
        alt={bannerAlt}
        link={bannerUrl}
      />
      <div className="white-gradient">
        <div className={Styles.greyBack}>
          <div style={{ marginTop: 35 }}>
            <TabNav
              items={tabItems}
              activeItemKey={activeTab}
              onClick={handleTabClick}
            />
          </div>
          <div className="container" ref={facilityRef}>
            <div className={Styles.heading} id="key-facility">
              <h1>{t('template_locker_title')}</h1>
            </div>
            <LeftImageText title={overviewTitle} image={lockerImage} imageAlt={lockerImageAlt}>
              <div dangerouslySetInnerHTML={{ __html: contentDescription }}/>
            </LeftImageText>
            <FeaturesAndBenefits
              firstTableTitle={requirementTitle}
              secondTableTitle={featureTitle}
              firstTableContent={lockerRequirements.content}
              secondTableContent={lockerFeatures.content}
              features={lockerRequirements.content}
              benefits={lockerFeatures.content}
              featuredBlockImage={lockerRequirements.image.url}
              featuredImageAlt={lockerRequirements.image.alt}
              featuredBlockImageCaption={lockerRequirements.image.description}
              featuredBlockImageTitle={lockerRequirements.image.title}
              featuredBlockImageLink={lockerRequirements.image.link}
              benefitsBlockImage={lockerFeatures.image.url}
              benefitsImageAlt={lockerFeatures.image.alt}
              benefitsBlockImageCaption={lockerFeatures.image.description}
              benefitsBlockImageTitle={lockerFeatures.image.title}
              benefitsBlockImageLink={lockerFeatures.image.link}
            />
          </div>
        </div>
        <div ref={sizesRef} id="key-sizes">
          <LockerSize
            items={items}
          />
        </div>
        <div className={Styles.otherTables}>
          <div className="container">
            <div className={Styles.tableSection} ref={timingsRef}>
              <div className={Styles.heading} id="key-timings">
                <h1>{t('template_locker_lockerTimings')}</h1>
              </div>
              <ContentHeader>
                <Row>
                  <Col span={2}>S.No</Col>
                  <Col span={7}>NIBL {t('global_branch')}</Col>
                  <Col span={5}>Normal</Col>
                  <Col span={5}>Fridays</Col>
                  <Col span={5}>{t('template_365_holidays')}</Col>
                </Row>
              </ContentHeader>
              {
                lockerTimings.map((l, i) => (
                  <TableBody key={`locker-types-${i}`}>
                    <Row>
                      <Col span={2}>{i + 1}</Col>
                      <Col span={7}>{l.branchName}</Col>
                      <Col span={5}>{l.normalTiming}</Col>
                      <Col span={5}>{l.fridayTiming}</Col>
                      <Col span={5}>{l.lockerTiming}</Col>
                    </Row>
                  </TableBody>
                ))
              }
            </div>
            <div className={Styles.tableSection} style={{ marginBottom: 60 }} ref={availableRef}>
              <div className={Styles.heading} id="key-available">
                <h1>{t('template_locker_availableLockers')}</h1>
              </div>
              <ContentHeader>
                <Row>
                  <Col span={2}>S.No</Col>
                  <Col span={8}>NIBL {t('global_branch')}</Col>
                  <Col span={14}>{t('template_locker_availableLockers')}</Col>
                </Row>
              </ContentHeader>
              {
                availableLockers.map((l, i) => {
                  const lockerTypes = l.details.map(e => e.lockerType);
                  return (
                    <TableBody key={`locker-types-${i}`}>
                      <Row>
                        <Col span={2}>{i + 1}</Col>
                        <Col span={8}>{l.branch}</Col>
                        <Col span={14}>{lockerTypes.join(', ')}</Col>
                      </Row>
                    </TableBody>
                  );
                })
              }
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withTranslation('common')(Lockers);
