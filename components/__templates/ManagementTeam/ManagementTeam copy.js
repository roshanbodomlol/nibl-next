import { Component } from 'react';

import BannerBlock from '../../BannerBlock';
import TabNav from '../../TabNav';
import TabContent from '../../TabContent';
import TableBody from '../../TableBody';
import ManagementTeamTbl from './ManagementTeamTbl';
import Styles from './managementTeam.module.scss';
import Transformer from './transformer';

class ManagementTeam extends Component {
  state = {
    activeItemKey: null,
    items: []
  }

  componentDidMount() {
    const data = new Transformer(this.props);
    const { activeItemKey, items } = data;
    this.setState({ activeItemKey, items });
  }

  handleNext = () => {
    const { items, activeItemKey } = this.state;

    let index = items.findIndex(item => item.key === activeItemKey);

    if (index === items.length - 1) {
      index = 0;
    } else {
      index++;
    }

    this.setState({ activeItemKey: items[index].key });
  }

  handlePrevious = () => {
    const { items, activeItemKey } = this.state;

    let index = items.findIndex(item => item.key === activeItemKey);

    if (index === 0) {
      index = items.length - 1;
    } else {
      index--;
    }

    this.setState({ activeItemKey: items[index].key });
  }

  render() {
    const data = new Transformer(this.props);
    const { activeItemKey, items } = this.state;
    const { bannerAlt, bannerCaption, bannerImage, bannerTitle } = data;

    const tableTabs = items.map((item, i) => {
      const tableContent = item.details.map((detail, j) => (
        <TableBody key={`team-member-table-${i}-${j}`}>
          <div className={Styles.tblGrid}>
            <span className={Styles.sn}>{i}</span>
            <span className={Styles.province}>{detail.memberProvince}</span>
            <span className={Styles.managers}>{detail.memberName}</span>
            <span className={Styles.provinceOffice}>{detail.memberProvinceOfficer}</span>
          </div>
        </TableBody>
      ));
      return <ManagementTeamTbl key={`team-table-${i}`} isActive={activeItemKey === item.key} content={tableContent}/>;
    });
    return (
      <>
        <BannerBlock background={bannerImage} alt={bannerAlt} title={bannerTitle} description={bannerCaption}/>
        <div className="white-gradient main--content">
          <div className="container">
           {
             tableTabs.length > 0
              ? (
                <>
                  <div className={Styles.tabNavWrapper}>
                    <TabNav
                      arrows
                      stretch
                      onClick={(key) => { this.setState({ activeItemKey: key }); }}
                      activeItemKey={activeItemKey}
                      items={items}
                      onLeftArrowClick={this.handlePrevious}
                      onRightArrowClick={this.handleNext}
                    />
                  </div>
                  <TabContent>
                    {tableTabs}
                  </TabContent>
                </>
              )
              : (
                <p>No data available</p>
              )
           }
          </div>
        </div>
      </>
    );
  }
}

export default ManagementTeam;
