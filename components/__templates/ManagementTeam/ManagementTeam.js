import { Row, Col } from 'antd';

import { withTranslation } from '../../../locales/i18n';
import BannerBlock from '../../BannerBlock';
import TableBody from '../../TableBody';
import Transformer from './transformer';
import Styles from './managementTeam.module.scss';
import ContentHeader from '../../ContentHeader';

const ManagementTeam = ({ t, ...props }) => {
  const data = new Transformer(props);

  const {
    bannerAlt,
    bannerCaption,
    bannerImage,
    bannerTitle,
    items,
    bannerUrl
  } = data;

  const tableContent = items.map((detail, i) => (
    <TableBody key={`team-member-table-${i}`}>
      <div className={Styles.tblGrid}>
        {
          props.pageDetails.teamType === 'DepartmentHead'
            ? (
              <Row>
                <Col span={2}>
                  <span className={Styles.sn}>{i + 1}</span>
                </Col>
                <Col span={10}>
                  <span className={Styles.managers}>{detail.memberName}</span>
                </Col>
                <Col span={6}>
                  <span className={Styles.managers}>{detail.memberDesignation}</span>
                </Col>
                <Col span={6}>
                  <span className={Styles.provinceOffice}>{detail.memberPhone}</span>
                </Col>
              </Row>
            )
            : (
              <Row>
                <Col span={2}>
                  <span className={Styles.sn}>{i + 1}</span>
                </Col>
                <Col span={8}>
                  <span className={Styles.managers}>{detail.memberName}</span>
                </Col>
                <Col span={6}>
                  <span className={Styles.provinceOffice}>{detail.memberProvince}</span>
                </Col>
                <Col span={6}>
                  <span className={Styles.provinceOffice}>{detail.memberPhone}</span>
                </Col>
              </Row>
            )
        }
      </div>
    </TableBody>
  ));
  return (
    <>
      <BannerBlock background={bannerImage} alt={bannerAlt} title={bannerTitle} description={bannerCaption} link={bannerUrl}/>
      <div className="white-gradient main--content">
        <div className="container">
          {
            tableContent.length > 0
              ? (
                <div className={Styles.tableContainer}>
                  <div className={Styles.tableInner}>
                    <ContentHeader>
                      <div className={Styles.tblGrid}>
                        {
                          props.pageDetails.teamType === 'DepartmentHead'
                            ? (
                              <Row>
                                <Col span={2}>
                                  <span className={Styles.sn}>S.No</span>
                                </Col>
                                <Col span={10}>
                                  <span className={Styles.managers}>{t('global_name')}</span>
                                </Col>
                                <Col span={6}>
                                  <span className={Styles.managers}>{t('global_department')}</span>
                                </Col>
                                <Col span={6}>
                                  <span className={Styles.provinceOffice}>{t('global_phone')}</span>
                                </Col>
                              </Row>
                            )
                            : (
                              <Row>
                                <Col span={2}>
                                  <span className={Styles.sn}>S.No</span>
                                </Col>
                                <Col span={8}>
                                  <span className={Styles.managers}>{t('global_name')}</span>
                                </Col>
                                <Col span={6}>
                                  <span className={Styles.provinceOffice}>{t('global_province')}</span>
                                </Col>
                                <Col span={6}>
                                  <span className={Styles.provinceOffice}>{t('global_phone')}</span>
                                </Col>
                              </Row>
                            )
                        }
                      </div>
                    </ContentHeader>
                    {tableContent}
                  </div>
                </div>
              )
              : (
                <p>{t('global_noDataAvailable')}</p>
              )
          }
        </div>
      </div>
    </>
  );
};

export default withTranslation('common')(ManagementTeam);
