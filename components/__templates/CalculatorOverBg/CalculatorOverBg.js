import { withTranslation } from '../../../locales/i18n';
import EmiCaclBlock from '../../EmiCalcBlock';
import styles from './calculatorOverBg.module.scss';

const CalculatorOverBg = ({ t }) => (
  <div className={styles.wrapper}>
    <div className="white-gradient main--content">
      <div className={styles.inner}>
        <div className={styles.left}>
          <h2>{t('global_contactUsQuery')}</h2>
        </div>
        <div className={styles.right}>
          <h1 className="heading">{t('template_calculator_title')}</h1>
          <div className={styles.calculator}>
            <EmiCaclBlock/>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default withTranslation('common')(CalculatorOverBg);
