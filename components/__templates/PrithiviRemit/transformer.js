export default class Transformer {
  constructor(APIObject) {
    this.bannerImg = APIObject.bannerDetails.bannerImage;
    this.bannerTitle = APIObject.bannerDetails.bannerTitle;
    this.bannerDescription = APIObject.bannerDetails.bannerCaption;
    this.bannerAlt = APIObject.bannerDetails.bannerAlt;
    this.bannerUrl = APIObject.bannerDetails.bannerUrl;
    this.title = APIObject.pageDetails.pageTitle;
    this.firstFeatureTitle = APIObject.pageContent[0].featureTitleA;
    this.firstFeatureContent = APIObject.pageContent[0].featureDescriptionA;
    this.firstFeatureImage = APIObject.pageContent[0].featureImageA;
    this.firstFeatureImageTitle = APIObject.pageContent[0].featureImageTitleA;
    this.firstFeatureImageDescription = APIObject.pageContent[0].featureImageCaptionA;
    this.secondFeatureTitle = APIObject.pageContent[0].featureTitleB;
    this.secondFeatureContent = APIObject.pageContent[0].featureDescriptionB;
    this.secondFeatureImage = APIObject.pageContent[0].featureImageB;
    this.secondFeatureImageTitle = APIObject.pageContent[0].featureImageTitleB;
    this.secondFeatureImageDescription = APIObject.pageContent[0].featureImageCaptionB;
    this.currencyImage = APIObject.pageContent[0].currencyImage;
    this.currencyContent = APIObject.pageContent[0].currencyDescription;
    this.overviewContent = APIObject.pageContent[0].overviewDescription;
    this.overviewImage = APIObject.pageContent[0].overviewImage;
    this.overviewImageAlt = APIObject.pageContent[0].overviewAltText;
  }
}
