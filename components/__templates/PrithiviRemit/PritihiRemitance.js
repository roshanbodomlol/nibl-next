import classnames from 'classnames';
import { Row, Col } from 'antd';

import { withTranslation } from '../../../locales/i18n';
import BannerBlock from '../../BannerBlock';
import BigTitleBlock from '../../Layouts/Block/BigTitleBlock';
import ImageBlockWithOverlay from '../../Layouts/Block/ImageBlockWithOverlay';
import ForexConverter from '../../ForexConverter';
import LeftImageText from '../../LeftImageText';
import Transformer from './transformer';
import Styles from './prithivi.module.scss';

const PritiviRemitance = ({ t, ...props }) => {
  const data = new Transformer(props);
  const {
    bannerImg,
    bannerTitle,
    bannerDescription,
    bannerAlt,
    firstFeatureTitle,
    firstFeatureContent,
    firstFeatureImage,
    firstFeatureImageTitle,
    firstFeatureImageDescription,
    secondFeatureTitle,
    secondFeatureContent,
    secondFeatureImage,
    secondFeatureImageTitle,
    secondFeatureImageDescription,
    currencyContent,
    overviewContent,
    overviewImage,
    overviewImageAlt,
    title,
    bannerUrl
  } = data;

  return (
    <div className={Styles.wrapper}>
      <BannerBlock
        background={bannerImg}
        title={bannerTitle}
        description={bannerDescription}
        alt={bannerAlt}
        link={bannerUrl}
      />
      <div className="white-gradient">
        <div className={Styles.greyInner}>
          <div className="container">
            <div className={Styles.heading}>
              <h1>{title}</h1>
            </div>
            <LeftImageText
              title={t('template_overview_title')}
              image={overviewImage}
              imageAlt={overviewImageAlt}
            >
              <div dangerouslySetInnerHTML={{ __html: overviewContent }}/>
            </LeftImageText>

            <div className={Styles.heading}>
              <h1 className="heading">{t('template_remittance_mainFeatures')}</h1>
            </div>
            <div className={Styles.flexSection}>
              <Row gutter={24} type="flex">
                <Col sm={17} xs={24}>
                  <BigTitleBlock title={firstFeatureTitle}>
                    <div className="wysiwyg" dangerouslySetInnerHTML={{ __html: firstFeatureContent }}/>
                  </BigTitleBlock>
                </Col>
                <Col sm={7} xs={24}>
                  <ImageBlockWithOverlay title={firstFeatureImageTitle} image={firstFeatureImage} imageHeight={400}>
                    <p>{firstFeatureImageDescription}</p>
                  </ImageBlockWithOverlay>
                </Col>
              </Row>
            </div>
            <div className={classnames(Styles.flexSection, Styles.flexSectionMob)}>
              <Row type="flex" gutter={24}>
                <Col sm={{ span: 7, order: 1 }} xs={{ span: 24, order: 2 }}>
                  <ImageBlockWithOverlay title={secondFeatureImageTitle} image={secondFeatureImage} imageHeight={400}>
                    <p>{secondFeatureImageDescription}</p>
                  </ImageBlockWithOverlay>
                </Col>
                <Col sm={{ span: 17, order: 2 }} xs={{ span: 24, order: 1 }}>
                  <BigTitleBlock title={secondFeatureTitle}>
                    <div className="wysiwyg" dangerouslySetInnerHTML={{ __html: secondFeatureContent }}/>
                  </BigTitleBlock>
                </Col>
              </Row>
            </div>
          </div>

          {/* <div className={Styles.bottomSection} style={{ background: 'url(/img/objectives-back.jpg)' }}>
            <div className="container">
              <div className={Styles.outerSection}>
                <div className={Styles.innerSection}>
                  <ForexConverter/>
                  <div className={Styles.bottomList}>
                    <div className="wysiwyg" dangerouslySetInnerHTML={{ __html: currencyContent }}/>
                  </div>
                </div>
              </div>
            </div>
          </div> */}
        </div>
      </div>
    </div>
  );
};

export default withTranslation('common')(PritiviRemitance);
