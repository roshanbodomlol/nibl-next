import { withTranslation } from '../../../locales/i18n';
import BannerBlock from '../../BannerBlock';
import ContentHeader from '../../ContentHeader';
import ExpansionPanel from '../../ExpansionPanel';
import Transformer from './transformer';
import Styles from './certification.module.scss';

const Certification = ({ t, ...props }) => {
  const data = new Transformer(props);
  const {
    bannerImg,
    bannerTitle,
    bannerDescription,
    bannerAlt,
    pageTitle,
    certification,
    bannerUrl
  } = data;

  const certificationTable = certification.map(item => (
    {
      primary:
      (
        <div className={`${Styles.primaryPanel}`}>
          <div>
            <p>
              {item.primary}
            </p>
          </div>
          <a href={`${item.downloadLink}`}><img src="/img/download.png" alt=""/></a>
        </div>
      ),
      secondary: <div>{item.secondary}</div>
    }
  ));

  return (
    <div className={Styles.wrapper}>
      <BannerBlock
        title={bannerTitle}
        description={bannerDescription}
        background={bannerImg}
        alt={bannerAlt}
        link={bannerUrl}
      />
      <div className="white-gradient main--content">
        <div className="container">
          <div className="heading">
            <h1>{pageTitle}</h1>
          </div>
          <ContentHeader grey>
            <div className={Styles.contentHeader}>
              <span className={Styles.sn}>S.No</span>
              <span className={Styles.details}>{t('Details')}</span>
            </div>
          </ContentHeader>
          <ExpansionPanel
            isOrdered
            items={certificationTable}
          />
        </div>
      </div>
    </div>
  );
};

export default withTranslation('common')(Certification);
