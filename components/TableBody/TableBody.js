import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Styles from './tableBody.module.scss';

/**
* @visbleName Table Body
* */

const TableBody = ({ children }) => (
  <div className={classnames(Styles.tableBodyWrapper, 'table-body-wrapper')}>
    <div className={classnames(Styles.tableContents, 'table-contents')}>
      {children}
    </div>
  </div>
);

TableBody.propTypes = {
  children: PropTypes.node.isRequired
};

export default TableBody;
