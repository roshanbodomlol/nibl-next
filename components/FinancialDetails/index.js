import PropTypes, { any } from 'prop-types';

import { withTranslation } from '../../locales/i18n';
import ContentHeader from '../ContentHeader';
import ExpansionPanel from '../ExpansionPanel';
import Styles from './styles.module.scss';

const FinancialDetails = ({
  details, title, showNetWorth, t, netWorthData, year
}) => {
  let networth = [];
  if (showNetWorth) {
    networth = netWorthData.filter(e => e.contentYear === year);
  }
  const detailsRow = details.map((item) => (
    {
      primary:
      (
        <div className={`${Styles.primaryPanel}`}>
          <div>
            <p>
              {item.primary}
            </p>
          </div>
          <a target="_blank" rel="noreferrer noopener" href={`${item.downloadLink}`}><img src="/img/download.png" alt=""/></a>
        </div>
      ),
      secondary: <div dangerouslySetInnerHTML={{ __html: item.secondary }}/>
    }
  ));

  return (
    <div>
      <div className="heading">
        <h1>{title}</h1>
      </div>
      {
        !!detailsRow && detailsRow.length > 0 && (
          <>
            <ContentHeader grey>
              <div className={Styles.contentHeader}>
                <span className={Styles.sn}>S.No</span>
                <span className={Styles.details}>{t('global_details')}</span>
              </div>
            </ContentHeader>
            <ExpansionPanel
              isOrdered
              items={detailsRow}
            />
          </>
        )
      }
      {
        showNetWorth && (
          <>
            <div className="spacer"/>
            {
              !!networth && networth.length > 0 && (
                <div className="financial-transactions">
                  <ContentHeader>
                    <span className={Styles.transactionTableIndex}>S.No</span>
                    <span className={Styles.transactionTableDetails}>{t('global_details')}</span>
                    <span className={Styles.transactionTableFigures}>{t('payment_figuesNPR')}</span>
                  </ContentHeader>
                  {
                    networth.map((item, index) => {
                      return (
                        <div className={Styles.transactionRow} key={`transaction-row-${index}`}>
                          <span>{index + 1}</span>
                          <span>{item.contentTitle}</span>
                          <span>{item.contentAmount}</span>
                        </div>
                      );
                    })
                  }
                </div>
              )
            }
          </>
        )
      }
    </div>
  );
};

FinancialDetails.propTypes = {
  details: PropTypes.arrayOf(PropTypes.object).isRequired,
  title: PropTypes.string.isRequired,
  showNetWorth: PropTypes.bool,
  year: any
};

FinancialDetails.defaultProps = {
  showNetWorth: false,
  year: null
};

export default withTranslation('common')(FinancialDetails);
