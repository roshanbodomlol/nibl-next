import { Component } from 'react';

import BannerBlock from '../BannerBlock';
import styles from './ebanking.module.scss';
import TabNav from '../TabNav';
import TabEbaking from './TabEbanking';
import TabContent from '../TabContent';
import TabSafetyTips from './TabSafetyTips';
import TabLogin from './TabLogin';
import Logos from '../Logos';

class Ebanking extends Component {
  state = {
    items: [],
    activeItemKey: ''
  }

  componentDidMount() {
    this.setState({
      items: [
        {
          name: 'eBanking',
          key: 'ebanking'
        },
        {
          name: 'Safety Tips',
          key: 'safety-tips'
        },
        {
          name: 'Information on Email Fraud',
          key: 'information'
        },
        {
          name: 'Features',
          key: 'features'
        },
        {
          name: 'Services Offered',
          key: 'services'
        },
        {
          name: 'Reporting Fake Emails',
          key: 'reporting'
        },
        {
          name: 'Login',
          key: 'login'
        }
      ],
      activeItemKey: 'ebanking'
    });
  }

  render() {
    const { items, activeItemKey } = this.state;

    return (
      <div className={styles.ebankingWrapper}>
        <BannerBlock
          background="/img/mobtop-bg.jpg"
        />
        <div className={styles.logos}>
          <Logos/>
        </div>
        <div className="white-gradient main--content">
          <TabNav
            onClick={(key) => { this.setState({ activeItemKey: key }); }}
            activeItemKey={activeItemKey}
            items={items}
          />
          <div className="container">
            <TabContent>
              <TabEbaking isActive={activeItemKey === 'ebanking'}/>
              <TabSafetyTips title="Safety Tips" isActive={activeItemKey === 'safety-tips'}/>
              <TabSafetyTips title="Information on Email Fraud" isActive={activeItemKey === 'information'}/>
              <TabSafetyTips title="Features" isActive={activeItemKey === 'features'}/>
              <TabSafetyTips title="Services Offered" isActive={activeItemKey === 'services'}/>
              <TabSafetyTips title="Reporting Fake Emails" isActive={activeItemKey === 'reporting'}/>
              <TabLogin isActive={activeItemKey === 'login'}/>
            </TabContent>
          </div>
        </div>
      </div>
    );
  }
}

export default Ebanking;
