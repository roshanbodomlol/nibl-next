import PropTypes from 'prop-types';

import ContentCard from '../ContentCard';

const TabsSafetyTips = ({ title }) => (
  <div>
    <h1 className="heading">{title}</h1>
    <ContentCard>
      <div className="wysiwyg">
        <h3>Online Banking Security</h3>
        <p>NIBL is committed in ensuring that your online banking is both safe and secure. With the state of the art security infrastructure in place, online access to customers’ accounts is both confidential and protected. Nevertheless, it is advisable to implement a proactive stance when it comes to online banking system.</p>
        <h3>Your Role</h3>
        <p>While NIBL works to protect your banking privacy, you can also play a role in protecting your accounts. There are a number of steps you can take to ensure that your banking experience on the Internet is safe and secure.</p>
        <h3>Ways to protect yourself</h3>
        <ul>
          <li>To continue to develop products and services that reduce our cost of funds</li>
          <li>Your online password will protect the privacy of your banking information only if you keep it to yourself. If you think your online Password has been compromised, change it immediately online.</li>
          <li>Don’t walk away from your computer if you are in the middle of a session.</li>
          <li>Once you have finished conducting your banking on the Internet, always log off before visiting other Internet sites. Do not just close your browser.</li>
          <li>If anyone else is likely to use your computer, clear your cache or turn off and reinitiate your browser in order to eliminate copies of Web pages that have been stored in your hard drive. How you clear your cache will depend on the browser and version you have. This function is generally found in your preferences menu.</li>
          <li>Avoid accessing your Internet Banking account from a cyber cafe or a shared computer. However, if you happen to do so change your passwords from your own computer.</li>
          <li>To access NIBL Internet Banking, always type in the correct URL ( <a href="https://www.nibl.com.np">https://www.nibl.com.np</a> ) into your browser window. Never click a link that offers to take you to our website</li>
          <li>NIBL never requests customers to provide their passwords.</li>
          <li>NIBL never send links in email for any reason.</li>
          <li>NIBL never asks for transaction password in the login screen.</li>
          <li>If your Customer Code or passwords appear automatically on the sign-in page of a secure website, you should disable the “Auto Complete” function to increase the security of your information.</li>
        </ul>
        <h3>To disable the "Auto Complete" function</h3>
        <ul>
          <li>Open Internet Explorer and click "Tools" > "Internet Options" > "Content".</li>
          <li>Under "Personal Information", click "Auto Complete”.</li>
          <li>Uncheck "User names and passwords on forms" and click "Clear Passwords".</li>
          <li>Click "OK".</li>
        </ul>
        <h3>Contact NIBL</h3>
        <ul>
          <li>If you forget your password </li>
          <li>If you are unable to log in to your Internet Banking (eBanking) account.</li>
          <li>If you notice any suspicious activities on your account.</li>
        </ul>
        <p><b><span style={{ color: '#E50019' }}>Once you have made sure that all these factors are taken into consideration you can be assured that your online banking is completely safe</span></b></p>
      </div>
    </ContentCard>
  </div>
);

TabsSafetyTips.propTypes = {
  title: PropTypes.string.isRequired
}

export default TabsSafetyTips;
