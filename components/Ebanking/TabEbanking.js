import { Row, Col } from 'antd';

import LeftImageText from '../LeftImageText';
import ContentHeader from '../ContentHeader';
import TableBody from '../TableBody';

const TabEbanking = () => (
  <div>
    <h1 className="heading">eBanking</h1>
    <LeftImageText
      redTitle
      title="Details"
      image="/img/nibl-dummy.jpg"
    >
      <p>
        We are pleased to introduce our new eBanking service, which allows you to avail online banking transactions from any part of the world.
      </p>
      <p>
        With the change in technology, Nepal Investment Bank Ltd, more than ever, felt the need for banking convenience for their clients. You can now access and have full control over your accounts 24 hours 7 days a week. We welcome you to our web site, 
        <a href="http://www.nibl.com.np" style={{ color: '#E50019' }}>http://www.nibl.com.np</a>.
      </p>
      <p>
        Please Visit our Features page, Advantages page and FAQs page for further information regarding the available facilities. You can even Download the form online and apply for this facility.
      </p>
      <p>
      This facility is secure. The transaction facility is made more secure through the use of NIBL One Time Password (OTP) facility 
(NPR 200 annual charge).
      </p>
    </LeftImageText>
    <ContentHeader>
      <Row style={{ width: '100%' }} type="flex" align="middle">
        <Col span={4}><span>S.No</span></Col>
        <Col span={5}><span>Types of Services</span></Col>
        <Col span={5}><span>Maximum Per Transaction Limit</span></Col>
        <Col span={5}><span>Maximum Daily Limit</span></Col>
        <Col span={5}><span>Maximum Monthly Limit</span></Col>
      </Row>
    </ContentHeader>
    <TableBody>
      <Row style={{ width: '100%' }} type="flex" align="middle">
        <Col span={4}><span>1</span></Col>
        <Col span={5}><span>E-Banking</span></Col>
        <Col span={5}><span>NPR 25,000.00</span></Col>
        <Col span={5}><span>NPR 50,000.00</span></Col>
        <Col span={5}><span>NPR 3,00,000.00</span></Col>
      </Row>
    </TableBody>
  </div>
);

export default TabEbanking;
