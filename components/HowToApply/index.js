import { Row, Col } from 'antd';

import Styles from './styles.module.scss';

const HowToApply = ({
  questions
}) => {
  const list = questions.map((question, index) => (
    <Col sm={8} xs={24} key={`ho-t-app-${index}`}>
      <div className={Styles.sideBlock}>
        <span className={Styles.title}>{question.question}</span>
        <div className={Styles.content}>
          <div dangerouslySetInnerHTML={{ __html: question.answer }}/>
        </div>
        <div className={Styles.link}>
          {
            question.link && <a href={question.link} download>Apply Now</a>
          }
        </div>
      </div>
    </Col>
  ));
  return (
    <div className={Styles.gridBlock}>
      <Row type="flex" gutter={30}>
        <Col sm={8} xs={24}>
          <div className={Styles.headingBlock}>
            <span>How To Apply</span>
          </div>
        </Col>
        {list}
      </Row>
    </div>
  );
};

export default HowToApply;
