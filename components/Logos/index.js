import { connect } from 'react-redux';

const mapStateToProps = ({ common }) => ({ common });

const Logos = ({ common }) => (
  <>
    {
      common.logos.other.map((logo) => (
        <>
          {
            logo.logoUrl
              ? <a target="_blank" rel="noopener noreferrer" href={logo.logoUrl}><img src={logo.logoImage} alt={logo.logoAltText}/></a>
              : <img src={logo.logoImage} alt={logo.logoAltText}/>
          }
        </>
      ))
    }
  </>
);

export default connect(mapStateToProps)(Logos);
