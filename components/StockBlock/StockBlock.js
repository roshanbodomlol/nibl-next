import { useState, useEffect } from 'react';
import moment from 'moment';

import { withTranslation } from '../../locales/i18n';
import Loading from '../Loading';
import { get } from '../../services/api.services';
import { API } from '../../constants';
import Styles from './stockBlockStyles.module.scss';

const StockBlock = ({ t }) => {
  const [nepseData, setNepseData] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    get(API.endPoints.NEPSE)
      .then((data) => {
        setLoading(false);
        setNepseData(data.content);
      })
      .catch((e) => console.error(e));
  }, []);

  return (
    <div className={Styles.back}>
      <div className={Styles.date}>
        On {moment(nepseData.postedDate).format('dddd, MMMM DD, YYYY, h:mm:ss a')}
      </div>
      {
        loading
          ? <Loading/>
          : (
            <table className={Styles.table}>
              <tbody>
                <tr>
                  <td>{t('template_stock_paidUpValue')}</td>
                  <td>{nepseData.paidUpValue}</td>
                </tr>
                <tr>
                  <td>{t('template_stock_closing')}</td>
                  <td>{nepseData.closingMRP}</td>
                </tr>
                <tr>
                  <td>{t('template_stock_listedShares')}</td>
                  <td>{nepseData.listedShares}</td>
                </tr>
              </tbody>
            </table>
          )
      }
    </div>
  );
};

export default withTranslation('common')(StockBlock);
