import { Component } from 'react';

import $ from 'jquery';
import LeftImageText from '../LeftImageText';
import css from './bodlist.module.scss';

class BODList extends Component {
  constructor() {
    super();
    this.state = {
      activeUserIndex: 0
    };
    this.mainRef = React.createRef();
  }

  handleActiveuser = (index) => {
    this.setState({
      activeUserIndex: index
    }, () => {
      $('body,html').animate(
        {
          scrollTop: $('#key-main').offset().top - 120
        },
        800
      );
      // window.scrollTo({ top: window.scrollY + this.mainRef.getBoundingClientRect().top - 150, behavior: 'smooth' });
    });
  }

  render() {
    const { activeUserIndex } = this.state;
    const { bods, title, ceo } = this.props;

    const activeUser = bods[activeUserIndex];

    const bodList = bods.map((bod, index) => (
      <div key={`bod-member-${bod.id}`} role="button" tabIndex="-1" onClick={() => { this.handleActiveuser(index); }} className={css.listElement}>
        <img src={bod.image} alt=""/>
        <div className={css.bodListText}>
          <span>{bod.name}</span>
          <span>{bod.designation}</span>
        </div>
      </div>
    ));

    return (
      <div className={css.wrapper} id="key-main" ref={(c) => { this.mainRef = c; }}>
        <div className={css.heading}>
          <span>{title}</span>
        </div>
        <div className={css.content}>
          <LeftImageText title={activeUser.name} image={activeUser.image}>
            <span><b>{activeUser.designation}</b></span>
            <div dangerouslySetInnerHTML={{ __html: activeUser.description }}/>
            <div className={css.socialIcons}>
              {
                activeUser.facebook && (
                  <a href={activeUser.facebook} target="_blank" rel="noopener noreferrer">
                    <i className="fab fa-facebook-f"/>
                  </a>
                )
              }
              {
                activeUser.twitter && (
                  <a href={activeUser.twitter} target="_blank" rel="noopener noreferrer">
                    <i className="fab fa-twitter"/>
                  </a>
                )
              }
              {
                activeUser.linkedin && (
                  <a href={activeUser.linkedin} target="_blank" rel="noopener noreferrer">
                    <i className="fab fa-linkedin"/>
                  </a>
                )
              }
            </div>
          </LeftImageText>
        </div>

        {
          !ceo && (
            <div className={css.list}>
              {bodList}
            </div>
          )
        }
      </div>
    );
  }
}

BODList.defaultProps = {
  ceo: false
};

export default BODList;
