import Styles from './mapWithWhiteCard.module.scss';

const MapWithWhiteCard = ({ content, map }) => (
  <div className={Styles.flexBlock}>
    <div className={Styles.leftBlock} dangerouslySetInnerHTML={{ __html: map }}/>
    {/* <iframe title="corporate-map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3532.1808203340884!2d85.3156966149504!3d27.711702831907708!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb1903e46cc0d7%3A0x1e4a3450efd18e9b!2sNepal%20Investment%20Bank%20Limited!5e0!3m2!1sen!2snp!4v1575265866984!5m2!1sen!2snp" width="100%" frameborder="0" style={{ border: 0, height: '100%' }} allowfullscreen=""/> */}
    <div className={Styles.rightBlock}>
      <div className="wysiwyg" dangerouslySetInnerHTML={{ __html: content }}/>
    </div>
  </div>
);

export default MapWithWhiteCard;
