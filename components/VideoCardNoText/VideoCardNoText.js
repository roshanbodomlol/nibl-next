import React from 'react';
import PropTypes from 'prop-types';

import Styles from './videoCardNoText.module.scss';

const VideoCardNoText = ({ onClick, videoThumb }) => (
  <div role="button" tabIndex="-1" onClick={onClick} className={Styles.wrapper}>
    <div className={Styles.contentWrapper}>
      <div className={Styles.videoThumb}>
        <img src={videoThumb} alt=""/>
        <div className={Styles.overlay}>
          <img src="/img/play.png" alt=""/>
        </div>
      </div>
    </div>
  </div>
);

VideoCardNoText.propTypes = {
  onClick: PropTypes.func.isRequired,
  videoThumb: PropTypes.string.isRequired
};

export default VideoCardNoText;
