import React, { Component } from 'react';
import { Spin, Icon } from 'antd';
import Slider from 'react-slick';
import { MdClose as CloseIcon } from 'react-icons/md';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { setModalOpen, setModalClose, setSplashClose } from '../../redux/actions/common.actions';
import { get } from '../../services/api.services';
import { API } from '../../constants';
import './SplashSlider.scss';

const mapStateToProps = ({ common }) => ({ common });

class SplashSlider extends Component {
  state = {
    isLoading: true,
    slides: []
  };

  sliderRef = React.createRef();

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(setModalOpen());
    this.getSlides();
    document.addEventListener('keydown', this.listenToKeyboard);
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch(setModalClose());
    document.removeEventListener('keydown', this.listenToKeyboard);
  }

  listenToKeyboard = (e) => {
    if (e.keyCode === 27) this.closeSplash();
    if (e.keyCode === 39) this.sliderRef.slickNext();
    if (e.keyCode === 37) this.sliderRef.slickPrev();
  }

  getSlides = () => {
    get(API.endPoints.POPUP)
      .then((response) => {
        const slides = response.pageContent.map((r) => ({ src: r.bannerImage, url: r.bannerUrl }));
        this.setState({
          slides
        }, () => {
          this.setState({ isLoading: false });
        });
      })
      .catch((e) => console.error(e));
  }

  closeSplash = () => {
    const { dispatch } = this.props;
    dispatch(setSplashClose());
  }

  render() {
    const { isLoading, slides } = this.state;
    const { common } = this.props;
    const loadingIcon = <Icon type="loading" style={{ fontSize: 24 }} spin/>;

    const sliderSettings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      fade: true,
      arrows: false,
      initialSlide: common.splashInitialSlide,
      adaptiveHeight: true
    };

    const sliderList = slides.map(slide => (
      <div className="splash-slide-wrap">
        <a href={slide.url}>
          <img src={slide.src} alt=""/>
        </a>
      </div>
    ));

    return (
      <>
        {
          slides.length > 0 && (
            <div id="splash-slider">
              {
                isLoading
                  ? <Spin indicator={loadingIcon}/>
                  : (
                    <div className="slider-wrap">
                      <div className="close-slider">
                        <CloseIcon style={{ color: '#fff' }} onClick={this.closeSplash}/>
                      </div>
                      <Slider {...sliderSettings} ref={(c) => { this.sliderRef = c; }}>
                        { sliderList }
                      </Slider>
                    </div>
                  )
              }
            </div>
          )
        }
      </>
    );
  }
}

SplashSlider.propTypes = {
  common: PropTypes.objectOf(PropTypes.any).isRequired
};

export default connect(mapStateToProps)(SplashSlider);
