import React from 'react';
import PropTypes from 'prop-types';

import Styles from './videoCard.module.scss';

const VideoCard = ({ onClick, videoThumb, videoTitle }) => (
  <div onClick={onClick} className={Styles.wrapper}>
    <div className={Styles.contentWrapper}>
      <div className={Styles.content}>
        <div className={Styles.videoThumb}>
          <img src={videoThumb} alt=""/>
          <div className={Styles.overlay}>
            <img src="/img/play.png" alt=""/>
          </div>
        </div>
        <div className={Styles.videoTitle}>
          <h4>{videoTitle}</h4>
        </div>
      </div>
    </div>
  </div>
);

VideoCard.propTypes = {
  onClick: PropTypes.func.isRequired,
  videoThumb: PropTypes.string.isRequired,
  videoTitle: PropTypes.string.isRequired
};

export default VideoCard;
