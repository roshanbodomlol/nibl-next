import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'antd';
import Link from 'next/link';

import { withTranslation } from '../../locales/i18n';
import ContentCard from '../ContentCard';
import Styles from './generalBlocks.module.scss';

const GeneralBlocks = ({ blocks, t }) => {
  const blockList = blocks.map(block => block.description && (
    <Col sm={8} xs={24}>
      <div className={Styles.block}>
        <ContentCard>
          <h4>{block.title}</h4>
          <div dangerouslySetInnerHTML={{ __html: block.description }}/>
          {
            block.link && <Link href={block.link}><a>{t('global_readMore')}</a></Link>
          }
        </ContentCard>
      </div>
    </Col>
  ));

  return (
    <Row type="flex" gutter={36}>
      {blockList}
    </Row>
  );
};

GeneralBlocks.propTypes = {
  blocks: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default withTranslation('common')(GeneralBlocks);
