import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Row, Col } from 'antd';

import BigTitleBlock from '../Layouts/Block/BigTitleBlock';
import ImageWithOverlay from '../Layouts/Block/ImageBlockWithOverlay';
import Styles from './featuresBenefits.module.scss';

class FeaturesAndBenefits extends Component {
  featuresRef = React.createRef();

  benefitsRef = React.createRef();

  render() {
    const {
      firstTableTitle, secondTableTitle, firstTableContent, secondTableContent,
      featuredBlockImage, featuredBlockImageCaption, featuredBlockImageTitle, featuredBlockImageLink,
      benefitsBlockImage, benefitsBlockImageCaption, benefitsBlockImageTitle, benefitsBlockImageLink,
      featuredImageAlt, benefitsImageAlt
    } = this.props;

    return (
      <div>
        {
          firstTableContent && (
            <div id="key-features" className={Styles.featuresBenefits} ref={(c) => { this.featuresRef = c; }}>
              <Row gutter={30} type="flex">
                <Col lg={16} sm={24} xs={24}>
                  <BigTitleBlock title={firstTableTitle}>
                    <div className="wysiwyg" dangerouslySetInnerHTML={{ __html: firstTableContent }}/>
                  </BigTitleBlock>
                </Col>
                <Col lg={8} sm={24} xs={24}>
                  <ImageWithOverlay
                    title={featuredBlockImageTitle}
                    image={featuredBlockImage}
                    link={featuredBlockImageLink}
                    imageHeight={300}
                    alt={featuredImageAlt}
                  >
                    <div dangerouslySetInnerHTML={{ __html: featuredBlockImageCaption }}/>
                  </ImageWithOverlay>
                </Col>
              </Row>
            </div>
          )
        }

        {
          secondTableContent && (
            <div id="key-benefits" className={classnames(Styles.featuresBenefits, Styles.mobFeaturesBenefits)} ref={(c) => { this.benefitsRef = c; }}>
              <Row type="flex" gutter={30}>
                <Col lg={{ span: 8, order: 1 }} sm={{ span: 24, order: 2 }} xs={{ span: 24, order: 2 }}>
                  <ImageWithOverlay
                    title={benefitsBlockImageTitle}
                    image={benefitsBlockImage}
                    link={benefitsBlockImageLink}
                    imageHeight={300}
                    alt={benefitsImageAlt}
                  >
                    <div dangerouslySetInnerHTML={{ __html: benefitsBlockImageCaption }}/>
                  </ImageWithOverlay>
                </Col>
                <Col lg={{ span: 16, order: 2 }} sm={{ span: 24, order: 1 }} xs={{ span: 24, order: 1 }}>
                  <BigTitleBlock title={secondTableTitle}>
                    <div className="wysiwyg" dangerouslySetInnerHTML={{ __html: secondTableContent }}/>
                  </BigTitleBlock>
                </Col>
              </Row>
            </div>
          )
        }
      </div>
    );
  };
}

FeaturesAndBenefits.propTypes = {
  firstTableTitle: PropTypes.string.isRequired,
  secondTableTitle: PropTypes.string.isRequired,
  firstTableContent: PropTypes.string.isRequired,
  secondTableContent: PropTypes.string.isRequired
};

export default FeaturesAndBenefits;
