import Head from 'next/head';

import Header from '../__elements/Header';
// import Footer from '../__elements/Footer';

import '../../styles/global.scss';

const LayoutHeader = ({
  metaTitle,
  metaDescription,
  metaKeyword,
  pageTitle
}) => (
  <div>
    <Head>
      <title>NIBL | {metaTitle || pageTitle}</title>
      <meta name="description" content={metaDescription}/>
      <meta name="keyword" content={metaKeyword}/>
      <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
      <link rel="manifest" href="/manifest.json"/>
      {/* <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon" />
      <link rel="icon" href="/img/favicon.ico" type="image/x-icon" /> */}
      <link rel="stylesheet" href="/css/slick.css"/>
      <link rel="stylesheet" href="/css/bootstrap-reboot.css"/>
      <link rel="stylesheet" href="/css/fonts.css"/>
      <link rel="stylesheet" href="/css/animate.css"/>
      <link rel="stylesheet" href="/css/polyfill.object-fit.css"/>
      <link rel="stylesheet" href="/css/elements-override.css"/>
      <script src="/js/polyfill.object-fit.min.js"/>
    </Head>
    <Header/>
    {/* <Footer/> */}
  </div>
);

export default LayoutHeader;
