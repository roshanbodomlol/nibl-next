import Head from 'next/head';

// import Header from '../__elements/Header';
import Footer from '../__elements/Footer';

import '../../styles/global.scss';

const LayoutHeader = ({
  metaTitle,
  metaDescription,
  metaKeyword,
  pageTitle
}) => (
  <div>
    <Head>
      <title>NIBL | {metaTitle || pageTitle}</title>
      <meta name="description" content={metaDescription}/>
      <meta name="keyword" content={metaKeyword}/>
      <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
      <link rel="manifest" href="/manifest.json"/>
      {/* <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon" />
      <link rel="icon" href="/img/favicon.ico" type="image/x-icon" /> */}
      <link rel="stylesheet" href="/css/slick.css"/>
      <link rel="stylesheet" href="/css/bootstrap-reboot.css"/>
      <link rel="stylesheet" href="/css/fonts.css"/>
      <link rel="stylesheet" href="/css/animate.css"/>
      <link rel="stylesheet" href="/css/elements-override.css"/>
      <link rel="stylesheet" href="/css/polyfill.object-fit.css"/>
      <script src="/js/polyfill.object-fit.min.js"/>

      <script dangerouslySetInnerHTML={{ __html: `window.dataLayer = window.dataLayer || [];
        var chatbox = document.getElementById('fb-customer-chat');
        chatbox.setAttribute("page_id", "241835915895900");
        chatbox.setAttribute("attribution", "biz_inbox");
      ` }}/>
      <script dangerouslySetInnerHTML={{ __html: `window.dataLayer = window.dataLayer || [];
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v13.0'
          });
        };
  
        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
      ` }}/>
    </Head>
    {/* <Header/> */}
    <Footer/>
  </div>
);

export default LayoutHeader;
