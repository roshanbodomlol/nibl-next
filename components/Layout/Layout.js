import { Component } from 'react';
import Head from 'next/head';
import { connect } from 'react-redux';
import { Spin } from 'antd';

import Header from '../__elements/Header';
import Footer from '../__elements/Footer';
import FixedRightIcon from '../FixedRightIcon';
// import { setSplashOpen } from '../../redux/actions/common.actions';

import '../../styles/global.scss';
import SplashSlider from '../SplashSlider';

const mapStateToProps = ({ common, currentpage }) => ({ common, currentpage });

class Layout extends Component {
  componentDidMount() {
    const { common, dispatch } = this.props;
    if (common.siteLoading) {
      const body = document.querySelector('body');
      body.classList.add('modal-open');
    }

    // splash token
    // const splashToken = localStorage.getItem('NIBL_SPLASH');
    // if (!splashToken) {
    //   localStorage.setItem('NIBL_SPLASH', 1);
    //   dispatch(setSplashOpen(0));
    // }
  }

  componentDidUpdate(prevProps) {
    const { common } = this.props;

    if (common.siteLoading !== prevProps.common.siteLoading) {
      this.toggleModalOpenClass();
    }
  }

  toggleModalOpenClass = () => {
    const { common } = this.props;

    const body = document.querySelector('body');
    if (common.siteLoading) {
      body.classList.add('modal-open');
    } else {
      body.classList.remove('modal-open');
    }
  }

  render() {
    const {
      children,
      metaTitle,
      metaDescription,
      metaKeyword,
      pageTitle,
      common,
      ogImage
    } = this.props;

    return (
      <div id="wrapper" className="asdf">
        <Head>
          <title>NIBL | {metaTitle || pageTitle}</title>
          <meta name="description" content={metaDescription}/>
          <meta name="keyword" content={metaKeyword}/>
          <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
          <meta name="theme-color" content="#003E92"/>
          <link rel="manifest" href="/manifest.json"/>
          {/* <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon" />
          <link rel="icon" href="/img/favicon.ico" type="image/x-icon" /> */}
          <link rel="stylesheet" href="/css/slick.css"/>
          <link rel="stylesheet" href="/css/bootstrap-reboot.css"/>
          <link rel="stylesheet" href="/css/fonts.css"/>
          <link rel="stylesheet" href="/css/animate.css"/>
          <link rel="stylesheet" href="/css/polyfill.object-fit.css"/>
          {/* <script src="/js/polyfill.object-fit.min.js"/>
          <script src="/js/scripts.js"/> */}
          {/* <script
            type="text/javascript"
            dangerouslySetInnerHTML={{ __html: `var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
              (function(){
              var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
              s1.async=true;
              s1.src='https://embed.tawk.to/5e8ea0f035bcbb0c9aaf4c9e/default';
              s1.charset='UTF-8';
              s1.setAttribute('crossorigin','*');
              s0.parentNode.insertBefore(s1,s0);
              })();` }}
          /> */}
          <meta property="og:title" content={metaTitle || pageTitle} />
          <meta property="og:description" content={metaDescription} />
          {
            ogImage && <meta property="og:image" content={ogImage} />
          }

          <script async src="https://www.googletagmanager.com/gtag/js?id=UA-177464146-2"/>
          <script dangerouslySetInnerHTML={{ __html: `window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-177464146-2');` }}/>
          <script src="https://www.google.com/recaptcha/api.js?render=explicit" async defer/>

        </Head>
        <Header/>
        <FixedRightIcon/>
        {
          common.splashOpen && <SplashSlider/>
        }

        <div id="fb-root"/>
        <div id="fb-customer-chat" class="fb-customerchat"/>

        <div id="content-wrappper">
          {
            common.contentLoading
              ? (
                <div className="content-loading">
                  <Spin/>
                </div>
              )
              : children
          }
        </div>
        <Footer/>
      </div>
    );
  }
}

export default connect(mapStateToProps)(Layout);
