import Layout from './Layout';
import LayoutHeader from './LayoutHeader';
import LayoutFooter from './LayoutFooter';

export default Layout;
export {
  LayoutHeader,
  LayoutFooter
};
