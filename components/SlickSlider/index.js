import React from 'react';

import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';
import Slider from 'react-slick';

const SlickSlider = ({ settings, children }) => {
  return (
    // <Carousel
    //   autoPlay
    //   infiniteLoop
    //   interval={3000}
    //   transitionTime={700}
    //   showArrows={false}
    //   showIndicators={settings.dots}
    //   showThumbs={false}
    // >
    //   {children}
    // </Carousel>
    <Slider {...settings}>
      {children}
    </Slider>
  );
};

export default SlickSlider;
