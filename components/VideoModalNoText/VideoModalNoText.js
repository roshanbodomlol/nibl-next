import { Component } from 'react';
import PropTypes from 'prop-types';
import ModalVideo from 'react-modal-video';

import VideoCardNoText from '../VideoCardNoText';

class VideoModalNoText extends Component {
  constructor() {
    super();
    this.state = {
      isOpen: false
    };
    this.openModal = this.openModal.bind(this);
  }

  openModal = () => {
    this.setState({ isOpen: true });
  }

  render() {
    const { isOpen } = this.state;
    const { videoId, videoThumb } = this.props;
    return (
      <>
        <ModalVideo channel="youtube" isOpen={isOpen} videoId={videoId} onClose={() => this.setState({ isOpen: false })} />
        <VideoCardNoText
          onClick={this.openModal}
          videoThumb={videoThumb}
        />
      </>
    );
  }
}

VideoModalNoText.propTypes = {
  videoId: PropTypes.string.isRequired,
  videoThumb: PropTypes.string.isRequired
};

export default VideoModalNoText;
