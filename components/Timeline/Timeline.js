import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Styles from './timeline.module.scss';

const Timeline = ({ items, onClick, activeItemKey }) => {
  const itemList = items.map((item) => {
    const classes = classnames({
      active: activeItemKey === item.year
    });
    return (
      <li
        key={`timeline-item-${item}`}
        onClick={() => { onClick(item.year); }}
        role="button"
        className={classes}
      >
        <span className={Styles.dot}></span>
        <span className={Styles.date}>{item.year}</span>
      </li>
    );
  });
  return (
    <div className={Styles.timelineList}>
      <ul>
        {itemList}
      </ul>
      <div className={Styles.border}></div>
    </div>
  );
};

Timeline.propTypes = {
  items: PropTypes.arrayOf(PropTypes.number).isRequired,
  onClick: PropTypes.func.isRequired,
  activeItemKey: PropTypes.number.isRequired
};

export default Timeline;
