import React from 'react';

import SavingCalculator from '../SavingCalculator';

const EmiCalcBlock = () => (
  <SavingCalculator enterCount={20} compact/>
);

export default EmiCalcBlock;
