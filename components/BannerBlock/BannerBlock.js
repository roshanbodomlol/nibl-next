import React from 'react';
import classnames from 'classnames';
import { string, node } from 'prop-types';

import Styles from './styles.module.scss';

/**
 * @visibleName Banner Block
 */

const BannerBlock = ({
  background, title, description, link, linkTitle, children, alt
}) => (
  <>
    {
      !!background && (
        <div className={Styles.wrapper}>
          <img src={background} alt={alt || title}/>
          <div className="big-container">
            <div className={classnames(Styles.textWrapper, 'banner-text-wrapper')}>
              <div className={Styles.heading}>
                <h1>{title}</h1>
              </div>
              <div className={Styles.text}>
                <p>
                  {description}
                </p>
              </div>
              {link && <a className={Styles.applyButton} href={link}>{linkTitle}</a>}
              {children}
            </div>
          </div>
        </div>
      )
    }
  </>
);

BannerBlock.propTypes = {
  background: string.isRequired,
  title: string.isRequired,
  description: string,
  link: string,
  linkTitle: string,
  children: node,
  alt: string
};

BannerBlock.defaultProps = {
  description: '',
  link: '',
  linkTitle: 'Apply Now',
  children: '',
  alt: false
};

export default BannerBlock;
