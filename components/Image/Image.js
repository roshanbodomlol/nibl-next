import React from 'react';
import classnames from 'classnames';

import './Image.scss';

const Image = ({
  src, height, alt
}) => {
  const classes = classnames('image-wrapper');
  return (
    <div className={classes}>
      <img src={src} alt={alt}/>
      {/* <div className="img" style={{ backgroundImage: `url(${src})`, minHeight: height }}/> */}
    </div>
  );
};

export default Image;
