import { Component } from 'react';
import PropTypes from 'prop-types';
import { MdExpandMore as ExpandMore, MdExpandLess as ExpandLess } from 'react-icons/md';
import classnames from 'classnames';
import { isEqual } from 'lodash';
import Styles from './expansionPanel.module.scss';

/**
 * @visibleName Expansion Panel
 */

class ExpansionPanel extends Component {
  state = {
    activePanel: null
  }

  componentDidUpdate(prevProps) {
    if (prevProps.items.length !== this.props.items.length) {
      this.setState({ activePanel: null });
    }
  }

  handlePanelClick = (index) => {
    this.setState((prevState) => {
      if (prevState.activePanel === index) {
        return {
          activePanel: null
        };
      }
      return {
        activePanel: index
      };
    });
  }

  render() {
    const { isOrdered, items, compact } = this.props;
    const { activePanel } = this.state;
    const panels = items.map((item, index) => {
      const panelClasses = classnames(Styles.panel, '_panel', {
        active: activePanel === index,
        compact
      });
      return (
        <div
          key={`ex-pnl-${index}`}
          className={panelClasses}
          role="button"
          tabIndex="-1"
          onClick={() => { this.handlePanelClick(index); }}
        >
          <div className={Styles.row}>
            {
              isOrdered && (
                <div className={Styles.number}>
                  {index + 1}
                </div>
              )
            }
            <div className={Styles.primary}>
              {item.primary}
            </div>
            <div className={classnames(Styles.arrow, 'expansion-arrow')}>
              {
                activePanel === index
                  ? <ExpandLess style={{ color: '#CC2128' }}/>
                  : <ExpandMore style={{ color: '#CC2128' }}/>
              }
            </div>
          </div>
          <div className={Styles.expandedPanel}>
            {item.secondary}
          </div>
        </div>
      );
    });
    return (
      <div className={Styles.wrapper}>
        {panels}
      </div>
    );
  }
}

ExpansionPanel.propTypes = {
  isOrdered: PropTypes.bool,
  items: PropTypes.arrayOf(PropTypes.any).isRequired,
  compact: PropTypes.bool
};

ExpansionPanel.defaultProps = {
  isOrdered: false,
  compact: false
};

export default ExpansionPanel;
