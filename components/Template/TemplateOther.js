import { connect } from 'react-redux';

import { TEMPLATES } from '../../constants';

const mapStateToProps = ({ navigation }) => ({ navigation });

const TemplateOther = (props) => {
  const { pageDetails } = props;
  const { templateName } = pageDetails;
  const Component = TEMPLATES[templateName];

  if (!Component) {
    return (
      <div>
        <h1>404 - Not Found</h1>
      </div>
    );
  }

  return (
    <Component {...props}/>
  );
};

export default connect(mapStateToProps)(TemplateOther);
