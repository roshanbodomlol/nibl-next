import { connect } from 'react-redux';
import { useRouter } from 'next/router';

import { TEMPLATES } from '../../constants';
import { findPage } from '../../utilities/currentpage.util';

const mapStateToProps = ({ navigation }) => ({ navigation });

const Template = (props) => {
  const { navigation, staticTemplate } = props;
  const { query } = useRouter();

  if (staticTemplate) {
    const Component = TEMPLATES[staticTemplate];
    return (
      <Component/>
    );
  }

  const page = findPage(navigation, query);
  const Component = TEMPLATES[page.pageTemplate];

  if (!Component) {
    return (
      <div>
        <h1>404 - Not Found</h1>
      </div>
    );
  }

  return (
    <Component {...props}/>
  );
};

export default connect(mapStateToProps)(Template);
