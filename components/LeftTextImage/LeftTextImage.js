import { Row, Col } from 'antd';
import PropTypes from 'prop-types';

import Styles from './leftTextImage.module.scss';

const LeftTextImage = ({ title, image, children }) => (
  <div className={Styles.wrapper}>
    <div className={Styles.section}>
      <Row type="flex" gutter={30}>
        <Col lg={16} sm={24}>
          <div className={Styles.leftSection}>
            <span className={Styles.blockHeading}>{title}</span>
            {children}
          </div>
        </Col>
        <Col lg={8} sm={24}>
          <div className={Styles.rightSection}>
            <img src={image} alt=""/>
          </div>
        </Col>
      </Row>
    </div>
  </div>
);

LeftTextImage.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  image: PropTypes.string.isRequired
};

export default LeftTextImage;
