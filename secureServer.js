const https = require('https');
const http = require('http');
const express = require('express');
const next = require('next');
const { parse } = require('url');
const { join } = require('path');
const nextI18NextMiddleware = require('next-i18next/middleware').default;
const helmet = require('helmet');
const { readFileSync } = require('fs');
const path = require('path');
const compression = require('compression');
require('dotenv').config();

const nextI18next = require('./locales/i18n');

const httpsOptions = {
   key: readFileSync('./certificates/private.key'),
   cert: readFileSync('./certificates/certificate.crt'),
  //pfx: readFileSync('./certificates/certificate.pfx'),
  //passphrase: 'changeit'
};

const app = next({ dev: process.env.NODE_ENV !== 'production' });
const handle = app.getRequestHandler();

const httpRedirectServer = express();

// set up a route to redirect http to https
httpRedirectServer.get('*', (request, response) => {
  response.redirect(`https://${request.headers.host}${request.url}`);
});

http.createServer(httpRedirectServer)
  .listen(80, err => {
    if (err) throw err;
    console.log('Listening to redirect http to https');
  });

app
  .prepare()
  .then(async () => {
    const server = express();
    // HELMET MIDDLEWARE
    server.use(helmet());

    await nextI18next.initPromise;
    server.use(nextI18NextMiddleware(nextI18next));
    server.use(compression());

    server.use('/public', express.static(path.join(__dirname, 'public')));

    server.get('/service-worker.js', (req, res) => {
      const filePath = join(__dirname, '.next', 'service-worker.js');
      res.sendFile(filePath);
    });

    server.get('*', (req, res) => {
      const parsedUrl = parse(req.url, true);

      return handle(req, res, parsedUrl);
    });
    https.createServer(httpsOptions, server).listen(443, err => {
      if (err) throw err;
      console.log('> Ready on https://localhost');
    });
  });
