const express = require('express');
const next = require('next');
const { parse } = require('url');
const { join } = require('path');
const nextI18NextMiddleware = require('next-i18next/middleware').default;
const helmet = require('helmet');
const path = require('path');
const compression = require('compression');
require('dotenv').config();

const nextI18next = require('./locales/i18n');

const port = process.env.PORT || 3000;
const app = next({ dev: process.env.NODE_ENV !== 'production' });
const handle = app.getRequestHandler();

app
  .prepare()
  .then(async () => {
    const server = express();

    // HELMET MIDDLEWARE
    server.use(helmet());
    server.use(compression());

    await nextI18next.initPromise;
    server.use(nextI18NextMiddleware(nextI18next));

    server.use('/public', express.static(path.join(__dirname, 'public')));

    server.get('/service-worker.js', (req, res) => {
      const filePath = join(__dirname, '.next', 'service-worker.js');
      res.sendFile(filePath);
    });

    server.get('*', (req, res) => {
      const parsedUrl = parse(req.url, true);

      return handle(req, res, parsedUrl);
    });

    await server.listen(port);
    console.log(`> Ready on http://localhost:${port}`); // eslint-disable-line no-console
  });
