import Overview from '../components/__templates/Overview';
import About from '../components/__templates/About';
import Team from '../components/__templates/BoardOfDirectors';
import DailyRates from '../components/__templates/DailyRates';
import InterestRates from '../components/__templates/InterestRates';
import Downloads from '../components/__templates/Downloads';
import Location from '../components/__templates/Location';
import BranchLocation from '../components/__templates/BranchLocation';
import MobileBanking from '../components/__templates/MobileBanking';
import Locker from '../components/__templates/Lockers';
import BranchlessBanking from '../components/__templates/BranchlessBanking';
import FullWidthContentCard from '../components/__templates/FullWidthContentCard';
import PrithiviRemitance from '../components/__templates/PrithiviRemit';
import DomesticRemitance from '../components/__templates/DomesticRemit';
import ContactForm from '../components/__templates/SupportContact';
import InternationalNetworks from '../components/__templates/InternationalNetwork';
import SupportFaq from '../components/__templates/SupportFaq';
import ManagementTeam from '../components/__templates/ManagementTeam';
import Compliance from '../components/__templates/Compliance';
import Stock from '../components/__templates/Stock';
import Reports from '../components/__templates/Reports';
import FinancialDownloads from '../components/__templates/FinancialDownloads';
import NewsAndContent from '../components/__templates/NewsAndContent';
import CorrespondentBank from '../components/__templates/CorrespondentBank';
import Certification from '../components/__templates/Certification';
import BlockListing from '../components/__templates/BlockListing';
import Payments from '../components/__templates/Payments';
import CalculatorOverBg from '../components/__templates/CalculatorOverBg';
import Tutorials from '../components/__templates/Tutorials';
import Career from '../components/__templates/Career';
import TeamImages from '../components/__templates/TeamImages';
import Research from '../components/__templates/Research';
import GeneralEnquiry from '../components/__templates/GeneralEnquiry';
import Disclosure from '../components/__templates/Disclosure';
import InformationOffice from '../components/__templates/InformationOffice';
import CorporateOffice from '../components/__templates/CorporateOffice';
import Security from '../components/__templates/Security';
import ManagerDetails from '../components/__templates/ManagerDetails';
import Banking from '../components/__templates/365DaysBanking';
import CEOTeam from '../components/__templates/CEOTeam';
// import Shareholder from '../components/__templates/Shareholder';
// import Services from '../components/__templates/Services';
// import BaselDisclosure from '../components/__templates/BaselDisclosure';
// import Corporate from '../components/__templates/CorporateLanding';
// import LostStolenCards from '../components/__templates/LostStolenCards';
// import Strategy from '../components/__templates/Strategy';

export default {
  overview: Overview,
  about: About,
  team: Team,
  dailyRates: DailyRates,
  interestRates: InterestRates,
  downloads: Downloads,
  location: Location,
  branchLocation: BranchLocation,
  mobileBanking: MobileBanking,
  locker: Locker,
  branchlessBanking: BranchlessBanking,
  fullWidthContentCard: FullWidthContentCard,
  prithiviRemitance: PrithiviRemitance,
  domesticRemitance: DomesticRemitance,
  contactForm: ContactForm,
  internationalNetworks: InternationalNetworks,
  supportFaq: SupportFaq,
  managementTeam: ManagementTeam,
  compliance: Compliance,
  stocks: Stock,
  reports: Reports,
  financialDownloads: FinancialDownloads,
  newsAndContent: NewsAndContent,
  correspondentBank: CorrespondentBank,
  certification: Certification,
  blockListing: BlockListing,
  payments: Payments,
  calculatorOverBg: CalculatorOverBg,
  videos: Tutorials,
  career: Career,
  teamImages: TeamImages,
  research: Research,
  generalEnquiry: GeneralEnquiry,
  disclosure: Disclosure,
  informationOffice: InformationOffice,
  corporateOffice: CorporateOffice,
  fullWidthTabContentCard: Security,
  branchTeam: ManagerDetails,
  bankingHours: Banking,
  ceoTeam: CEOTeam
  // services: Services,
  // shareholder: Shareholder,
  // corporate: Corporate,
  // lostStolenCards: LostStolenCards,
  // strategy: Strategy,
};
