export default {
  endPoints: {
    PAGE_ABOUT: 'About',
    MENU: 'Menu',
    ATM: 'ATM/:hasPageContent/:menuId',
    BRANCH: 'branch/:hasPageContent/:menuId',
    LOCKER: 'Locker/:hasPageContent/:menuId',
    PRODUCT: 'Product/:menuId',
    PRODUCT_LIST: 'FeaturedProduct/:isFeatured/:limit',
    MOBILE_BANKING: 'MobileBanking/:hasPageContent/:menuId',
    ABOUT_OVERVIEW: 'overview/:menuId',
    ABOUT_BOD: 'bodmembers/:menuId',
    ABOUT_TEAM: 'teammembers/:menuId',
    ABOUT_COMPLIANCE: 'compliance/:menuId',
    ABOUT_CORRESPONDENT: 'correspondentbank/:menuId',
    ABOUT_CERTIFICATION: 'certification/:menuId',
    ABOUT_DISCLOSURE: 'disclosure/:menuId',
    FAQ: 'FAQ/:hasPageContent/:menuId',
    ABOUT_DOWNLOADS: 'Download/:menuId',
    NEWS: 'news/:menuId',
    NEWS_SINGLE: 'newsbyid/:id',
    NEWS_FEATURED: 'featurednews/:limit/:pageId',
    NEWS_LATEST: 'latestnews/:limit',
    VIDEOS: 'videos/:menuId',
    GENERAL: 'general/:hasPageContent/:menuId',
    REMITTANCE_PRITHIVI: 'prithiviremittance/:menuId',
    REMITTANCE_DOMESTIC: 'domesticremittance/:menuId',
    REMITTANCE_INTERNATIONAL: 'internationalnetwork/:menuId',
    FOREX_ALL: 'getallforex',
    FOREX_PAGE: 'forexbypageid/:menuId',
    FOREX_DATE_CURRENT: 'getcurrentforex',
    FOREX_DATE: 'getforexbydate/:date',
    RATES: 'ratefees/:menuId',
    HOME_SLIDER: 'homeslider',
    RESEARCH: 'research/:menuId',
    FINANCIAL_DOWNLOADS: 'financialdownload/:menuId',
    REPORTS: 'report/:menuId',
    CAREER: 'career/:menuId',
    VACANCY: 'vacancy/:vacancyId',
    HOME_BANNER: 'homebanner',
    FEATURED_SERVICES: 'featuredservice/:isFeatured/:limit',
    SUBSCRIBE: 'subscribe',
    GENERAL_ENQUIRY: 'generalenquiry/:menuId',
    INFORMATION_OFFICE: 'informationoffice/:menuId',
    QUICK_LINKS: 'quicklinks',
    CORPORATE_OFFICE: 'corporateoffice/:menuId',
    POPUP: 'homepopup',
    NEPSE: 'getnepse',
    CAREER_FORM: 'application',
    GENERAL_TABS: 'generaltabs/:menuId',
    PAYMENTS: 'payments/:menuId',
    MANAGER_DETAILS: 'branchmembers/:menuId',
    OTHER_PAGE: 'singlepage/:slug',
    BANKING_365: 'bankingHours/:menuId',
    TOOLBAR_LINKS: 'toolbarlinks',
    FOOTER_INFO: 'footersection',
    CEO_TEAM: 'ceomembers/:menuId',
    SEND_MAIL: 'sendemail',
    FOREX_SUBSCRIBE: 'forexsubscriber',
    SEARCH: 'search/:keyword',
    SUBSCRIPTION_VERIFY: 'activatesubscription/:token',
    FOREX_HOME: 'getcurrentforexhome',
    SOCIAL_LINKS: 'socialmedia'
    // FOREX_RATES: 'https://v2.api.forex/rates/latest.json?beautify=true&key=f94a8f9f-79e0-4c7e-8390-2e8306cd510c'
  }
};
