const USDFlag = '/svg/flags/USD.svg';
const AUDFlag = '/svg/flags/AUD.svg';
const EURFlag = '/svg/flags/EUR.svg';
const GBPFlag = '/svg/flags/GBP.svg';
const SGDFlag = '/svg/flags/SGD.svg';
const CNYFlag = '/svg/flags/CNY.svg';
const HKDFlag = '/svg/flags/HKD.svg';
const CADFlag = '/svg/flags/CAD.svg';
const JPYFlag = '/svg/flags/JPY.svg';
const CHFFlag = '/svg/flags/CHF.svg';

export default {
  loginURL: 'http://pagodalabs.com.np/nibllogin',
  LANG_KEY: '__nibl__lang__',
  SCROLL_ANCHOR_THRESHOLD: 700,
  SCROLL_ANCHOR_DEBOUNCE: 1000,
  FOREX_CURRENCIES: {
    USD: USDFlag,
    AUD: AUDFlag,
    EUR: EURFlag,
    GBP: GBPFlag,
    SGD: SGDFlag,
    CNY: CNYFlag,
    HKD: HKDFlag,
    CAD: CADFlag,
    JPY: JPYFlag,
    CHF: CHFFlag
  },
  TEMPLATES_TO_APIKEY: {
    overview: 'PRODUCT',
    about: 'ABOUT_OVERVIEW',
    team: 'ABOUT_BOD',
    managementTeam: 'ABOUT_TEAM',
    teamImages: 'ABOUT_BOD',
    dailyRates: 'FOREX_PAGE',
    interestRates: 'RATES',
    downloads: 'ABOUT_DOWNLOADS',
    location: 'ATM',
    branchLocation: 'BRANCH',
    mobileBanking: 'MOBILE_BANKING',
    locker: 'LOCKER',
    fullWidthContentCard: 'GENERAL',
    prithiviRemitance: 'REMITTANCE_PRITHIVI',
    domesticRemitance: 'REMITTANCE_DOMESTIC',
    internationalNetworks: 'REMITTANCE_INTERNATIONAL',
    supportFaq: 'FAQ',
    compliance: 'ABOUT_COMPLIANCE',
    reports: 'REPORTS',
    disclosure: 'ABOUT_DISCLOSURE',
    newsAndContent: 'NEWS',
    correspondentBank: 'ABOUT_CORRESPONDENT',
    certification: 'ABOUT_CERTIFICATION',
    research: 'RESEARCH',
    financialDownloads: 'FINANCIAL_DOWNLOADS',
    career: 'CAREER',
    videos: 'VIDEOS',
    generalEnquiry: 'GENERAL_ENQUIRY',
    informationOffice: 'INFORMATION_OFFICE',
    corporateOffice: 'CORPORATE_OFFICE',
    fullWidthTabContentCard: 'GENERAL_TABS',
    payments: 'PAYMENTS',
    branchTeam: 'MANAGER_DETAILS',
    bankingHours: 'BANKING_365',
    ceoTeam: 'CEO_TEAM'
  },
  STATIC_TEMPLATES: [
    'branchlessBanking',
    'contactForm',
    'stocks',
    'blockListing',
    'calculatorOverBg',
    'quickLinks'
  ],
  CALCULATOR: {
    AMOUNT_MIN: 333,
    AMOUNT_MAX: 1000000,
    TIME_MIN_MONTHS: 1,
    TIME_MAX_MONTHS: 360,
    TIME_MIN_YEARS: 1,
    TIME_MAX_YEARS: 30,
    INTEREST_MIN: 1,
    INTEREST_MAX: 30,
    DEFAULT_TIME_UNIT: 'm'
  }
};
