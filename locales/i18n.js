const NextI18Next = require('next-i18next').default;

// const NextI18NextInstance = new NextI18Next({
//   defaultLanguage: 'en',
//   otherLanguages: ['ne']
// });

// // export default NextI18NextInstance;

// /* Optionally, export class methods as named exports */
// const {
//   appWithTranslation,
//   withTranslation
// } = NextI18NextInstance;

// module.exports = {
//   appWithTranslation,
//   withTranslation
// };

module.exports = new NextI18Next({
  defaultLanguage: 'en',
  otherLanguages: ['ne']
});
