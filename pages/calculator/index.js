import Layout from '../../components/Layout';
import Calculator from '../../components/__templates/CalculatorOverBg';
import { get } from '../../services/api.services';
import makeSlugs from '../../navigation/makeRouteSlugs';
import { setCurrentpage } from '../../redux/actions/currentpage.actions';
import { setNavigation } from '../../redux/actions/navigation.actions';
import { setSiteLoading, setContentLoading, setLogos } from '../../redux/actions/common.actions';
import { API } from '../../constants';

const CalculatorPage = () => (
  <Layout metaTitle="Nepal Investment Bank Ltd.">
    <Calculator/>
  </Layout>
);

CalculatorPage.getInitialProps = async ({ isServer, store }) => {
  let routes = null;
  const rawRoutes = await get(API.endPoints.MENU);
  const logos = rawRoutes.logo;
  store.dispatch(setLogos(logos));
  routes = makeSlugs(rawRoutes);
  store.dispatch(setCurrentpage(routes.primary[0]));
  store.dispatch(setNavigation(routes));
  store.dispatch(setSiteLoading(false));

  store.dispatch(setContentLoading(false));
  return {};
};

export default CalculatorPage;
