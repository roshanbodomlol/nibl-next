import { setContentLoading } from '../../redux/actions/common.actions';
import { initializePage } from '../../navigation/initializePage';
import Layout from '../../components/Layout';
import Template from '../../components/Template';

const Page = ({ pageProps }) => {
  // check for static template
  if (pageProps.staticTemplate) {
    return (
      <Layout staticTemplate={pageProps.staticTemplate}>
        <Template staticTemplate={pageProps.staticTemplate}/>
      </Layout>
    );
  }

  const layoutProps = pageProps.data[0] ? { ...pageProps.data[0].pageDetails } : { ...pageProps.data.pageDetails };
  const templateProps = pageProps.data[0] ? { ...pageProps.data[0] } : { ...pageProps.data };
  return (
    <Layout {...layoutProps}>
      <Template {...templateProps}/>
    </Layout>
  );
};

Page.getInitialProps = async (ctx) => {
  const { isServer, store } = ctx;
  if (!isServer) {
    store.dispatch(setContentLoading(true));
  }
  return initializePage(ctx);
};

export default Page;
