import Layout from '../../../components/Layout';
import Application from '../../../components/__templates/Career/Application';
import makeSlugs from '../../../navigation/makeRouteSlugs';
import { get } from '../../../services/api.services';
import { API } from '../../../constants';
import { setContentLoading, setSiteLoading } from '../../../redux/actions/common.actions';
import { setCurrentpage } from '../../../redux/actions/currentpage.actions';
import { setNavigation } from '../../../redux/actions/navigation.actions';
import { generatePath } from '../../../utilities/url.utils';

const ApplicationPage = ({ pageProps }) => (
  <Layout metaTitle="Nepal Investment Bank Ltd.">
    <Application {...pageProps.data}/>
  </Layout>
);

ApplicationPage.getInitialProps = async ({ isServer, store, query }) => {
  let routes = null;
  const rawRoutes = await get(API.endPoints.MENU);
  routes = makeSlugs(rawRoutes);
  store.dispatch(setCurrentpage(routes.primary[0]));
  store.dispatch(setNavigation(routes));
  store.dispatch(setSiteLoading(false));

  const url = generatePath(
    API.endPoints.VACANCY, { vacancyId: query.id }
  );

  const data = await get(url);

  store.dispatch(setContentLoading(false));
  return {
    data
  };
};

export default ApplicationPage;
