import Layout from '../../components/Layout';
import Career from '../../components/__templates/Career';
import makeSlugs from '../../navigation/makeRouteSlugs';
import { get } from '../../services/api.services';
import { API } from '../../constants';
import { setContentLoading, setSiteLoading, setLogos } from '../../redux/actions/common.actions';
import { setCurrentpage } from '../../redux/actions/currentpage.actions';
import { setNavigation } from '../../redux/actions/navigation.actions';

const CareerPage = ({ pageProps }) => (
  <Layout metaTitle="Nepal Investment Bank Ltd.">
    <Career {...pageProps.data}/>
  </Layout>
);

CareerPage.getInitialProps = async ({ isServer, store }) => {
  let routes = null;
  const rawRoutes = await get(API.endPoints.MENU);
  const logos = rawRoutes.logo;
  store.dispatch(setLogos(logos));
  routes = makeSlugs(rawRoutes);
  store.dispatch(setCurrentpage(routes.primary[0]));
  store.dispatch(setNavigation(routes));
  store.dispatch(setSiteLoading(false));

  store.dispatch(setContentLoading(false));
  return {};
};

export default CareerPage;
