import Layout from '../../../components/Layout';
import NewsDetail from '../../../components/__templates/DetailPage';
import makeSlugs from '../../../navigation/makeRouteSlugs';
import { get } from '../../../services/api.services';
import { API } from '../../../constants';
import { setContentLoading, setSiteLoading, setLogos } from '../../../redux/actions/common.actions';
import { setCurrentpage } from '../../../redux/actions/currentpage.actions';
import { setNavigation } from '../../../redux/actions/navigation.actions';
import { generatePath } from '../../../utilities/url.utils';

const ChildPage = ({ pageProps }) => (
  <Layout
    metaTitle={pageProps.data.pageContent[0].newsTitle}
    metaDescription={pageProps.data.pageContent[0].newsDescription}
  >
    <NewsDetail {...pageProps.data}/>
  </Layout>
);

ChildPage.getInitialProps = async ({ isServer, store, query }) => {
  let routes = null;
  const rawRoutes = await get(API.endPoints.MENU);
  const logos = rawRoutes.logo;
  store.dispatch(setLogos(logos));
  routes = makeSlugs(rawRoutes);
  store.dispatch(setCurrentpage(routes.primary[0]));
  store.dispatch(setNavigation(routes));
  store.dispatch(setSiteLoading(false));

  const url = generatePath(
    API.endPoints.NEWS_SINGLE, { id: query.id }
  );
  const data = await get(url);
  store.dispatch(setContentLoading(false));
  return {
    data
  };
};

export default ChildPage;
