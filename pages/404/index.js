import Link from 'next/link';

import Layout from '../../components/Layout';
import { get } from '../../services/api.services';
import { API } from '../../constants';
import makeSlugs from '../../navigation/makeRouteSlugs';
import { setCurrentpage } from '../../redux/actions/currentpage.actions';
import { setNavigation } from '../../redux/actions/navigation.actions';
import { setSiteLoading, setContentLoading, setLogos } from '../../redux/actions/common.actions';
import './NotFound.scss';

const NotFound = () => (
  <Layout metaTitle="Nepal Investment Bank Ltd.">
    <div className="white-gradient main--content">
      <div className="container">
        <div className="not-found">
          <h1>Not Found</h1>
          <p>The link you requested does not exist</p>
          <Link href="/">
            <a>Home</a>
          </Link>
        </div>
      </div>
    </div>
  </Layout>
);

// NotFound.getInitialProps = async ({ isServer, store }) => {
//   let routes = null;
//   if (isServer) {
//     const rawRoutes = await get(API.endPoints.MENU);
//     const logos = rawRoutes.logo;
//     store.dispatch(setLogos(logos));
//     routes = makeSlugs(rawRoutes);
//     store.dispatch(setCurrentpage(routes.primary[0]));
//     store.dispatch(setNavigation(routes));
//     store.dispatch(setSiteLoading(false));
//   } else {
//     routes = store.getState().navigation;
//   }

//   store.dispatch(setContentLoading(false));
//   return {};
// };

export default NotFound;
