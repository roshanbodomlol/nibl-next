import { LayoutHeader } from '../../../components/Layout';
import { get } from '../../../services/api.services';
import makeSlugs from '../../../navigation/makeRouteSlugs';
import { setCurrentpage } from '../../../redux/actions/currentpage.actions';
import { setNavigation } from '../../../redux/actions/navigation.actions';
import { setSiteLoading, setContentLoading, setLogos } from '../../../redux/actions/common.actions';
import { API } from '../../../constants';

const HeaderFrame = () => (
  <LayoutHeader metaTitle="Nepal Investment Bank Ltd."/>
);

HeaderFrame.getInitialProps = async ({ isServer, store }) => {
  let routes = null;
  const rawRoutes = await get(API.endPoints.MENU);
  const logos = rawRoutes.logo;
  store.dispatch(setLogos(logos));
  routes = makeSlugs(rawRoutes);
  store.dispatch(setCurrentpage(routes.primary[0]));
  store.dispatch(setNavigation(routes));
  store.dispatch(setSiteLoading(false));

  store.dispatch(setContentLoading(false));
  return {};
};

export default HeaderFrame;
