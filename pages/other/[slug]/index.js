import { setContentLoading } from '../../../redux/actions/common.actions';
import { initializePageOther } from '../../../navigation/initializePageOther';
import Layout from '../../../components/Layout';
import { TemplateOther } from '../../../components/Template';

const PageOther = ({ pageProps }) => {
  const layoutProps = { ...pageProps.data.pageDetails };
  const templateProps = { ...pageProps.data };
  return (
    <Layout {...layoutProps}>
      <TemplateOther {...templateProps}/>
    </Layout>
  );
};

PageOther.getInitialProps = async (ctx) => {
  const { isServer, store } = ctx;
  if (!isServer) {
    store.dispatch(setContentLoading(true));
  }
  return initializePageOther(ctx);
};

export default PageOther;
