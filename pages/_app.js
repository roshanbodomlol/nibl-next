import App from 'next/app';
import withRedux from 'next-redux-wrapper';
import { Provider } from 'react-redux';
import Router from 'next/router';

import { i18n, appWithTranslation } from '../locales/i18n';
import 'antd/dist/antd.css';
import myStore from '../redux/store';
import { GLOBALS } from '../constants';

class RootApp extends App {
  componentDidMount() {
    // check for Navigation Timing API support
    if (performance?.navigation?.type === performance?.navigation?.TYPE_RELOAD) {
      const localLocale = localStorage.getItem(GLOBALS.LANG_KEY);
      i18n.changeLanguage(localLocale);
      Router.replace(Router.asPath);
    }

    // register();
    // if ('serviceWorker' in navigator) {
    //   navigator.serviceWorker
    //     .register('/service-worker.js')
    //     .then(registration => {
    //       // console.log('service worker registration successful');
    //     })
    //     .catch(err => {
    //       console.warn('service worker registration failed', err.message);
    //     });
    // }
    // if ('serviceWorker' in navigator) {
    //   window.addEventListener('load', function() {
    //     navigator.serviceWorker.getRegistrations().then(registrations => {
    //       for(let registration of registrations) {
    //         registration.unregister().then(bool => {console.log('unregister: ', bool);});
    //       }
    //       window.location.reload();
    //     });
    //   });
    // }
  }

  componentDidUpdate() {
    // unregister();
  }

  render() {
    const {
      store, Component, ...props
    } = this.props;

    return (
      <Provider store={store}>
        <Component {...props}/>
      </Provider>
    );
  }
}

export default withRedux(myStore)(appWithTranslation(RootApp));
