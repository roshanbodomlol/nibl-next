import Layout from '../components/Layout';

function Error({ statusCode }) {
  console.error('_error_', statusCode);
  return (
    <Layout>
      <p>Error</p>
    </Layout>
  );
}

Error.getInitialProps = ({ res, err }) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404; // eslint-disable-line
  return { statusCode };
};

export default Error;
