import Layout from '../../components/Layout';
import SubscriptionVerifyComp from '../../components/SubscriptionVerify';
import { get } from '../../services/api.services';
import makeSlugs from '../../navigation/makeRouteSlugs';
import { setCurrentpage } from '../../redux/actions/currentpage.actions';
import { setNavigation } from '../../redux/actions/navigation.actions';
import { setSiteLoading, setContentLoading, setLogos } from '../../redux/actions/common.actions';
import { API } from '../../constants';

const SubscriptionVerify = () => (
  <Layout metaTitle="Nepal Investment Bank Ltd.">
    <SubscriptionVerifyComp/>
  </Layout>
);

SubscriptionVerify.getInitialProps = async ({ isServer, store }) => {
  let routes = null;
  if (isServer) {
    const rawRoutes = await get(API.endPoints.MENU);
    const logos = rawRoutes.logo;
    store.dispatch(setLogos(logos));
    routes = makeSlugs(rawRoutes);
    store.dispatch(setCurrentpage(routes.primary[0]));
    store.dispatch(setNavigation(routes));
    store.dispatch(setSiteLoading(false));
  } else {
    routes = store.getState().navigation;
  }

  store.dispatch(setContentLoading(false));
  return {};
};

export default SubscriptionVerify;
