import { SHOW_GLOBAL_SNACK, HIDE_GLOBAL_SNACK } from '../actionTypes';

export const initialState = {
  type: 'normal',
  message: '',
  duration: 3000
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SHOW_GLOBAL_SNACK:
      return {
        ...action.payload
      };
    case HIDE_GLOBAL_SNACK:
      return {
        type: 'normal',
        message: '',
        duration: 3000
      };
    default:
      return state;
  }
};
