import { combineReducers } from 'redux';
import common, { initialState as commonState } from './common.reducer';
import currentpage, { initialState as currentPageState } from './currentpage.reducer';
import locale, { initialState as localeState } from './locale.reducer';
import navigation, { initialState as navigationState } from './navigation.reducer';
import snack, { initialState as snackState } from './snack.reducer';

export const initialState = {
  common: commonState,
  currentpage: currentPageState,
  locale: localeState,
  navigation: navigationState,
  snack: snackState
};

export default combineReducers({
  common,
  currentpage,
  locale,
  navigation,
  snack
});
