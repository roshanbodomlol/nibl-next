import { SET_NAVIGATION } from '../actionTypes';

export const initialState = null;

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_NAVIGATION:
      return {
        ...action.payload
      };
    default:
      return state;
  }
};
