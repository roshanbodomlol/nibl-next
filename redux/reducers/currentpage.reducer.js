import { SET_CURRENTPAGE } from '../actionTypes';

export const initialState = {
  slug: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_CURRENTPAGE:
      return {
        ...action.payload
      };
    default:
      return state;
  }
};
