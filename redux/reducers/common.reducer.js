import {
  SET_MODAL_CLOSE, SET_MODAL_OPEN,
  SET_SPLASH_OPEN, SET_SPLASH_CLOSE, SHOW_SEARCH_BLOCK, HIDE_SEARCH_BLOCK,
  SET_SITE_LOADING, SET_CONTENT_LOADING, SET_LOGOS
} from '../actionTypes';

export const initialState = {
  modalOpen: false,
  splashOpen: true,
  splashInitialSlide: 0,
  searchBlockOpen: false,
  siteLoading: true,
  contentLoading: true,
  logos: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_MODAL_CLOSE:
      return {
        ...state,
        modalOpen: false
      };
    case SET_MODAL_OPEN:
      return {
        ...state,
        modalOpen: true
      };
    case SET_SPLASH_OPEN:
      return {
        ...state,
        splashOpen: true,
        splashInitialSlide: action.payload
      };
    case SET_SPLASH_CLOSE:
      return {
        ...state,
        splashOpen: false
      };
    case SHOW_SEARCH_BLOCK:
      return {
        ...state,
        searchBlockOpen: true
      };
    case HIDE_SEARCH_BLOCK:
      return {
        ...state,
        searchBlockOpen: false
      };
    case SET_SITE_LOADING:
      return {
        ...state,
        siteLoading: action.payload
      };
    case SET_CONTENT_LOADING:
      return {
        ...state,
        contentLoading: action.payload
      };
    case SET_LOGOS:
      return {
        ...state,
        logos: action.payload
      };
    default:
      return state;
  }
};
