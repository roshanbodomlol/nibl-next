import { SET_LOCALE } from '../actionTypes';

export const initialState = {
  lang: 'en'
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_LOCALE: {
      return {
        ...state,
        lang: action.lang
      };
    }

    default: {
      return state;
    }
  }
};
