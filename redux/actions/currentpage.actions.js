import { SET_CURRENTPAGE } from '../actionTypes';

export const setCurrentpage = currentPageSlug => (
  {
    type: SET_CURRENTPAGE,
    payload: currentPageSlug
  }
);
