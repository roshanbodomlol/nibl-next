import {
  SET_MODAL_OPEN,
  SET_MODAL_CLOSE,
  SET_SPLASH_OPEN,
  SET_SPLASH_CLOSE,
  SHOW_SEARCH_BLOCK,
  HIDE_SEARCH_BLOCK,
  SET_SITE_LOADING,
  SET_CONTENT_LOADING,
  SET_LOGOS
} from '../actionTypes';

export const setModalOpen = () => ({
  type: SET_MODAL_OPEN
});

export const setModalClose = () => ({
  type: SET_MODAL_CLOSE
});

export const setSplashOpen = slideIndex => ({
  type: SET_SPLASH_OPEN,
  payload: slideIndex
});

export const setSplashClose = () => ({
  type: SET_SPLASH_CLOSE
});

export const showSearchBlock = () => ({
  type: SHOW_SEARCH_BLOCK
});

export const hideSearchBlock = () => ({
  type: HIDE_SEARCH_BLOCK
});

export const setSiteLoading = (flag) => ({
  type: SET_SITE_LOADING,
  payload: flag
});

export const setContentLoading = (flag) => ({
  type: SET_CONTENT_LOADING,
  payload: flag
});

export const setLogos = (logos) => ({
  type: SET_LOGOS,
  payload: logos
});
