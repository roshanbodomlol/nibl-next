import { SET_NAVIGATION } from '../actionTypes';

export const setNavigation = routes => (
  {
    type: SET_NAVIGATION,
    payload: routes
  }
);
