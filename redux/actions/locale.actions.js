import { SET_LOCALE } from '../actionTypes';
import { GLOBALS } from '../../constants';

export const setLocale = lang => ({
  type: SET_LOCALE,
  lang
});

export const setLocaleAndRefresh = (lang) => {
  localStorage.setItem(GLOBALS.LANG_KEY, lang);
  window.location.reload();
};
