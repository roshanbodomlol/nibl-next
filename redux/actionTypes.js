export const SET_LOCALE = 'SET_LOCALE';
export const SHOW_GLOBAL_SNACK = 'SHOW_GLOBAL_SNACK';
export const HIDE_GLOBAL_SNACK = 'HIDE_GLOBAL_SNACK';
export const SET_NAVIGATION = 'SET_NAVIGATION';
export const SET_CURRENTPAGE = 'SET_CURRENTPAGE';
export const SET_MODAL_OPEN = 'SET_MODAL_ACTIVE';
export const SET_MODAL_CLOSE = 'SET_MODAL_CLOSE';
export const SET_SPLASH_OPEN = 'SET_SPLASH_OPEN';
export const SET_SPLASH_CLOSE = 'SET_SPLASH_CLOSE';
export const SHOW_SEARCH_BLOCK = 'SHOW_SEARCH_BLOCK';
export const HIDE_SEARCH_BLOCK = 'HIDE_SEARCH_BLOCK';
export const SET_SITE_LOADING = 'SET_SITE_LOADING';
export const SET_CONTENT_LOADING = 'SET_CONTENT_LOADING';
export const SET_LOGOS = 'SET_LOGOS';
