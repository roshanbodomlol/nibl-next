import { get } from '../services/api.services';
import { setCurrentpage } from '../redux/actions/currentpage.actions';
import { setSiteLoading, setContentLoading, setLogos } from '../redux/actions/common.actions';
import { setNavigation } from '../redux/actions/navigation.actions';
import makeSlugs from './makeRouteSlugs';
import { findPage } from '../utilities/currentpage.util';
import { API, GLOBALS } from '../constants';
import { generatePath } from '../utilities/url.utils';

export const initializePageOther = async ({ store, isServer, query, res }) => {
  let routes = null;
  const rawRoutes = await get(API.endPoints.MENU);
  const logos = rawRoutes.logo;
  store.dispatch(setLogos(logos));
  routes = makeSlugs(rawRoutes);
  store.dispatch(setCurrentpage(routes.primary[0]));
  store.dispatch(setNavigation(routes));
  store.dispatch(setSiteLoading(false));

  const { slug } = query;

  const url = generatePath(
    API.endPoints.OTHER_PAGE, { slug }
  );
  const data = await get(url);

  store.dispatch(setContentLoading(false));
  return {
    data,
    namespacesRequired: ['common']
  };
};
