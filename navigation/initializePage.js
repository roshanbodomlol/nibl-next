import { get } from '../services/api.services';
import { setCurrentpage } from '../redux/actions/currentpage.actions';
import { setSiteLoading, setContentLoading, setLogos } from '../redux/actions/common.actions';
import { setNavigation } from '../redux/actions/navigation.actions';
import makeSlugs from './makeRouteSlugs';
import { findPage } from '../utilities/currentpage.util';
import { API, GLOBALS } from '../constants';
import { generatePath } from '../utilities/url.utils';

export const initializePage = async ({ store, isServer, query, res }) => {
  let routes = null;
  if (isServer) {
    const rawRoutes = await get(API.endPoints.MENU);
    const logos = rawRoutes.logo;
    store.dispatch(setLogos(logos));
    routes = makeSlugs(rawRoutes);
    const flattenedRoutes = [...routes.primary, ...routes.secondary, ...routes.footer];
    if (query.page) {
      store.dispatch(setCurrentpage(flattenedRoutes.find(route => route.slug === query.page)));
    } else {
      store.dispatch(setCurrentpage(routes.primary[0]));
    }
    store.dispatch(setNavigation(routes));
    store.dispatch(setSiteLoading(false));
  } else {
    const rawRoutes = await get(API.endPoints.MENU);
    routes = makeSlugs(rawRoutes);
    const flattenedRoutes = [...routes.primary, ...routes.secondary, ...routes.footer];
    if (query.page) {
      store.dispatch(setCurrentpage(flattenedRoutes.find(route => route.slug === query.page)));
    } else {
      store.dispatch(setCurrentpage(routes.primary[0]));
    }
    store.dispatch(setNavigation(routes));
  }

  let data = {};
  if (!query.page) {
    data = {
      pageContent: {
        metaTitle: 'Nepal Investment Bank'
      }
    };
  } else {
    const page = findPage(routes, query);

    if (!page) {
      res.redirect('/404');
      return {};
    }

    if (GLOBALS.STATIC_TEMPLATES.indexOf(page.pageTemplate) > -1) {
      store.dispatch(setContentLoading(false));
      return {
        staticTemplate: page.pageTemplate
      };
    }
    const url = generatePath(
      API.endPoints[GLOBALS.TEMPLATES_TO_APIKEY[page.pageTemplate]], page
    );
    data = await get(url);
  }
  store.dispatch(setContentLoading(false));
  return {
    data,
    namespacesRequired: ['common']
  };
};
