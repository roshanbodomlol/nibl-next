import slugs from 'slugs';
import { sortBy } from 'lodash';

const makeSlug = name => slugs(name);

const makeSlugs = (routes) => {
  const primary = [...routes.primary];
  const secondary = [...routes.secondary];
  const footer = routes.footer ? [...routes.footer] : [];

  primary.forEach((route) => {
    const routeSlug = makeSlug(route.metaTitle);
    const routeRoute = `/${routeSlug}`;
    const { menuId } = route;
    route.route = routeRoute;
    route.slug = routeSlug;

    if (route.routes && route.routes.length > 0) {
      route.routes.forEach((childRoute) => {
        const childRouteSlug = makeSlug(childRoute.metaTitle);
        const childRouteRoute = `${routeRoute}/${childRouteSlug}`;
        childRoute.slug = childRouteSlug;
        childRoute.route = childRouteRoute;
        childRoute.topLevelId = menuId;

        if (childRoute.routes && childRoute.routes.length > 0) {
          childRoute.routes.forEach((gChild) => {
            const gChildSlug = makeSlug(gChild.metaTitle);
            const gChildRoute = `${childRouteRoute}/${gChildSlug}`;
            gChild.route = gChildRoute;
            gChild.slug = gChildSlug;
            gChild.topLevelId = menuId;
          });
        }
      });
    }
  });

  secondary.forEach((route) => {
    const routeSlug = makeSlug(route.metaTitle);
    const routeRoute = `/${routeSlug}`;
    const { menuId } = route;
    route.route = routeRoute;
    route.slug = routeSlug;

    if (route.routes && route.routes.length > 0) {
      route.routes.forEach((childRoute) => {
        const childRouteSlug = makeSlug(childRoute.metaTitle);
        const childRouteRoute = `${routeRoute}/${childRouteSlug}`;
        childRoute.slug = childRouteSlug;
        childRoute.slug = childRouteSlug;
        childRoute.topLevelId = menuId;

        if (childRoute.routes && childRoute.routes.length > 0) {
          childRoute.routes.forEach((gChild) => {
            const gChildSlug = makeSlug(gChild.metaTitle);
            const gChildRoute = `${childRouteRoute}/${gChildSlug}`;
            gChild.route = gChildRoute;
            gChild.slug = gChildSlug;
            gChild.topLevelId = menuId;
          });
        }
      });
    }
  });

  footer.forEach((route) => {
    const routeSlug = makeSlug(route.metaTitle);
    const routeRoute = `/${routeSlug}`;
    route.route = routeRoute;
    route.slug = routeSlug;
  });

  return {
    primary,
    secondary,
    footer
  };
};

export default makeSlugs;
