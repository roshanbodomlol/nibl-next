import fetch from 'isomorphic-fetch';

import { i18n } from '../locales/i18n';

export const get = api => new Promise((resolve, reject) => {
  const isServer = typeof window === 'undefined';
  const API_URL = isServer ? process.env.API_URL_LOCAL : process.env.API_URL;
  fetch(`${API_URL}/${api}`, {
    method: 'GET',
    headers: { 'X-Custom-Locale': i18n.language }
  })
    .then(response => response.json())
    .then((data) => {
      if (data.status) {
        resolve(data.result);
      } else reject(data.error);
    })
    .catch((e) => reject(e));
});

export const post = (api, data) => new Promise((resolve, reject) => {
  const isServer = typeof window === 'undefined';
  const API_URL = isServer ? process.env.API_URL_LOCAL : process.env.API_URL;

  const formData = new FormData();

  for(const name in data) {
    formData.append(name, data[name]);
  }

  fetch(`${API_URL}/${api}`, {
    method: 'POST',
    body: formData
  })
    .then(response => response.json())
    .then((response) => {
      if (response.status) {
        resolve(response.result);
      } else reject(new Error(response.error));
    })
    .catch((error) => reject(new Error(error)));
});

export const postNoCors = (api, data) => new Promise((resolve, reject) => {
  fetch(`${process.env.API_URL}/${api}`, {
    method: 'POST',
    body: data,
    mode: 'no-cors',
    headers: {
      'Content-Type': 'application/json'
    }
  })
    .then(response => response.json())
    .then((response) => {
      if (response.status) {
        resolve(response.result);
      } else reject(new Error(response.error));
    })
    .catch((error) => reject(new Error(error)));
});
