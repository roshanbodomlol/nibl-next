# Nepal Investment Bank Front End

## Pre-requisites
- Node v12.14.1
- pm2 `npm install -g pm2`

This guide is for deploying and updating the app in **local environment**

## Configuration
Create .env file in root folder with the configurations. e.g.
```
PORT=80
NODE_ENV=production
API_URL=http://localhost:80/api
GOOGLE_API_KEY=<API_KEY>
```
**NOTE** .env is not included in repository. So, it doesn't need to be changed with every update

## Build & Deploy
1. Navigate to folder where app is to be deployed.
2. In the command line, run `git clone https://roshanbodomlol@bitbucket.org/roshanbodomlol/nibl-next.git`
3. CD inside folder
4. Run `npm install`
5. Run `npm run build`
6. Run `pm2 start server.js`

The server will now keep running and command prompt can be closed

## Update
1. Navigate to project folder
2. Run `git pull`
3. Run `npm install` if necessary
4. Run `npm run build`
5. Run `pm2 list` to list running instances
6. Run `pm2 restart <id>` OR `pm2 stop <id>` and `pm2 start server.js`

## Production PORTS
- ENV variable: *API_URL_LOCAL* for server local api endpoint
- Place certificates in certificates folder (preserving current names)
- Run `npm run build`
- Run `pm2 start secureServer.js` for https server. This will ignore PORT ENV variable and use PORTS 80 & 443