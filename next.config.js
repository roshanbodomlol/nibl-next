const withSass = require('@zeit/next-sass');
const withCSS = require('@zeit/next-css');
const withPlugins = require('next-compose-plugins');
const path = require('path');
const loaderUtils = require('loader-utils');
const withOffline = require('next-offline');

require('dotenv').config();

const nextConfig = {
  env: {
    API_URL: process.env.API_URL,
    API_URL_LOCAL: process.env.API_URL_LOCAL,
    GOOGLE_API_KEY: process.env.GOOGLE_API_KEY
  },
  poweredByHeader: false
};

module.exports = withPlugins([
  [
    withCSS,
    {
      webpack: (config) => {

        config.module.rules.push({
          test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif)$/,
          use: {
            loader: 'url-loader',
            options: {
              limit: 100000,
              name: '[name].[ext]'
            }
          }
        });

        return config;
      }
    }
  ],
  [
    withSass,
    {
      cssModules: true,
      autoprefixer: true,
      cssLoaderOptions: {
        localIdentName: '[local]__[hash:base64:5]',
        getLocalIdent: (loaderContext, localIdentName, localName, options) => {
          const { resourcePath, rootContext } = loaderContext;
          if (resourcePath.indexOf('.module.') < 0) return localName;

          if (!options.context) {
            // eslint-disable-next-line no-param-reassign
            options.context = rootContext;
          }

          const request = path
            .relative(rootContext, loaderContext.resourcePath)
            .replace(/\\/g, '/');

          // eslint-disable-next-line no-param-reassign
          options.content = `${request}+${localName}`;

          // eslint-disable-next-line no-param-reassign
          localIdentName = localIdentName.replace(/\[local\]/gi, localName);

          const hash = loaderUtils.interpolateName(
            loaderContext,
            localIdentName,
            options
          );

          return hash
            .replace(new RegExp('[^a-zA-Z0-9\\-_\u00A0-\uFFFF]', 'g'), '-')
            .replace(/^((-?[0-9])|--)/, '_$1');
        }
      }
    }
  ],
  [
    withOffline,
    {
      generateInDevMode: false,
      workboxOpts: {
        runtimeCaching: [
          {
            urlPattern: /\.(?:png|jpg|jpeg|svg)$/,
            handler: 'CacheFirst'
          },
          {
            urlPattern: /api/,
            handler: 'NetworkFirst',
            options: {
              cacheableResponse: {
                statuses: [0, 200]
              }
            }
          }
        ]
      }
    }
  ]
], nextConfig);
