import React from 'react';

const Location = () => (
  <svg className="svg-icon _location" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17.86 25.92">
    <g id="Layer_2" data-name="Layer 2">
      <g id="Layer_1-2" data-name="Layer 1">
        <path id="Path_95" data-name="Path 95" d="M17.43,6.45a6.55,6.55,0,0,0-.42-1A8.88,8.88,0,0,0,0,8V9.15s0,.46,0,.67c.32,2.58,2.36,5.32,3.88,7.9,1.63,2.76,3.33,5.48,5,8.2L12,20.62c.28-.51.61-1,.88-1.5.18-.33.54-.65.7-.95,1.63-3,4.27-6,4.27-9V8a8.79,8.79,0,0,0-.43-1.52ZM8.88,12a3.14,3.14,0,0,1-3-2.16A2.8,2.8,0,0,1,5.76,9V8.32a3,3,0,0,1,3-3H9a3.28,3.28,0,0,1,3.27,3.26v.06A3.35,3.35,0,0,1,8.88,12Z"/>
      </g>
    </g>
  </svg>
);

export default Location;
